---
title: Disroot ist
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Disroot ist eine Plattform, die Internet-Dienste anbietet, die auf den Prinzipien von Freiheit, dem Schutz der Privatsphäre, der Föderation und der Dezentralisierung basieren.
**Kein Tracking, keine Werbung, kein Benutzerprofiling, kein Data Mining!**
