---
title: 'Disroot App'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: right
---

<br>

## Скачайте в  [![F-droid](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/ru/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/ru/packages/org.disroot.disrootapp/?class=lighter)

---

## Disroot app
Это приложение вроде швейцарского ножа для платформы **Disroot**, созданной сообществом для сообщества. Если у вас нет учетной записи **Disroot**, вы все равно можете использовать это приложение для доступа ко всем сервисам **Disroot**, для которых не требуется учетная запись, таким как быстрые заметки, файлообменник и т.д.
