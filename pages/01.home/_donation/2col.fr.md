---
title: Online-donation
bgcolor: '#fff'
wider_column: left
fontcolor: '#1e5a5e'
---

![](premium.png?class=reward) **Disroot** compte sur les dons et le soutien de sa communauté et des utilisateurs de ses services. Si vous souhaitez que le projet se poursuive et contribuer à créer un environnement favorable à de nouveaux disrooters potentiels, veuillez utiliser l'une des méthodes disponibles pour apporter une contribution financière. Un lien vers un  [domaine personnalisé](/services/email#alias) est disponible pour les donateurs réguliers.

Vous pouvez également collaborer en achetant de l'espace [stockage supplémentaire de courrier électronique](/services/email#storage) et/ou du [stockage cloud supplémentaire](/services/nextcloud#storage).

---

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Contribuer en utilisant Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Devenez un mécène" src="donate/_donate/p_button.png" /></a>

<a href="https://flattr.com/profile/disroot" target=_blank><img alt="Flatter" src="donate/_donate/f_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=fr&hosted_button_id=AW6EU7E9NN3VQ&source=url" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://disroot.org/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

<span style="color:#4e1e2d; font-size:1.1em; font-weight:bold;">Virement bancaire:</span>
<span style="color:#4e1e2d; font-size:1.1em;">
Stichting Disroot.org
IBAN NL19 TRIO 0338 7622 05
BIC TRIONL2U
</span>
