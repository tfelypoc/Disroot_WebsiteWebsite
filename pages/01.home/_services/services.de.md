---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: E-Mail
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Freie und sichere E&#8209;Mail-Accounts, nutzbar mit einem IMAP-Client oder via Online-Benutzeroberfläche."
        button: 'https://webmail.disroot.org'
        buttontext: "Anmelden"
    -
        title: Cloud
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "Deine Daten unter Deiner Kontrolle! Sichern, Synchronisieren, Teilen von Kalendern, Kontakten und mehr."
        button: 'https://cloud.disroot.org'
        buttontext: "Anmelden"
    -
        title: Forum
        icon: forum.png
        link: 'https://disroot.org/services/forum'
        text: "Diskussionsforen / Mailinglisten für Deine Projekte, Gruppen oder Gemeinschaften."
        button: 'https://forum.disroot.org'
        buttontext: "Anmelden"
    -
        title: XMPP-Chat
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Dezentralisiertes, sicheres und freies Instant-Messaging."
        button: 'https://webchat.disroot.org'
        buttontext: "Anmelden"
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Gemeinschaftlich und in Echtzeit Dokumente direkt im Webbrowser bearbeiten"
        button: 'https://pad.disroot.org'
        buttontext: "Ein Pad starten"

    -
        title: 'Paste-Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Minimalistischer, quelloffener und verschlüsselter Wegwerf-Container (Paste-Bin) mit Diskussionsboard."
        button: 'https://bin.disroot.org'
        buttontext: "Teile einen Paste-Bin"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Software zur verschlüsselten, temporären Dateiaufbewahrung und -verbreitung."
        button: 'https://upload.disroot.org'
        buttontext: "Datei teilen"
    -
        title: Suche
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Eine anonyme Meta-Suchmaschine."
        button: 'https://search.disroot.org'
        buttontext: "Suche"

    -
        title: Projekt-Board
        icon: project_board.png
        link: 'https://disroot.org/services/project-board'
        text: "Ein Werkzeug zur Projektverwaltung im Kanban-Stil."
        button: 'https://board.disroot.org'
        buttontext: "Anmelden"
    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Ein Werkzeug für Videokonferenzen."
        button: 'https://calls.disroot.org'
        buttontext: "Aufruf"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Eine Code-Hosting- und -Projektzusammenarbeits-Plattform."
        button: 'https://git.disroot.org'
        buttontext: "Anmelden"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Eine Voice-Chat-Anwendung mit niedriger Latenz und hoher Sprachqualität."
        button: 'https://mumble.disroot.org'
        buttontext: "Anmelden"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Eine schon vom Design her vertraulich gestaltete Alternative zu gängigen Office-Tools."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Zugang"
        #badge: EXPERIMENTAL
---
