---
title: Servicios
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Correo
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Cuentas de correo seguras y gratuitas para tu cliente IMAP de escritorio o via web."
        button: 'https://webmail.disroot.org'
        buttontext: "Accede"
    -
        title: Nube
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "¡Tus datos bajo tu control! Colabora, sincroniza y comparte archivos, calendarios, contactos y más."
        button: 'https://cloud.disroot.org'
        buttontext: "Accede"
    -
        title: Foro
        icon: forum.png
        link: 'https://disroot.org/services/forum'
        text: "Foros de discusión y lista de correo para tu comunidad, grupo o colectivo."
        button: 'https://forum.disroot.org'
        buttontext: "Accede"
    -
        title: Chat XMPP
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Mensajería instantánea descentralizada."
        button: 'https://webchat.disroot.org'
        buttontext: "Accede"
    -
        title: Blocs
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Crea y edita documentos colaborativamente y en tiempo real directamente en tu navegador."
        button: 'https://pad.disroot.org'
        buttontext: "Abre un bloc"

    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Paste-bin/tablero de discusión cifrado en línea."
        button: 'https://bin.disroot.org'
        buttontext: "Comparte un pastebin"
    -
        title: Subida
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Alojamiento temporario cifrado."
        button: 'https://upload.disroot.org'
        buttontext: "Comparte un archivo"
    -
        title: Búsqueda
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Plataforma multimotor de búsqueda anónima."
        button: 'https://search.disroot.org'
        buttontext: "Buscar"

    -
        title: 'Tablero de proyecto'
        icon: project_board.png
        link: 'https://disroot.org/services/project-board'
        text: "Herramienta de gestión de proyectos."
        button: 'https://board.disroot.org'
        buttontext: "Accede"
    -
        title: 'Llamadas'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Herramienta de videoconferencia."
        button: 'https://calls.disroot.org'
        buttontext: "Llamar"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Hospedaje de código y proyectos colaborativos."
        button: 'https://git.disroot.org'
        buttontext: "Accede"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Herramienta de audio chat."
        button: 'https://mumble.disroot.org'
        buttontext: "Conéctate"
        #badge: EXPERIMENTAL
    -
        title: 'CryptPad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Herramientas de oficina privadas-por-defecto."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Accede"
        #badge: EXPERIMENTAL
---
