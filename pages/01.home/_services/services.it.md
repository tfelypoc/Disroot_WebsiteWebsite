---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Email
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Account di posta libero e sicuro accessibile attraverso client o interfaccia web."
        button: 'https://webmail.disroot.org'
        buttontext: "Accedi"
    -
        title: Cloud
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "I tuoi dati sotto il tuo controllo. Collabora, sincronizza e condividi file, calendari e contatti."
        button: 'https://cloud.disroot.org'
        buttontext: "Accedi"
    -
        title: Forum
        icon: forum.png
        link: 'https://disroot.org/services/forum'
        text: "Forum di discussione/mailing list per la tua comunità, gruppo o collettivo."
        button: 'https://forum.disroot.org'
        buttontext: "Accedi"
    -
        title: XMPP Chat
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Sistema di messaggistica decentralizzato."
        button: 'https://webchat.disroot.org'
        buttontext: "Accedi"
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Crea e modifica documenti direttamente nel browser web."
        button: 'https://pad.disroot.org'
        buttontext: "Iniziare"

    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Paste Bin crittografata."
        button: 'https://bin.disroot.org'
        buttontext: "Iniziare"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Software di condivisione per file temporanei crittografato ."
        button: 'https://upload.disroot.org'
        buttontext: "Condividere"
    -
        title: Search
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Un metamotore di ricerca anonimo."
        button: 'https://search.disroot.org'
        buttontext: "Ricerca"

    -
        title: 'Project Board'
        icon: project_board.png
        link: 'https://disroot.org/services/project-board'
        text: "Uno strumento di project management."
        button: 'https://board.disroot.org'
        buttontext: "Accedi"
    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Uno strumento di videoconferenza."
        button: 'https://calls.disroot.org'
        buttontext: "Chiamare"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Un hosting per codice e gestione di progetti."
        button: 'https://git.disroot.org'
        buttontext: "Entra"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Un'applicazione di chat vocale a bassa latenza e di alta qualità."
        button: 'https://mumble.disroot.org'
        buttontext: "Entra"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Un'alternativa personalizzata ai popolari strumenti di ufficio."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Accedi"
        #badge: EXPERIMENTAL
---
