---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: 'Почта'
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Бесплатный и безопасный почтовый сервер с поддержкой IMAP и приятным веб-интерфейсом."
        button: 'https://webmail.disroot.org'
        buttontext: "Войти"
    -
        title: 'Облако'
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "Ваши данные под вашим контролем! Совместная работа, синхронизация, обмен файлами и многое другое."
        button: 'https://cloud.disroot.org'
        buttontext: "Войти"
    -
        title: 'Форум'
        icon: forum.png
        link: 'https://disroot.org/services/forum'
        text: "Дискуссионные форумы/список рассылки для вашего сообщества, группы или коллектива."
        button: 'https://forum.disroot.org'
        buttontext: "Войти"
    -
        title: 'XMPP чат'
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Децентрализованный мессенджер, проверенный временем."
        button: 'https://webchat.disroot.org'
        buttontext: "Войти"
    -
        title: 'Документы'
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Совместно создавайте и редактируйте документы прямо в браузере."
        button: 'https://pad.disroot.org'
        buttontext: "Создать документ"

    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Зашифрованные онлайн заметки и обсуждения."
        button: 'https://bin.disroot.org'
        buttontext: "Поделиться заметкой"
    -
        title: 'Файлообменник'
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Зашифрованное файловое хранилище с ограниченным сроком хранения."
        button: 'https://upload.disroot.org'
        buttontext: "Поделиться файлом"
    -
        title: 'SearX'
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Гибкий приватный инструмент для поиска в интернете."
        button: 'https://search.disroot.org'
        buttontext: "Найти"

    -
        title: 'Проекты'
        icon: project_board.png
        link: 'https://disroot.org/services/project-board'
        text: "Инструмент для управления проектами."
        button: 'https://board.disroot.org'
        buttontext: "Войти"
    -
        title: 'Jitsi'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Сервис для проведения видеозвонков."
        button: 'https://calls.disroot.org'
        buttontext: "Начать конференцию"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Площадка для хостинга исходных текстов и совместной разработки."
        button: 'https://git.disroot.org'
        buttontext: "Войти"
    -
        title: 'Mumble'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Голосовой чат с низкой задержкой и хорошим качеством звука."
        button: 'https://mumble.disroot.org'
        buttontext: "Войти"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Альтернатива популярным офисным приложениям с акцентом на приватность."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Начать"
        #badge: EXPERIMENTAL
---
