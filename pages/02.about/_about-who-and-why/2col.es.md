---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="/mission-statement">Lee nuestra Declaración de Objetivos</a>

---

**Disroot** es un proyecto que nació de la necesidad personal de comunicarnos, compartir y organizarnos dentro de nuestros círculos a través de herramientas que se ajustaran a una serie de criterios fundamentales para nosotrxs: tenían que ser **abiertas**, **descentralizadas**, **federadas** y sobre todo **respetuosas** de la **libertad** y la **privacidad**. Ninguna de las soluciones más populares cumplían con ellos, pero en nuestra búsqueda encontramos algunos proyectos muy interesantes que sí lo hacían. Y como pensamos que debían estar al alcance de todas aquellas personas con principios éticos similares,  decidimos no solo agrupar estas aplicaciones y compartirlas con otrxs, sino también gestionarlas de acuerdo con esos valores. Así es como **Disroot** comenzó.


El proyecto está radicado en Amsterdam, fue fundado por **Muppeth** y **Antilopa** en 2015, es mantenido por voluntarixs y depende del apoyo de su comunidad. Entre 2017 y 2018, un grupo de miembros comenzaron a involucrarse muy activamente y a colaborar de manera muy estrecha. En 2019, dos de ellos, **Fede** y **Meaz**, pasaron oficialmente a formar parte del Equipo Principal que administra la plataforma. Dos años más tarde, en 2021, **Avg_Joe** se sumó a las tareas y responsabilidades.


**Disroot** pretende cambiar la manera en que las personas están acostumbradas a interactuar en la web. Queremos alentarlas y mostrarles no solo que existen alternativas abiertas y éticas sino también que es el único camino posible para liberarse de los *jardines vallados* que proponen y promueven el software propietario y las corporaciones, ya sea a través de nuestra plataforma, de otras que tienen valores y objetivos similares o incluso de sus propios proyectos.


Estamos convencidxs que podemos construir juntxs una red verdaderamente independiente, enfocada en el beneficio de la gente y no en su explotación.
