---
title: Föderation und Dezentralisierung
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

<br>
<br>
![](about-organize.jpg)

---

<br>
# Föderation und Dezentralisierung

Die meisten Internetdienste werden von einem zentralen Punkt aus betrieben, im Eigentum und Betrieb von gewinnorientierten Unternehmen. Diese speichern persönliche Daten ihrer Nutzer und analysieren sie mithilfe ausgeklügelter Algorithmen, um genaue und umfangreiche Profile ihrer 'Nutzer' anzulegen. Diese Informationen werden oft genutzt, um die Menschen zum Vorteil der Werbeindustrie auszubeuten. Ebenso können die Informationen durch staatliche Organisationen oder durch böswillige Hacker erlangt und ausgenutzt werden. Daten können ohne Warnung entfernt werden, aus fragwürdigen Gründen oder aufgrund regionaler Zensurvorgaben.

Ein dezentraler Service kann auf vielen Geräten laufen, im Eigentum verschiedener Individuen, Unternehmen oder Organisationen. Dank föderativer Protokolle können diese Instanzen miteinander interagieren und so ein Netzwerk formen, bestehend aus vielen Knotenpunkten (Servern, die die gleichen Dienste hosten). Es mag vorkommen, dass jemand es schafft, einen Knotenpunkt auszuschalten, aber niemals das gesamte Netzwerk. Bei einem solchen Aufbau ist Zensur praktisch unmöglich.

Ein solcher Aufbau ist nichts Neues. Überleg' Dir mal, wie E&#8209;Mails funktionieren: Du kannst Dir jeden Dienstanbieter (Provider) aussuchen, den Du willst, oder sogar einen eigenen E&#8209;Mail-Dienst anbieten. Und trotzdem kannst Du mit jedem anderen Menschen in Kontakt treten, auch wenn dieser einen anderen Anbieter nutzt. E&#8209;Mail baut auf einem dezentralen und föderativen Protokoll auf.

Diese zwei Prinzipien zusammengenommen bilden den Hintergrund für ein riesiges Netzwerk, das bei aller Größe eine eher einfache Infrastruktur mit relativ geringen Kosten benötigt. Jedes Gerät kann ein Server und ein gleichberechtigter Teilnehmer im Netzwerk werden. Vom kleinen Heimcomputer über den dedizierten Server bis hin zum Serverschrank. Dieser Ansatz gibt uns die Möglichkeit, das größte benutzereigene, globale Netzwerk zu erschaffen - genau so, wie das Internet mal sein sollte.

Wir wollen nicht, dass **Disroot** eine zentralisierte Entität wird, sondern vielmehr ein Teil einer viel größeren Gemeinschaft - ein Knotenpunkt von vielen. Wir hoffen, dass Andere sich inspirieren lassen, mehr Projekte mit den gleichen Zielen zu starten.
