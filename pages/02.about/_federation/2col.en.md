---
title: Federation and Decentralization
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

<br>
<br>
![](about-organize.jpg)

---

<br>
# Federation and Decentralization

Most internet services are run from centralized point owned and run by corporations. These store private data of their users and analyze it using advanced algorithms in order to create accurate profiles of their 'users'. This information is often used to exploit the people for the sake of advertisers. Information can also be obtained by governmental institutions or by malicious hackers. Data can be removed without warning due to dubious reasons or regional censorship policies.

A decentralized service can reside on multiple machines, owned by different individuals, companies or organization. With federation protocols those instances can interact and form one network of many nodes (servers hosting similar services). One might be able to shut down one node but never the entire network. In such a setup censorship is practically impossible.

Think of how we use E-mail; you can choose any service provider or set up your own, and still exchange emails with people using another email provider. E-mail is built upon a decentralized and federated protocol.

These two principals, together, sets up the backdrop for a huge network that depends on a rather simple infrastructure and relatively low costs. Any machine can become a server and be an equal participant in the network. From a small desktop computer at your home to a dedicated server or racks with multiple servers. This approach provides the possibility to create the biggest user owned global network - just as the Internet was intended to be.

We do not wish for **Disroot** to become one centralized entity but rather a part of a larger community - one node out of many. We hope others will be inspired to create more projects with similar intentions.
