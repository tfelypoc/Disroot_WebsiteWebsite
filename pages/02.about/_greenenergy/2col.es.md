---
title: Online-donation
bgcolor: '#1F5C60'
wider_column: left
fontcolor: '#ffffff'
---

Los servidores de **Disroot** están alojados en un data center que utiliza energía renovable.

---

<a href="https://api.thegreenwebfoundation.org/greencheckimage/disroot.org?nocache=true" target=_blank><img src="https://api.thegreenwebfoundation.org/greencheckimage/disroot.org?nocache=true" alt="This website is hosted Green - checked by thegreenwebfoundation.org" style="height:100px"></a>
