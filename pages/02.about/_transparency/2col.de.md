---
title: Transparency
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

<br>
![](about-trust.png)

---

# Transparenz und Offenheit

Für **Disroot** nutzen wir zu 100% freie und quelloffene Software. Das heißt, dass der Quellcode (die Anweisungen, die die Software abarbeitet) öffentlich zugänglich ist. Jeder kann Verbesserungen oder Anpassungen zu der Software hinzufügen und sie kann jederzeit überprüft und kontrolliert werden - keine versteckten Hintertürchen oder bösartigen Schadprogramme.

Wir wollen völlig offen und transparent sein den Menschen gegenüber, die unser Angebot nutzen. Daher veröffentlichen wir Informationen über den momentanen Status des Projekts, die finanzielle Situation, unsere Pläne und Ideen. Wir freuen uns auch über Vorschläge und Feedback, damit wir allen **Disrootern** das bestmögliche Nutzererlebnis bieten können.
