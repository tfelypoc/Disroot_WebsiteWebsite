---
title: 'Nutze Deinen bevorzugten E-Mail-Client'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

<br>
# Nutze das E&#8209;Mail-Programm Deiner Wahl
Es gibt Unmengen E&#8209;Mail-Clients für den Desktop oder Dein Mobilgerät. Such Dir den Client aus, der Dir am besten gefällt.<br>

Schau Dir unser [Tutorial](https://howto.disroot.org/de/tutorials/email/clients) an, um nähere Informationen zum Setup Deines bevorzugten Clients zu erhalten.
<br>
*Oben auf dieser Seite findest Du die Einstellungen, um Deine* **Disroot**-*E&#8209;Mail auf Deinem Gerät einzurichten. Diese Einstellungen sind die Standardinformationen, die Deinem E&#8209;Mail-Client sagen, wie er den* **Disroot**-*E&#8209;Mailserver erreicht.*
