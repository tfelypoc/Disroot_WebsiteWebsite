---
title: 'Clients de messagerie'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

<br>
# Utilisez votre client mail favori
Il y a beaucoup de clients de messagerie pour les PC ou les appareils mobiles. Choisissez celui qui vous préférez.<br>

Consultez notre [page de tutoriels](https://howto.disroot.org/fr/tutorials/email/clients) dédié à tous les clients de messagerie.
<br>
*Vérifiez les paramètres en haut de cette page pour configurer votre messagerie* **Disroot** *sur votre appareil. Ces paramètres sont des informations standard qui indiquent à votre client de messagerie comment accéder au serveur de messagerie de* **Disroot**.
