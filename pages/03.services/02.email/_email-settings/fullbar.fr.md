---
title: 'Paramètres de courriel'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">993</span> | Authentification: <span style="color:#8EB726">Mot de passe normal</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Port <span style="color:#8EB726">587</span> | Authentification: <span style="color:#8EB726">Mot de passe normal</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Port <span style="color:#8EB726">465</span> | Authentification: <span style="color:#8EB726">Mot de passe normal</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">995</span> | Authentification: <span style="color:#8EB726">Mot de passe normal</span>

---

**Taille de la boîte mail:** 1 Go
**Limite de taille de la pièce jointe:** 64 Mo

---

**Delimiteurs:** Vous pouvez utiliser des délimiteurs (le signe "+") dans votre adresse électronique pour créer des sous-adresses, telles que **nom d'utilisateur+nimporte@disroot.org**, par exemple pour le filtrage et le suivi du spam. Exemple : david@disroot.org peut créer des adresses de courrier électronique comme "david+bank@disroot.org" qu'il pourrait donner à sa banque. Ces adresses peuvent être utilisées **seulement pour recevoir des courriels**, pas pour en envoyer. Vos e-mails seront toujours envoyés sous la forme david@disroot.org.
