---
title: 'Email Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Порт <span style="color:#8EB726">993</span> | Аутентификация: <span style="color:#8EB726">Обычный пароль</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Порт <span style="color:#8EB726">587</span> | Аутентификация: <span style="color:#8EB726">Обычный пароль</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Порт <span style="color:#8EB726">465</span> | Аутентификация: <span style="color:#8EB726">Обычный пароль</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Порт <span style="color:#8EB726">995</span> | Аутентификация: <span style="color:#8EB726">Обычный пароль</span>

---

**Ограничение почтового хранилища:** 1 GB
**Максимальный размер вложения:** 64 MB

---

**Разделители:** Вы можете использовать разделители (знак плюса «+») в своем адресе электронной почты для создания дополнительных адресов вида **username+whatever@disroot.org**, например, для фильтрации и отслеживания спама. Пример: david@disroot.org может настроить почтовые адреса типа david+bank@disroot.org, которые он может передать своему банку. Его можно использовать **только для получения писем**, но не для их отправки. Электронные письма всегда будут отправляться от настоящего адреса, в нашем примере это david@disroot.org.
