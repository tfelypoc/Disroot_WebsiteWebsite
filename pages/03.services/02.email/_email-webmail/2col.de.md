---
title: 'E-mail Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
Du kannst von jedem Gerät (mit Internetzugang) auf Deine E&#8209;Mails zugreifen, indem Du unsere Webmail-Oberfläche unter [https://webmail.disroot.org](https://webmail.disroot.org) nutzt.

Unsere Webmail haben wir mit **Roundcube** verwirklicht. Roundcube ist ein moderner Ansatz zum Thema Webmail. Es ist simpel, schnell und bietet die meisten Features, die man von Webmail erwarten kann:

<ul class=disc>
<li>Theme: Eine ansprechende Oberfläche: im Disroot Design mit Unterstützung für unterschiedliche Endgeräte.</li>
<li>Übersetzungen: Die Benutzeroberfläche gibt es in über 80 Sprachen.</li>
<li>Drag-&-drop support: verschiebe deine Nachrichten mit der Maus.</li>
<li>Volle Unterstützung von MIME und HTML Nachrichten.</li>
<li>Vorschau von Anhängen: Anhänge werden direkt in der E&#8209;Mail angezeigt.</li>
<li>Anzeige als Threads: Konversationen können als Thread angezeigt werden.</li>
<li>Filter: Mit Hilfe von Regeln verschiebst oder kopierst Du E&#8209;Mails in die richtigen Verzeichnisse, leitest sie weiter oder lehnt sie ab.</li>
<li>Verzeichnisse: Verwalte Deine Verzeichnisse (Erstellen, Entfernen oder Verstecken).</li>
</ul>

Projekt-Website: [https://roundcube.net](https://roundcube.net)
