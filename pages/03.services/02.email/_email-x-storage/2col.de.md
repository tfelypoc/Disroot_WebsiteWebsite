---
title: 'Zusätzlicher Mailboxspeicherplatz'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Erweitere den Speicherplatz Deines Postfachs
Wenngleich eine Postfachgröße von 1 GB nicht so klein ist, kann es natürlich sein, dass Du mehr Speicher benötigst. Es ist daher möglich, Deinen E&#8209;Mail-Speicher zu erweitern.

Dies sind die Preise **pro Jahr, Zahlungsgebühren inklusive**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Auf Transaktionen innerhalb der EU wird eine zusätzliche VAT (Value Added Tax) von 21 % fällig.

---

<br><br>

<a class="button button1" href="/forms/extra-storage-space">Zusätzlichen E&#8209;Mail-Speicher beantragen</a>
