---
title: 'Spazio extra per la posta elettronica'
bgcolor: '#FFF'
fontcolor: '#7A7A7A'
wider_column: left
---

## Aggiungi spazio di archiviazione per la posta elettronica

Se 1GB di spazio gratuito non è abbastanza, puoi estendere la tua memoria di posta elettronica.

Ecco i prezzi **all'anno, spese di pagamento incluse**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |

<br>
Le transazioni all'interno dell'UE sono soggette a un'IVA aggiuntiva (imposta sul valore aggiunto) del 21%.

---

<br><br>

<a class="button button1" href="https://disroot.org/it/forms/extra-storage-mailbox">Richiedi ulteriore spazio di archiviazione per la posta elettronica</a>
