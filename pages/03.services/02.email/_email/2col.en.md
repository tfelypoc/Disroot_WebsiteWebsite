---
title: Email
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Sign up for Disroot</a>
<a class="button button1" href="https://webmail.disroot.org/">Log in</a>

---

## E-mail

**Disroot** provides secure email accounts for your desktop client or via a web interface. The communication between you and the mail server is encrypted with SSL, providing as much privacy as possible. Furthermore, all the emails being sent out from our server are encrypted as well (TLS) if the recipients email server supports it. This means that emails are no longer sent as traditional "postcard", but are actually put in an "envelope".

**Nevertheless, we encourage you to always be cautious when using email communication, and to make use of GPG end-to-end encryption to ensure your correspondence is as private as can be.**
