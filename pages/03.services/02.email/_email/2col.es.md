---
title: Correo
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Inscribirse en Disroot</a>
<a class="button button1" href="https://webmail.disroot.org/">Iniciar sesión</a>

---

## Correo electrónico

**Disroot** provee cuentas de correo seguro para tu cliente de escritorio o a través de la web.
La comunicación entre el servidor de correo y tú está cifrada con [SSL](https://es.wikipedia.org/wiki/Transport_Layer_Security), otorgando tanta privacidad como es posible. Además, todos los correos que son enviados desde nuestro servidor están cifrados también ([TLS](https://es.wikipedia.org/wiki/Seguridad_de_la_capa_de_transporte)) si el servidor de correo destinatario los soporta. Esto significa que los correos electrónicos ya no se envían como "postales" tradicionales, sino que realmente son puestos en un "sobre".

**Sin embargo, les sugerimos y alentamos a ser siempre cautelosxs cuando utilizan la comunicación por correo electrónico, y utilizar el cifrado GPG para garantizar que la correspondencia sea más segura.**
