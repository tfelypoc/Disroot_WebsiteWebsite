---
title: Email
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org">Registrati su Disroot</a>
<a class="button button1" href="https://webmail.disroot.org/">Accedi</a>

---

## E-mail

**Disroot** fornisce account di posta elettronica accessibili da client o da interfaccia web.
Le comunicazioni con il server di posta sono criptate con SSL, fornendo così la maggior privacy possibile. L'invio di messaggi di posta elettronica sono anch'essi criptate con TLS (se il server del destinatario supporta questa specifica). Questo sistema permette maggior privacy nel servizio di posta elettronica.

**Nonostante ciò incoraggiamo gli utenti a essere cauti e nel limite del possibile utilizzare GPG per criptare i messaggi e rendere la corrispondenza ancora più sicura.**
