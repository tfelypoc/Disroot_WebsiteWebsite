---
title: 'Kalender und Kontakte'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Nutze Deinen bevorzugten Client'
clients:
    -
        title: GOA
        logo: de/goa.png
        link: https://wiki.gnome.org/Projects/GnomeOnlineAccounts
        text: 'Gnome Online Accounts'
        platforms: [fa-linux]
    -
        title: Kaddressbook
        logo: de/KDE.png
        link: https://kde.org/applications/office/org.kde.kaddressbook
        text:
        platforms: [fa-linux]
    -
        title: Thunderbird
        logo: de/thunderbird.png
        link: https://www.thunderbird.net/
        text:
        platforms: [fa-linux, fa-windows]
    -
        title: DAVx⁵
        logo: de/davx5.png
        link: https://f-droid.org/de/packages/at.bitfire.davdroid/
        text:
        platforms: [fa-android]


---

## Kalender und Kontakte

Teile Deine Kalender mit anderen **Nextcloud**-Nutzern oder -Gruppen auf Deinem Nextcloud-Server, einfach und schnell. Speicher Deine Kontakte in **Nextcloud** und teile sie zwischen Deinen Geräten, so dass Du immer Zugriff auf sie hast, egal mit welchem Gerät Du gerade arbeitest.

---


![](de/nextcloud-cal.png?lightbox=1024)
