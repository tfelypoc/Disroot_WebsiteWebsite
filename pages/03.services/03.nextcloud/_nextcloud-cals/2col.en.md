---
title: 'Calendars and Contacts'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choose your favorite client'
clients:
    -
        title: GOA
        logo: en/goa.png
        link: https://wiki.gnome.org/Projects/GnomeOnlineAccounts
        text: 'Gnome online accounts'
        platforms: [fa-linux]
    -
        title: Kaddressbook
        logo: en/KDE.png
        link: https://kde.org/applications/office/org.kde.kaddressbook
        text:
        platforms: [fa-linux]
    -
        title: Thunderbird
        logo: en/thunderbird.png
        link: https://www.thunderbird.net/
        text:
        platforms: [fa-linux, fa-windows]
    -
        title: DAVx⁵
        logo: en/davx5.png
        link: https://f-droid.org/en/packages/at.bitfire.davdroid/
        text:
        platforms: [fa-android]

---

## Calendars and Contacts

Share your calendars with other Nextcloud users or groups on your Nextcloud server, easily and quickly. Store your contacts in Nextcloud and share them among your devices so you always have access to it no matter which device you are currently on.

---

![](en/nextcloud-cal.png?lightbox=1024)
