---
title: 'Nextcloud Checksum'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Checksum app

Allows users to create a hash checksum of a file.
Possible algorithms are md5, sha1, sha256, sha384, sha512 and crc32.

---

![](en/checksum.gif)
