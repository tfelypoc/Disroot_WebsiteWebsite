---
title: 'Suma de verificación Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Aplicación para Suma de verificación (Checksum)

Permite a lxs usuarixs crear un [hash](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash) de [suma de verificación](https://es.wikipedia.org/wiki/Suma_de_verificaci%C3%B3n) de un archivo.
Los algoritmos posibles son md5, sha1, sha256, sha384, sha512 y crc32.

---

![](en/checksum.gif)
