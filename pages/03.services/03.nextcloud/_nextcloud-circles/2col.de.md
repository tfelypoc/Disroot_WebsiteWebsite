---
title: 'Nextcloud Kreise'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](de/nextcloud-circles.png?lighbox=1024)

---

## Kreise

Erstelle Deine eigenen Gruppen von Nutzern/Kollegen/Freunden, um mit ihnen Inhalt einfach zu teilen. Deine Kreise können öffentlich, privat (für andere sichtbar, benötigen jedoch eine Einladung) oder geheim (sind unsichtbar und benötigen ein Passwort) sein.
