---
title: 'End zu Ende'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](de/endtoend-android-nw.png?lightbox=1024)

---

## Ende-zu-Ende Verschlüsselung  !! Derzeit deaktiviert !!

**Nextcloud**'s Ende-zu-Ende-Verschlüsselung bietet Dir den ultimativen Schutz für Deine Daten. Nexcloud's E2E-Verschlüsselungsplugin ermöglicht es den Nutzern, ein oder mehrere Verzeichnisse auf ihrem Desktop- oder Mobil-Gerät zur Ende-zu-Ende-Verschlüsselung auszuwählen. Die Verzeichnisse können mit anderen Nutzern geteilt oder mit anderen Geräten synchronisiert werden, aber sind auf der Serverseite nicht lesbar.

<span style="color:red">**Ende-zu-Ende-Verschlüsselung befindet sich noch in der Alpha-Version. Nutze sie nicht im Wirkbetrieb sondern nur mit Testdaten!**</span>

Mehr erfährst Du [hier](https://nextcloud.com/endtoend/)
