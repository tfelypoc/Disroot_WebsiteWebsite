---
title: 'Fédération Nextcloud '
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Fédération

Grâce à la fédération, vous pouvez lier et partager vos dossiers avec n'importe qui utilisant d'autres instances **Nextcloud** (ou **ownCloud**) autres que **Disroot**.

---

![](en/NC_federation.png?lightbox=1024)
