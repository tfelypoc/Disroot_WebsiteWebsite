---
title: 'Galerías Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-gallery.png?lightbox=1024)

---

## Galerías

Comparte galerías de fotos con amigxs y familiares. Dales acceso para subir imágenes, verlas y descargarlas. Envía un link a quien quieras, y controla si pueden compartir esas fotos con otras personas.
