---
title: 'Notes'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Scegli il tuo client preferito'
clients:
    -
        title: Notes
        logo: en/notes_logo.png
        link: https://f-droid.org/en/packages/it.niedermann.owncloud.notes/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-notes.png?lightbox=1024)

---

## Notes

Crea, condividi note e sincronizzale su tutti i tuoi dispositivi.
