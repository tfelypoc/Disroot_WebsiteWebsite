---
title: 'Nextcloud Sync'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choose your favorite client'
clients:
    -
        title: Desktop
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: Mobile
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-android, fa-apple]
    -
        title: Webbrowser
        logo: en/webbrowser.png
        link: https://cloud.disroot.org
        text: 'Access directly from your browser'
        platforms: [fa-linux, fa-windows, fa-apple]

---

## Nextcloud Sync

Access and organize your data on any platform. Use the **Desktop**, **Android** or **iOS** clients to work with your files on the go or synchronize your favorite folders seamlessly between your desktop and laptop devices.

---

![](en/nextcloud-files.png?lightbox=1024)
