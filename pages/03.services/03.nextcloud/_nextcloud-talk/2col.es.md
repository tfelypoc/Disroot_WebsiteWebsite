---
title: 'Talk'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Talk
        logo: en/talk_logo.png
        link: https://f-droid.org/en/packages/com.nextcloud.talk2/
        text:
        platforms: [fa-android]

---
## Talk

Llamadas en conferencia de audio/video. Comunícate de forma segura con tus amigxs o familia utilizando audio y video enriquecidos desde tu navegador. Todas las llamadas son [peer 2 peer](https://es.wikipedia.org/wiki/Peer-to-peer), utilizando tecnología [WebRTC](https://es.wikipedia.org/wiki/WebRTC), así que el único atasco que podría producirse sería por la velocidad de tu internet local antes que por los recursos del servidor de **Disroot**.

Puedes crear un enlace público para compartir con personas que no tienen una cuenta de **Nextcloud**.

---

![](en/call-in-action.png?lightbox=1024)
