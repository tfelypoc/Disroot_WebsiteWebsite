---
title: 'Nextcloud Text'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Texto

Crea archivos [Markdown](https://es.wikipedia.org/wiki/Markdown), y edítalos directamente en línea con vista previa en vivo. También puedes compartirlos y hacer que cualquiera pueda editarlos.

---

![](en/nextcloud-text.png?lightbox=1024)
