---
title: 'Nextcloud Webclient'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/webclient.png)

---

## Access Nextcloud from anywhere

**Nextcloud** is accessible from any device through your web browser. You can therefore use the all the applications on the server directly from your computer, without installing anything but a web browser.
