---
title: 'Zusätzlicher Cloud-Speicherplatz'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Cloud-Speicherplatz

Mit Deinem **Disroot**-Account erhältst Du 2 GB FREIEN Speicher.
Es ist möglich diesen Speicherplatz zu erweitern.

Dies sind die Preise **pro Jahr, Zahlungsgebühren inklusive**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Auf Transaktionen innerhalb der EU wird eine zusätzliche VAT (Value Added Tax) von 21 % fällig.

---

<br><br>

<a class="button button1" href="/forms/extra-storage-space">Zusätzlichen Speicher beantragen</a>
