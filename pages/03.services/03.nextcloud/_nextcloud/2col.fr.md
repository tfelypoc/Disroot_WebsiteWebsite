---
title: Nextcloud
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">S'inscrire sur **Disroot**</a>
<a class="button button1" href="https://cloud.disroot.org/">Se connecter</a>

---
![](Nextcloud_Logo.png)

**Le service Cloud de Disroot** est alimenté par **Nextcloud**. Il vous permet d'héberger et de partager vos fichiers en ligne et dispose de plusieurs fonctionnalités en ligne, telles que le partage d'agenda, la gestion des contacts, les appels vidéo et bien plus encore.

**Nextcloud** offre une solution de partage sûre, sécurisée et conforme aux normes, basée sur des standards compatibles avec tout système d'exploitation.
Plus important encore, toutes vos données sont stockées sur notre instance de cloud chiffrée ! Cela signifie que personne n'est en mesure de voir le contenu de vos fichiers si vous ne l'avez pas explicitement autorisé. Même pas les administrateurs système.

Disroot Cloud: [https://cloud.disroot.org](https://cloud.disroot.org)

Page d'accueil du projet: [https://nextcloud.com](https://nextcloud.com)

Code source: [https://github.com/nextcloud/server](https://github.com/nextcloud/server)
