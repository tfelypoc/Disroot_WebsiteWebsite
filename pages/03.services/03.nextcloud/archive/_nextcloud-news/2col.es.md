---
title: 'Noticias'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Feedreader
        logo: en/rss_logo.png
        link: https://github.com/jangernert/FeedReader
        text:
        platforms: [fa-linux]
    -
        title: News
        logo: en/news_logo.png
        link: https://f-droid.org/en/packages/de.luhmer.owncloudnewsreader/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-news.png?lightbox=1024)

---

## Noticias

[Agregador de noticias](https://es.wikipedia.org/wiki/Agregador) RSS/Atom. Puedes hacer el seguimiento y la gestión de todas tus fuentes en un solo lugar y sincronizarlas a través de múltiples dispositivos y clientes.
