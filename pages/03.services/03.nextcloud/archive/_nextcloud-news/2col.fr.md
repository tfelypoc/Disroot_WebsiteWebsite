---
title: 'News'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Choisissez votre client préféré'
clients:
    -
        title: Feedreader
        logo: en/rss_logo.png
        link: https://github.com/jangernert/FeedReader
        text:
        platforms: [fa-linux]
    -
        title: News
        logo: en/news_logo.png
        link: https://f-droid.org/en/packages/de.luhmer.owncloudnewsreader/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-news.png?lightbox=1024)

---

## News

RSS/Atom feed aggregator. You can keep track and manage all your feeds in one place, and sync them across multiple devices and clients.
