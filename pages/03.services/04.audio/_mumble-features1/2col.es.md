---
title: 'Características de Mumble'
wider_column: left
---

## Fácil de usar

Descarga la aplicación Mumble disponible para tu dispositivo (escritorio, Android o iOS), conéctate a [mumble.disroot.org](mumble.disroot.org) y ya puedes comenzar a crear canales de audio o unirte a uno y charlar con personas.

## Gran calidad de audio

Baja latencia y supresión de ruido lo hacen grandioso para conversar. ¡Puedes tener alrededor de 100 participantes en simultáneo!

## Privado y seguro

Las comunicaciones son cifradas y la validación es, por defecto, a través de claves públicas/privadas. Si utilizas el cliente para escritorio de Mumble, puedes otorgar permisos muy específicos a tus canales, grupos y usuarios y usuarias.

## Aplicación para móvil

Gestiona tus proyectos sobre la marcha con la aplicación para móviles con Android (disponible en F-Droid) o iOS.


---

![](en/connected.png?lightbox=1024)

![](en/notification.png?lightbox=1024)

![](en/acl.png?lightbox=1024)
