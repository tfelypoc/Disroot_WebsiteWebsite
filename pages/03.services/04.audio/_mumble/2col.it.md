---
title: Audio
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://mumble.disroot.org/">Connettiti</a>

---

![mumble_logo](mumble.png?resize=100,100)


## Mumble

L'audio di Disroot è alimentato da Mumble. Mumble è un'applicazione di chat vocale gratuita, open source, a bassa latenza e di alta qualità. Inizialmente era destinato ai giocatori, ma può essere utilizzato per organizzare riunioni audio, conferenze, ecc. 

**NOTE!**

Non è necessario alcun account per utilizzare Mumble. Ma hai più diritti se registri il tuo nome utente. 

Disroot Mumble: [mumble.disroot.org](https://mumble.disroot.org)

Homepage del progetto: [https://www.mumble.info](https://www.mumble.info)

Codice sorgente: [https://github.com/mumble-voip/mumble](https://github.com/mumble-voip/mumble)
