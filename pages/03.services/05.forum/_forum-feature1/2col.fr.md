---
title: 'Caractéristique n.1'
wider_column: right
---

![](en/discourse_feature1.png?lightbox=1024)

---
# Liste de diffusion et forum d'un seul coup
Outre le forum, Discourse peut également être utilisé comme liste de diffusion. Ceci résout la lutte éternelle entre les défenseurs de l'une ou l'autre option dans votre communauté et permet à tout le monde d'en profiter et de communiquer les uns avec les autres de la façon qu'ils pensent être la meilleure. Vous pouvez commencer de nouveaux sujets et y répondre en envoyant un courriel ou en utilisant l'interface web.

# Interface mobile conviviale
Discourse a été conçu pour les appareils tactiles haute résolution, avec une disposition mobile intégrée. Pas besoin d'une application externe. Vous exécuterez toujours la dernière version de Discourse puisque vous n'avez pas besoin de le mettre à jour manuellement.

# Groupes publics ou privés
Vous pouvez créer des groupes de discussion publics ou privés qui sont modérés par vous-même. En tant qu'administrateur de groupe, vous pourrez gérer qui participera à votre groupe de discussion.
