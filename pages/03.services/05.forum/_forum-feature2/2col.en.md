---
title: 'Feature 2'
wider_column: left
---

## A nice user interface:
The user interface is very practical:

<ul class=disc>
<li>Dynamic notifications will give you heads up whenever someone replies to your post, quotes you or mentiones your name, and if you aren't online, you'll get a notification via email.</li>
<li>No more discussions that are split into pages, the thread reveals while you scroll down.</li>
<li>Reply while you still read. With split screen you can still read other replies or threads, and dynamically add quotes, links and references.</li>
<li>Real time updates. If a topic changes while you are reading or responding to it, or new replies arrive, it will update automatically.</li>
<li>Search from any page – just start typing and get results instantly as you type.</li>
</ul>

---

![](en/discourse_feature2.png?lightbox=1024)
