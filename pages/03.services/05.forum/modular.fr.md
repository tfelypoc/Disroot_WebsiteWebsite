---
title: Forum
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _forum
            - _forum-highlights
            - _forum-feature1
            - _forum-feature2
body_classes: modular
header_image: forum_banner.jpeg

translation_banner:
    set: true
    last_modified: Mars 2022
    language: French
---
