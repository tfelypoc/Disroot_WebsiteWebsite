---
title: 'File storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](drive.png)

## Verschlüsselter Datenspeicher
Uploade und teile jede beliebige Datei. Alle gespeicherten Dateien sind Ende-zu-Ende verschlüsselt!



---
![](code.png)

## Kollaborativer Code editor
Bearbeite Deinen Code mit Deinen Teammitgliedern. Da er Ende-zu-Ende verschlüsselt ist, kann der Service-Provider keine Einsicht in Deine Daten nehmen.
