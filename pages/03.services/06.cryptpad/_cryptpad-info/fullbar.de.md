---
title: 'Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Jeder Nutzer erhält **250MB Speicherplatz** für Dateien.

---

**Obwohl die Nutzung von Cryptpad keinen Account erfordert, kannst Du einen erstellen, wenn Du all Deine Pads, Dateien und Umfragen an einem Platz behalten willst. Du musst für Cryptpad einen separaten Account erstellen. Dein Disroot-Account wird nicht funktionieren!**

---
