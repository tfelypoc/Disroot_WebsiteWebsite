---
title: 'Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Every user gets **250MB storage space** for files.

---

**Although Cryptpad does not require an account, you can create one if you want to keep all your pads, files, and polls in one place. You will need to create a separate account for Cryptpad. Your disroot account won't work!**

---
