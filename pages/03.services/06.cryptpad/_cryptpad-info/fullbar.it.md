---
title: 'Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
Ogni utente riceve **250 MB di spazio di archiviazione** per i file. 

---

**Sebbene Cryptpad non richieda un account, puoi crearne uno se desideri conservare tutti i tuoi pad, file e sondaggi in un unico posto. Dovrai creare un account separato per Cryptpad. Il tuo account disroot non funzionerà!** 

---
