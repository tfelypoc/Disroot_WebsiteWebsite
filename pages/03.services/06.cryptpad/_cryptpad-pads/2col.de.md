---
title: 'Pads'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](pads.png)

## Rich Text Pads
Der **Rich text** Editor ermöglicht es Dir, Ende-zu-Ende verschlüsselte Textdateien mit Echtzeit-Kollaborationsfunktion zu erstellen. Der Editor bietet Standardformatierungen. Alle Pads können von mehreren Leuten zur selben Zeit bearbeitet werden.


---
![](sheet.png)

## Echtzeit-Tabellenkalkulation
**Echtzeit-Tabellenkalkulation** ermöglicht es Dir, kollaborative Tabellenkalkulationen zu erstellen. Die Anwendung unterstützt alle Standardformatierungen (Libre Office/MS Office) und die Bearbeitung durch Gruppen. All das wird ermöglicht durch das fantastische *"Only Office"*.
