---
title: 'Pads'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](pads.png)

## Rich text pads
**Rich text** editor allows you to create end-to-end encrypted text files with realtime collaboration feature. Editor allows standard formating. All pads can be edited with multiple people at the same time.


---
![](sheet.png)

## Realtime spreadsheet
**Realtime spreadsheets** allow you to create collaborative spreadsheet. Application supports all standard (libreoffice/MS office) formatting. All that powered by amazing *"Only Office"* and allowing for group editing.
