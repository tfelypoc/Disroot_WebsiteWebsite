---
title: Cryptpad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">Sign up</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Create Pads</a>

---
![](cryptpad_logo.png)

## Cryptpad

Cryptpad di Disroot è alimentato da Cryptpad. Fornisce una suite per ufficio collaborativa e crittografata completamente end-to-end. Ti consente di creare, condividere e lavorare insieme su documenti di testo, fogli di calcolo, presentazioni, lavagne o organizzare il tuo progetto su una lavagna kanban.

Disroot Cryptpad: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Homepage del progetto: [https://cryptpad.fr](https://cryptpad.fr)

Codice sorgente: [https://github.com/xwiki-labs/cryptpad](https://github.com/xwiki-labs/cryptpad)
