---
title: 'Xmpp Direct Messaging'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](monal.jpg)

---

## 1 on 1 chats

Talk freely with any other Disrooter or anyone out there using any XMPP compatible server on the internet. You decide if to store any history of the chat on the server or enable encryption. Usernames on XMPP resemble an email address corresponding with the XMPP server (username@example.com).  
