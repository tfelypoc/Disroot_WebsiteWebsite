---
title: 'Xmpp Bridges'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations2.png)

---

## Gateways, Bridges and Transports

XMPP allows for various ways to connect to different chat protocols.

### Connect to a IRC room
You can connect to any IRC room hosted on any IRC server via our Biboumi IRC gateway:
<ul class=disc>
<li>To join an IRC room: <b><i>#room%irc.domain.tld@irc.disroot.org</i></b></li>
<li>To add an IRC contact: <b><i>contactname%irc.domain.tld@irc.disroot.org</i></b></li>
</ul>

Where ***#room*** is the IRC room you want to join and ***irc.domain.tld*** is the IRC server address you want to join. Make sure to leave ***@irc.disroot.org*** as it is, because it's the IRC gateways address.

### Connect to a Matrix room
You can connect to any Matrix room hosted on any Matrix server via Matrix.org bridge:
<ul class=disc>
<li>To join a Matrix room: <b><i>#room#matrix.domain.ldt@matrix.org</i></b></li>
</ul>

Where ***#room*** is the Matrix room you want to join and **matrix.domain.tld*** is the Matrix server address you want to join. Make sure to leave ***@matrix.org*** as it is, because it's the Matrix bridge address.


### To come...
In the future we are planning to run our own Matrix bridge. We also hope to provide a Telegram bridge and a number of other bridges and transports.
