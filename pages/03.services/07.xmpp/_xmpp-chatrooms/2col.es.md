---
title: 'Salas de chat XMPP'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Salas de chat

Con XMPP puedes crear y unirte a salas públicas o privadas en la red. Las direcciones de las salas son simples de recordar y se parecen a la dirección de correo de una lista: *tu_sala*@chat.disroot.org. Adicionalmente, puedes establecer varias configuraciones de privacidad por sala, como por ejemplo: si el historial de la sala debería almacenarse en el servidor, si la sala puede buscarse públicamente y está abierta para que cualquiera pueda unirse, puedes designar administradorxs y moderadorxs para la sala y habilitar el cifrado de la misma.

Puedes revisar la [lista global de salas](https://search.jabbercat.org/rooms/1) de todos los servidores que voluntariamente envían sus salas públicas al directorio global.


---

## <br> ##
![](chatrooms.png)
