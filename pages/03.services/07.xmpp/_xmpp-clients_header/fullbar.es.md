---
title: 'Clientes XMPP'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Usa tu cliente de chat preferido
Hay un montón de clientes para el escritorio/la web/el móvil para elegir. Escoge el que más te guste.<br>
[https://xmpp.org/software/clients.html](https://xmpp.org/software/clients.html)
