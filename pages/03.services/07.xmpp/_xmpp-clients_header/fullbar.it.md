---
title: 'Jabber Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Usa il tuo client di chat preferito

Ci sono diversi client per desktop, web e mobile tra cui scegliere. Utilizza quello che preferisci.

[https://xmpp.org/software/clients.html](https://xmpp.org/software/clients.html)
