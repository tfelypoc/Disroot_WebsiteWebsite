---
title: 'Cifrado XMPP'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## ¡Cifra todo!

Usando los métodos de cifrado Punto-a-Punto OMEMO (GPG o OTR) en el lado del cliente (tu lado), tus mensajes llegarán al destinatario sin ser interceptados por cualquiera (ni siquiera los administradores).  

---

![](omemo_logo.png)
