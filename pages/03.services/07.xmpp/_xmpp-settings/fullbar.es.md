---
title: 'Configuraciones XMPP'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Configuración del servidor
### ID XMPP:<span style="color:#8EB726"> <i>tu_nombre_de_usuarix</i>@disroot.org </span>
##### Configuración avanzada: servidor/puerto<span style="color:#8EB726"> disroot.org/5222 </span>

---

#### Tamaño máximo de archivo para subir:<span style="color:#8EB726"> 512 MB </span>
#### Los mensajes y ficheros archivados expiran luego de <span style="color:#8EB726"> 1 meses </span>
<br>
