---
title: 'Impostazioni Xmpp'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Impostazioni server
### XMPP ID:<span style="color:#8EB726"> <i>tuo username</i>@disroot.org </span>
### Impostazioni avanzate: server:<span style="color:#8EB726"> disroot.org/5222 </span>

---

#### Limite file in upload:<span style="color:#8EB726"> 512 MB </span>
#### Messaggi e file archiviati verranno eliminati dopo <span style="color:#8EB726"> 1 mesi </span>
<br>
