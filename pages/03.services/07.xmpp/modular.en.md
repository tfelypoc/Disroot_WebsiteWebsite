---
title: XMPP Chat
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _xmpp
            - _xmpp-settings
            - _xmpp-highlights
            - _xmpp-clients_header
            - _xmpp-clients
            - _xmpp-encrypt
            - _xmpp-1on1
            - _xmpp-chatrooms
            - _xmpp-bridges
            - _xmpp-bots
body_classes: modular
header_image: everyones-welcome.jpg
---
