---
title: 'Características de Ethepad'
text_align: left
wider_column: right
---

![](en/etherpad-history.gif)

---

## Importar / exportar Blocs de notas

Importa y exporta blocs de notas en varios formatos (texto plano, HTML, Etherpad, Mediawiki).

## Guardar correcciones

Guarda una versión en particular de tu bloc de notas.

## Marcadores

Guarda localmente una lista de marcadores de tus blocs visitados en el almacenamiento local del navegador.

## Comentarios

Una barra lateral para agregar comentarios que están vinculados directamente al texto.
