---
title: "Caractéristiques d'Ethercalc"
text_align: left
wider_column: right
---

![](en/etherpad-history.gif)

---

## Importer / exporter des pads

Importer et exporter des pads en différents formats (texte brut, HTML, Etherpad, Mediawiki).

## Sauvegarder les révisions

Sauvegardez des versions particulières de votre bloc-notes.

## Signets

Le plugin Etherpad permet aux utilisateurs d'enregistrer localement une liste de signets des pads visités dans le stockage local du navigateur.

## Commentaires

Une barre latérale pour ajouter des commentaires qui sont directement liés au texte.
