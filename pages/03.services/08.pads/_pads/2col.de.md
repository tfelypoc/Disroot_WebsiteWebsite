---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Ein Pad öffnen</a>

---

![](etherpad.png)
## Gemeinschaftliches Bearbeiten in wirklicher Echtzeit
Disroots Pads basiert auf **Etherpad**. Ein Pad ist ein Onlinetext, den Du gemeinschaftlich mit anderen bearbeiten kannst, in Echtzeit, direkt im Browser. Die Änderungen jedes Benutzers werden sofort auf dem Bildschirm dargestellt.

Schreibe Artikel, Pressemitteilungen, To-Do-Listen, etc. zusammen mit Deinen Freunden, Mitstudenten oder Kollegen.

Du benötigst keinen Disroot-Account, um diesen Service zu nutzen.

Du kannst [**Padland**](https://f-droid.org/de/packages/com.mikifus.padland/) auf Android nutzen, um Deine Disroot-Pads direkt auf Deinem Android-Gerät zu öffnen oder zu erstellen.


Disroot Pads: [https://pad.disroot.org](https://pad.disroot.org)

Projekt-Website: [http://etherpad.org](http://etherpad.org)

Quellcode: [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)
