---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Open a pad</a>

---

![](etherpad.png)
## Collaborative editing in really real-time
Disroot's pads are powered by **Etherpad**. A pad is an online text that you can collaboratively edit, in real-time, directly in the web browser. Everybody's changes are instantly reflected on all screens.

Write articles, press releases, to-do lists, etc. together with your friends, fellow students or colleagues.

You don't need any account on Disroot to use this service.

You can use [**Padland**](https://f-droid.org/en/packages/com.mikifus.padland/) on Android to directly open or create your Disroot's pads on your Android device.


Disroot Pads: [https://pad.disroot.org](https://pad.disroot.org)

Project homepage: [http://etherpad.org](http://etherpad.org)

Source code: [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)
