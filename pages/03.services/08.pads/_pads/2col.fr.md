---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Ouvrir un pad</a>

---

![](etherpad.png)
## Edition collaborative en temps réel
Les pads de Disroot sont alimentés par **Etherpad**. Un pad est un texte en ligne que vous pouvez éditer de manière collaborative, en temps réel, directement dans le navigateur web. Les modifications de chacun sont instantanément répercutées sur tous les écrans.

Rédigez des articles, des communiqués de presse, des listes de choses à faire, etc. avec vos amis, collègues ou étudiants.

Vous n'avez pas besoin d'un compte sur Disroot pour utiliser ce service.

Vous pouvez utiliser [**Padland**](https://f-droid.org/en/packages/com.mikifus.padland/) sur Android pour ouvrir ou créer directement les pads Disroot sur votre appareil Android.


Disroot Pads : [https://pad.disroot.org](https://pad.disroot.org)

Page d'accueil du projet : [http://etherpad.org](http://etherpad.org)

Le code source : [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)
