---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Apri un pad</a>

---

![](etherpad.png)
## Editor collaborativo in tempo reale

Etherpad permette di editare documenti in modo collaborativo e in modo sincrono direttamente dall'interfaccia web.
Scrivi in modo collettivo e sincrono con i tuoi amici o colleghi un articolo, un comunicato stampa o una to-do list.


[https://pad.disroot.org](https://pad.disroot.org)

Pagina del progetto: [http://etherpad.org](http://etherpad.org)

[Codice sorgente](https://github.com/ether/etherpad-lite)
