---
title: 'Easy'
wider_column: right
---

![](en/privatebin01.gif)

---

# Easy to use
<ul class=disc>
<li>Paste text, click “Send”, share the URL.</li>
<li>Set an expiration limit: 5 minutes, 10 minutes, 1 hour, 1 day, 1 week, 1 month, 1 year or never.</li>
<li>“Burn after reading” option: The paste is destroyed when read.</li>
<li>Unique deletion URL generated for each paste.</li>
<li>Syntax coloring for 54 languages (using highlight.js), supporting mixing (html/css/javascript).</li>
<li>Automatic conversion of URLs into clickable links (http, https, ftp and magnet).</li>
<li>Single button to clone an existing paste.</li>
<li>Discussions: You can enable discussion on each paste.</li>
</ul>
