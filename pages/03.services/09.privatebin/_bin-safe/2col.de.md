---
title: 'Sicher'
wider_column: left
---

# Sicher und Geschützt
<ul class=disc>
<li>Daten werden im Browser komprimiert und mit 256 bit AES verschlüsselt, bevor sie an den Server gesendet werden. Der Server hat keine Ahnung, welche Daten gespeichert werden.</li>
<li>Deine Daten sind sicher, selbst bei einem unbefugten Zugriff auf den Server oder dessen Beschlagnahme.</li>
<li>Suchmaschinen sind blind für Pastebin-Inhalte.</li>
<li>Diskussionen werden ebenfalls im Browser ver- und entschlüsselt.</li>
<li>Der Server kann Kommentarinhalte oder Nicknames nicht einsehen.</li>
<li>Mit Hilfe des Verfallsdatums kannst Du Ad-hoc-Kurzzeit-Diskussionen führen, die nach Ablauf automatisch im Nirwana verschwinden, ohne Spuren Eurer Diskussionen in Euren Email-Postfächern zu hinterlassen.</li>
<li>Diskussionen oder Paste-Inhalt können durch Suchmaschinen nicht indiziert werden.</li>
</ul>
---


![](de/privatebin02.gif)
