---
title: Bin
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _bin
            - _bin-highlights
            - _bin-easy
            - _bin-safe
body_classes: modular
header_image: bin_banner.jpg

translation_banner:
    set: true
    last_modified: März 2022
    language: Spanish
---
