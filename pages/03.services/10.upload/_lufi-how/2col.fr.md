---
title: 'Comment utiliser Lufi'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Comment ça marche?

Faites glisser et déposez les fichiers dans la zone appropriée ou utilisez la méthode traditionnelle pour parcourir et sélectionner les fichiers et les fichiers seront découpés, chiffrés et envoyés au serveur. Vous obtiendrez deux liens par fichier: un lien de téléchargement, que vous donnerez aux personnes avec lesquelles vous souhaitez partager le fichier et un lien de suppression, vous permettant de supprimer le fichier quand vous le souhaitez.
Vous pouvez voir la liste de vos fichiers en cliquant sur le lien "Mes fichiers" en haut à droite de cette page.

Vous n'avez pas besoin de vous enregistrer pour uploader des fichiers.

---

![](en/lufi-drop.png?lightbox=1024)
