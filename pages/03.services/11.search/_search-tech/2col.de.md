---
title: Suche
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: left
---

# Wie funktioniert es?

Searx erstellt selbst keine Indizes von Websites. Wenn Du Deine Suchanfrage in die Suchbox eingibst, vermittelt Searx diese Anfrage an diverse andere Suchmaschinen wie Google, DuckDuckGo, Bing, etc. weiter und gibt Dir die Suchergebnisse gesammelt zurück. Searx tritt dabei bei den Suchmaschinen als Anfragesteller auf, so dass Deine Daten nicht zu den Suchmaschinenbetreibern gelangen. Du bleibst anonym für die Datenkraken.

Searx bietet Dir zwar nicht so personalisierte Ergebnisse wie Google, das liegt jedoch daran, dass Searx kein Benutzerprofil von Dir anlegt oder gar persönliche Informationen über Dich, Deinen Standort oder Dein Endgerät an die beteiligten Suchmaschinen weitergibt. Dies bietet Dir im Gegenzug viel mehr Privatsphäre und wirkt wie ein 'Schutzschild' gegen die großen Konzerne, die Dich ausspionieren wollen.

[Hier](https://search.disroot.org/preferences) kannst Du Dir anschauen, welche Suchmaschinen Du nutzen kannst, um Deine Suchergebnisse zu erhalten.
