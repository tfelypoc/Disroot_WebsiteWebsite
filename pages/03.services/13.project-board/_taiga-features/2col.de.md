---
title: 'Taiga-Funktionen'
wider_column: left
---

## Kanban-Board

Für diejenigen, die bisher nichts mit Projektmanagement-Tools und Scrum zu tun hatten, empfehlen wir, eine simple Kanban-Vorlage zu wählen (To-Do > In Arbeit > Fertig).<br>[Kanban](https://de.wikipedia.org/wiki/Kanban)


## Scrum-Projekt

Mit Backlog, Sprintplanung, User-Story-Punkten und all den anderen Annehmlichkeiten, die jedes Scrum-Projekt benötigt.<br>[Software-Entwicklung](https://de.wikipedia.org/wiki/Scrum)


## Fragen-Board

Ein Ort, an dem du all die Fragen, Anregungen, Probleme, etc. sammeln und gemeinsam an ihrer Lösung arbeiten kannst.


## Wiki

Dokumentiere Deine Arbeit mit einem integrierten Wiki.


## Epics

Bündele Aufgaben in Epics, die Du über die Zeit erfüllen kannst, sogar über mehrere Projekte.

---


![](de/taiga-backlog.png?lightbox=1024)

![](de/taiga-kanban.png?lightbox=1024)
