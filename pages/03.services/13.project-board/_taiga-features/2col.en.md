---
title: 'Taiga Features'
wider_column: left
---

## Kanban board

For those unfamiliar with project management tools and scrum, we advice to choose simple Kanban template (Todo > In progress > Done).  [https://en.wikipedia.org/wiki/Kanban](https://en.wikipedia.org/wiki/Kanban)


## Scrum Project

With backlog, sprint planning, user story points and all the other goodies every Scrum project needs. [Software development](https://en.wikipedia.org/wiki/Scrum_(software_development))

## Issue board

Place where you can collect all the issues, suggestions, problems etc. and work collectively to resolve them.


## Wiki

Document your work with integrated wiki.


## Epics
Bundle tasks into epics that you can accomplish over time even across multiple projects.

---

![](en/taiga-backlog.png?lightbox=1024)

![](en/taiga-kanban.png?lightbox=1024)
