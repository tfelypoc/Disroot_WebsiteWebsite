---
title: Taiga
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Sign up for Disroot</a>
<a class="button button1" href="https://board.disroot.org/">Log in</a>

---
![taiga_logo](taiga.png)

## Project management board

Disroot's Project board is a project management tool powered by **Taiga**, working with agile methodology in mind. It creates a clear, visual overview of the current state of your project to anyone involved. It makes planning very easy and it keeps you and your team focused on tasks. Simply create a project, invite your group members, create tasks and put them on the board. Decide who will take responsibility for the tasks, follow progress, comment, decide and see your project flourish.

Disroot Board: [https://board.disroot.org](https://board.disroot.org)

Project homepage: [https://taiga.io/](https://taiga.io/)

Source code: [https://github.com/taigaio](https://github.com/taigaio)
