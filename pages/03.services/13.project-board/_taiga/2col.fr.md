---
title: Taiga
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">S'inscrire sur Disroot</a>
<a class="button button1" href="https://board.disroot.org/">Se connecter</a>

---
![taiga_logo](taiga.png)

## Tableau de gestion de projet

Le Tableau de gestion de projet de Disroot est un outil de gestion de projet développé par **Taiga**, travaillant avec une méthodologie agile en tête. Il crée une vue d'ensemble claire et visuelle de l'état actuel de votre projet pour toutes les personnes impliquées. Cela rend la planification très facile et vous permet, à vous et à votre équipe, de rester concentrés sur les tâches à accomplir. Créez simplement un projet, invitez les membres de votre groupe, créez des tâches et mettez-les dans le tableau. Décidez qui assumera la responsabilité des tâches, suivez l'avancement, commentez, décidez et voyez votre projet s'épanouir.

Tableau de projet Disroot: [https://board.disroot.org](https://board.disroot.org)

Page d'accueil du projet: [https://taiga.io/](https://taiga.io/)

Code source: [https://github.com/taigaio](https://github.com/taigaio)
