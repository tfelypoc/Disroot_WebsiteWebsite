---
title: 'Projekt-Board'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _taiga
            - _taiga-highlights
            - _taiga-features
body_classes: modular
header_image: power_people.jpeg

translation_banner:
    set: true
    last_modified: März 2022
    language: German
---
