---
title: 'Project Board'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _taiga
            - _taiga-highlights
            - _taiga-features
body_classes: modular
header_image: power_people.jpeg
---
