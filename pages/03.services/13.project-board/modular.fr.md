---
title: 'Tableau de projet'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _taiga
            - _taiga-highlights
            - _taiga-features
            - _taiga-support
            - _taiga-blog
body_classes: modular
header_image: power_people.jpeg

translation_banner:
    set: true
    last_modified: Mars 2022
    language: French
---
