---
title: 'Jitsi-Clients'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: Jitsi Web
        logo: jitsi_circle.png
        link: https://calls.disroot.org
        text:
        platforms: [fa-windows, fa-linux, fa-apple]
    -
        title: Jitsi Desktop
        logo: ../_calls/jitsi_logo.png
        link: https://github.com/jitsi/jitsi-meet-electron/#installation
        text:
        platforms: [fa-windows, fa-linux, fa-apple]

    -
        title: Jitsi Android
        logo: android_logo.png
        link: https://play.google.com/store/apps/details?id=org.jitsi.meet&hl=de
        text:
        platforms: [fa-android]

    -
        title: Jitsi Apple
        logo: apple_logo.png
        link: https://apps.apple.com/in/app/jitsi-meet/id1165103905
        text:
        platforms: [fa-apple]


---

<div class=clients markdown=1>

</div>
