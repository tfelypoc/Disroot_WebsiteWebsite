---
title: 'Jitsi-Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Nutz Deine bevorzugte App
Es gibt verschiedene Desktop-/Web-/mobile Clients, zwischen denen Du wählen kannst. Such Dir den aus, den Du am meisten magst.
