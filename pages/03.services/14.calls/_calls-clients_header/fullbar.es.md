---
title: 'Clientes para Jitsi'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Usa tu aplicación preferida
Hay un montón de clientes para el escritorio/la web/el móvil para elegir. Escoge el que más te guste.
