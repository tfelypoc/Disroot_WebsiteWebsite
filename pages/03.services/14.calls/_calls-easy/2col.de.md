---
title: 'Einfach zu nutzen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Einfach zu nutzen
Starte einfach eine Konferenz und teile den Link mit anderen Mitgliedern.
Du benötigst nicht mal einen Account für die Nutzung und der Dienst läuft in Deinem Browser, ganz ohne die Installation zusätzlicher Software auf Deinem Computer.

---

![](easy.png)
