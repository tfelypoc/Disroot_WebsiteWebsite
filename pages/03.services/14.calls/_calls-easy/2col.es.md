---
title: 'Fácil de usar'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Fácil de usar
Simplemente inicia una reunión y comparte el enlace con otrxs usuarixs.
Ni siquiera necesitas una cuenta para usarlo y puede ejecutarse en tu navegador sin instalar ningún programa adicional en tu computadora.

---

![](easy.png)
