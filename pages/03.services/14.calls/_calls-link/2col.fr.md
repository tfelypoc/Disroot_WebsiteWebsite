---
title: 'Custom link'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](link.png)

---

# Lien personnalisé
Vous pouvez choisir votre propre nom de salle de conférence, qui sera l'adresse de votre conférence.
