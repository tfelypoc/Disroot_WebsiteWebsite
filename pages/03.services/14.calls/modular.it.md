---
title: Calls
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _calls
            - _calls-highlights
            - _calls-clients_header
            - _calls-clients
            - _calls-easy
            - _calls-link
            - _calls-chat

body_classes: modular
header_image: 'calls-banner.png'

translation_banner:
    set: true
    last_modified: Marzo 2022
    language: Italian
---
