---
title: 'Características de Git'
wider_column: left
---

## Fácil de usar
Revisa los repositorios públicos, encuentra un proyecto en el que quieras trabajar y clónalo en tu computadora. ¡O crea el tuyo propio! Puedes configurarlo como un repositorio público o privado.

## Rápido y liviano
**Gitea** ofrece una experiencia muy veloz y liviana. Sin características infladas o configuraciones demasiado técnicas. ¡Solo las cosas que son esenciales para colaborar en tu proyecto de código abierto!

## Notificaciones
Recibe correos cuando un problema fue resuelto, una solicitud es creada, etc.

## Seguro
Utiliza claves SSH, autenticación en dos pasos y GnuPG para asegurar tu repositorio.


---

![](en/gitea_explore.png?lightbox=1024)

![](en/gitea_secure.png?lightbox=1024)
