---
title: 'Git Funktionalitäten'
wider_column: left
---

![](de/gitea_dashboard.png?lightbox=1024)

![](de/gitea_activity.png?lightbox=1024)


---

## Problem-Nachverfolgung
Verwalte Problemstellungen, Funktionalitäten-Anfragen und Rückmeldungen zu Deinem Projekt.

## Tags, Bevollmächtigte, Meilensteine, Kommentare
Verwalte Dein Repository effizient, indem Du Meilensteine festlegst, Deine Problemstellungen sortierst, diskutierst und Aufgaben an Deine Team-Mitglieder verteilst.

## Wiki
Erstelle ein Wiki, um Dein Projekt zu erklären, und nutze Markdown.

## Mobile-App
Verwalte Dein Projekt von unterwegs mit der Mobile-App für Android (erhältlich auf [F-Droid](https://f-droid.org/de/packages/org.mian.gitnex/)).
