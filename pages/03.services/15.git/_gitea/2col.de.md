---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Registrieren</a>
<a class="button button1" href="https://git.disroot.org/">Anmelden</a>

---

![gitea_logo](gitea.png?resize=100,100)


## Gitea

**Disroot's Git** wird mit **Gitea** betrieben. **Gitea** ist eine gemeinschaftsbetriebene, mächtige, einfach zu benutzende und leichtgewichtige Lösung für Code-Hosting und Projekt-Kollaboration. Es baut auf der GIT-Technologie auf, welche aktuell das meistgenutzte moderne Versionskontrollsystem der Welt ist.

Disroot Git: [https://git.disroot.org](https://git.disroot.org)

Projekt-Seite: [https://gitea.io](https://gitea.io)

Quellcode: [https://github.com/go-gitea/gitea](https://github.com/go-gitea/gitea)

<hr>

Du musst auf [git.disroot.org](https://git.disroot.org/) einen eigenen Account erstellen, um diese Anwendung zu nutzen. Deine **Disroot**-Zugangsdaten werden nicht unterstützt.
