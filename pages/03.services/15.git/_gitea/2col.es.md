---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Sign up</a>
<a class="button button1" href="https://git.disroot.org/">Log in</a>

---

![gitea_logo](gitea.png?resize=100,100)


## Gitea

**Git de Disroot** está desarrollado por **Gitea**. **Gitea** es una solución impulsada por la comunidad, poderosa, liviana y fácil de usar, para hospedar código y proyectos colaborativos. Está construida sobre la tecnología [GIT](https://es.wikipedia.org/wiki/Git), que actualmente es el más moderno y ampliamente utilizado sistema de control de versiones en el mundo.

Git de Disroot: [https://git.disroot.org](https://git.disroot.org)

Página del proyecto: [https://gitea.io](https://gitea.io)

Código fuente: [https://github.com/go-gitea/gitea](https://github.com/go-gitea/gitea)

<hr>

Necesitas crear una cuenta aparte en [git.disroot.org](https://git.disroot.org/) para utilizar el servicio. Las credenciales de **Disroot** no son compatibles.
