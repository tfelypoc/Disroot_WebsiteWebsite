---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Registrati</a>
<a class="button button1" href="https://git.disroot.org/">Accedi</a>

---

![gitea_logo](gitea.png?resize=100,100)


## Gitea

**Disroot Git** è alimentato da **Gitea**. **Gitea** è una soluzione guidata dalla community, potente, facile da usare e leggera per l'hosting di codice e la collaborazione di progetti. È basato sulla tecnologia GIT, che è il sistema di controllo versione moderna più utilizzato al mondo oggi.

Disroot Git: [https://git.disroot.org](https://git.disroot.org)

Pagina del progetto: [https://gitea.io](https://gitea.io)

Codice sorgente: [https://github.com/go-gitea/gitea](https://github.com/go-gitea/gitea)

<hr>

Per utilizzare [git.disroot.org](https://git.disroot.org/) necessiti di creare un account specifico. Le credenziali degli altri servizi di **Disroot** non sono supportate.
