---
title: 'Gitea'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _gitea
            - _gitea-highlights
            - _gitea-features1
            - _gitea-features2
body_classes: modular
header_image: Hackers_Junction_2015_by_vmuru.jpg

translation_banner:
    set: true
    last_modified: Marzo 2022
    language: Italian
---
