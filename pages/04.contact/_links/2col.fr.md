---
title: Contact
bgcolor: '#1F5C60'
fontcolor: '#FFF'
process:
    markdown: true
    twig: true
twig_first: false
---

[![](email.png?classes=contact)](mailto:support@disroot.org) support@disroot.org

[![](email.png?classes=contact,pull-left)](mailto:cryptsupport@disroot.org) <div> <br/> cryptsupport@disroot.org <br/> GPG: <a class="lighter" href="https://keyserver.ubuntu.com/pks/lookup?search=cryptsupport%40disroot.org&fingerprint=on&op=index"><b>server.ubuntu.com</b></a><br/> Empreinte: FFC9 DCE6 5A96 98B0 FE49  EA83 6734 AE05 298C 41E8 </div>

[![](mastodon.png?classes=contact)](https://social.weho.st/@disroot) Mastodon:  <a class="lighter" href="https://social.weho.st/@disroot/">social.weho.st/@disroot</a>

---

![](irc.png?classes=contact) IRC: #disroot sur irc.libera.chat

[![](xmpp.png?classes=contact)](xmpp:disroot@chat.disroot.org?join) Xmpp: <a class="lighter" href="xmpp:disroot@chat.disroot.org?join">disroot@chat.disroot.org/@disroot</a>

[![](webchat.png?classes=contact)](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org) Xmpp <a class="lighter" href="https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org">webchat</a>

[![](nextcloud.png?classes=contact)](https://cloud.disroot.org/call/di5y9zno
) NextCloud: <a class="lighter" href="https://cloud.disroot.org/call/di5y9zno">cloud.disroot.org/call/di5y9zno</a>


---

Tous les salons de discussion répertoriés sont reliés entre eux. Cela signifie que, quel que soit le salon de discussion que vous choisissez dans cette liste, vous pourrez communiquer avec toute la communauté par l'intermédiaire d'un robot de relais de discussion.
