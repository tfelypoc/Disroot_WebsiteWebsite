---
title: 'Roadmap'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

# Community

---      

**Transparenz** spielt eine große Rolle hier bei **Disroot**.
Wir wollen offen und ehrlich über unsere Pläne, Ideen und aktuellen Fortschritte berichten.

Wir wollen außerdem Beiträge aus der **Disroot**-Gemeinschaft zum Projekt fördern.
Daher haben wir ein Bündel mit Werkzeugen gepackt, um es jedem einfach zu machen, **Disroots** Entwicklung zu verfolgen und aktiv daran mitzuwirken durch Diskussionen, Fehlerberichte und das Einreichen von Ideen und Verbesserungen.
