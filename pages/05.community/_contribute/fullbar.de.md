---
title: Mitwirken
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
published: true
---

![](bug.png?resize=45,45&classes=pull-left)<br>
# Wie kann ich helfen?

Es gibt viele Möglichkeiten, aktiv zu werden und beim **Disroot**-Projekt mitzuwirken.
