---
title: 'Come posso aiutare'
bgcolor: '#FFF'
fontcolor: '#555'
---

### Inviare e tenere traccia dei bug
Non importa se hai usato Disroot per molto tempo o se ti sei appena iscritto, la segnalazione di bug è qualcosa che puoi sempre fare. Quindi, se si scopre qualcosa che non ha l'aspetto giusto o non funziona come dovrebbe - faccelo sapere su [**Disroot-Project repository**](https://git.disroot.org/Disroot/Disroot-Project/issues)..

### Proporre nuove funzionalità
È inoltre possibile utilizzare [**Disroot-Project repository**](https://git.disroot.org/Disroot/Disroot-Project/issues) per farci sapere quali nuove funzionalità vorresti vedere in futuro.

![](theme://images/logo_project_board.png?resize=35,35) [Project board](https://board.disroot.org/project/disroot-disroot/?classes=button5)

---

### Discussioni sul forum
Prima di inviarci un'e-mail con domande o problemi, cerca nel [forum](https://forum.disroot.org/c/disroot) per vedere se c'è già una risposta. Se non riesci ancora a trovare una soluzione al tuo problema, inserisci la tua domanda nella sezione Supporto nel forum. Nella maggior parte dei casi altri Disrooters saranno in grado di rispondere alla tua domanda, il che è di grande beneficio per tutti.

![](theme://images/logo_forum.png?resize=35,35) [Disroot Forum](https://forum.disroot.org/c/disroot?classes=button5)


### Aiutare a risolvere i problemi di altre persone
Se si utilizza Disroot da qualche tempo, si ha familiarità con la piattaforma o si è dovuto risolvere alcuni problemi prima, allora si può essere di grande aiuto ad altri che stanno lottando per trovare la loro strada intorno a tutte le caratteristiche. È anche un grande aiuto per noi se più Disrooters aiutano a rispondere alle domande e a risolvere problemi minori per gli altri. Significa che il carico di lavoro è meglio distribuito e le risposte possono apparire più velocemente. Inizia a guardare il forum Disroot e il nostro canale a matrice per vedere come puoi aiutare.

![](theme://images/logo_chat.png?resize=35,35) [disroot@chat.disroot.org](xmpp:disroot@chat.disroot.org?classes=button5)


---

### Aiutaci con le guide
Abbiamo già scritto parecchie [guide e tutorial] (https://howto.disroot.org) per aiutare le persone che vogliono spostare la loro presenza online su Disroot, ma c'è ancora molto da fare prima di riuscire a coprire tutti gli aspetti dei servizi forniti, per tutti i possibili dispositivi e sistemi operativi. Abbiamo bisogno di più "howto writer" se vogliamo creare una documentazione universale che possa essere utilizzata per familiarizzare con l'ambiente Disroot e altre piattaforme auto-ospitate.

![](theme://images/logo_h2.png?resize=35,35) [Disroot Howto Project](https://howto.disroot.org?classes=button5)
![](theme://images/logo_chat.png?resize=35,35) [howto@chat.disroot.org](xmpp:howto@chat.disroot.org?classes=button5)

### Donare soldi
E infine, ma non meno importante, il sostegno finanziario è sempre necessario, quindi non abbiate paura di gettare una valigetta piena di dollari a modo nostro. e se questo è troppo, ricordate che ogni centesimo conta e se decidete di "comprarci il caffè" su base regolare, sarebbe fantastico. Per maggiori dettagli, controlla [qui](https://disroot.org/donate)

---
