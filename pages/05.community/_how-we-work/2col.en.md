---
title: 'How can I see'
published: true
bgcolor: '#FFF'
fontcolor: '#555'
---

![](puzzle-piece.png?resize=45,45)
# How do we work?

We use a project board where we put all the things needed to be done. We've chosen a "agile methodology" as a starting point and we're working in sprints, which are short periods of time. In our case, a sprint takes four weeks. In the beginning of the sprint, we sit together and decide which tasks have priority and should be added to the current task-board. Then, within a period of four weeks, we do our best to accomplish as much as we can. Once the time is up, we close the sprint, move unfinished tasks back on the backlog pile, and post a message via all the channels (Forum, Mastodon, blog post) about the recent changes and new developments on the **Disroot** platform.

Additionally once a year we send out our **Annual Report** which is a summary of what has been done in the past year, what were the costs and incomes for the entire year and our estimated budget and plans for the following year.

---

![](eye.png?resize=45,45)
# How can I see what is happening?

The best way to see what are we currently busy with, what we've done so far and what we are planning for the future is to follow our git repositories.

[Disroot Project](https://git.disroot.org/Disroot/Disroot-Project) | [Current progress](https://git.disroot.org/Disroot/CHANGELOG) |
[List of issues](https://git.disroot.org/Disroot/Disroot-Project/issues)
