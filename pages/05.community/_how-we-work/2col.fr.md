---
title: 'Comment puis-je voir'
published: true
bgcolor: '#FFF'
fontcolor: '#555'
---

![](puzzle-piece.png?resize=45,45)
# Comment travaillons-nous?

Nous avons choisi la méthodologie agile comme point de départ. Nous travaillons en "sprints". Chaque sprint dure quatre semaines. Au début du sprint, nous nous asseyons ensemble et décidons quelles tâches sont prioritaires et devraient être ajoutées au tableau des tâches actuel. Ensuite, au cours de la période de quatre semaines, nous faisons de notre mieux pour accomplir autant que possible. Une fois le temps écoulé, nous fermons le sprint, remettons les tâches inachevées sur la pile d'arriérés, et publions un message via tous les canaux (forum, mastodon, message blog) sur les changements récents et les nouveaux développements de la plateforme **Disroot**.

De plus, une fois par an, nous envoyons notre **rapport annuel** qui est un résumé de ce qui a été fait au cours de l'année écoulée, quels ont été les coûts et les revenus pour l'ensemble de l'année, ainsi que notre budget estimatif et nos plans pour l'année suivante.

---

![](eye.png?resize=45,45)
# Comment puis-je voir ce qui se passe?

La meilleure façon de voir ce qui nous occupe actuellement, ce que nous avons fait jusqu'à présent et ce que nous planifions pour l'avenir est de regarder notre dépôt git.

[Le dépôt Disroot](https://git.disroot.org/Disroot/Disroot-Project) | [Les progrès en cours](https://git.disroot.org/Disroot/CHANGELOG) |
[Liste des problèmes](https://git.disroot.org/Disroot/Disroot-Project/issues)
