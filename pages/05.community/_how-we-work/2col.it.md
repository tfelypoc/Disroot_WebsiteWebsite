---
title: 'How can I see'
published: true
bgcolor: '#FFF'
fontcolor: '#555'
---

![](puzzle-piece.png?resize=45,45)
# Come operiamo?

Abbiamo scelto la metodologia AGILE come punto di partenza. Stiamo lavorando negli sprint. Ogni sprint dura quattro settimane. All'inizio dello sprint, ci sediamo insieme e decidiamo quali compiti hanno la priorità e dovrebbero essere aggiunti all'attuale taskboard. Poi, nel giro di quattro settimane, facciamo del nostro meglio per fare tutto il possibile. Una volta scaduto il tempo, chiudiamo lo sprint, rimuoviamo i compiti non completati sul mucchio di lavoro arretrato e pubblichiamo un messaggio su tutti i canali (forum, mastodon, blog post) riguardo ai recenti cambiamenti e ai nuovi sviluppi sulla piattaforma Disroot.

Inoltre una volta all'anno inviamo il nostro **rapporto annuale**, che è una sintesi di quanto è stato fatto l'anno scorso, quali sono stati i costi e le entrate per l'intero anno e il nostro budget stimato e i piani per l'anno successivo.

---

![](eye.png?resize=45,45)
# Come posso vedere cosa sta succedendo?

Il modo migliore per vedere con cosa siamo attualmente impegnati, cosa abbiamo fatto finora e cosa stiamo pianificando per il futuro è seguire i nostri repository git. 

[Disroot Project](https://git.disroot.org/Disroot/Disroot-Project) | [Current progress](https://git.disroot.org/Disroot/CHANGELOG) |
[List of issues](https://git.disroot.org/Disroot/Disroot-Project/issues)
