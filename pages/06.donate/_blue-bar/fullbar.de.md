---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Wir sind dankbar für Eure Spenden und Unterstützung!

Durch Eure Unterstützung kommen wir unserem Ziel der finanziellen Zukunftsfähigkeit unseres Projektes immer näher und beweisen, dass ein Modell basierend auf sozialem Zussamenhalt möglich ist. 

To thanks those of you that donate at least 10€, we will send a **Disroot stickers pack**. Don't forget to leave, in the reference of your donation, the postal address we should send this to.

![](stickers.jpg?resize=50%)