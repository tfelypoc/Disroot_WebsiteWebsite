---
title: Online-Spende
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---


# "Wenn Du nicht für das Produkt zahlst, bist Du das Produkt."

---

#### Online-Spende:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Spenden via Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Ein Patron werden" src="donate/_donate/p_button.png" /></a>

<a href="https://flattr.com/profile/disroot" target=_blank><img alt="Flatter das!" src="donate/_donate/f_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=de&hosted_button_id=AW6EU7E9NN3VQ&source=url" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Kryptowährung" src="donate/_donate/c_button.png" /></a>

</div>

#### Banküberweisung:
<span style="color:#8EB726; font-size:1.8em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>
*(Eine Stichting ist eine niederländische Gesellschaftsform ähnlich einer deutschen Stiftung. Kernpunkt einer Stichting ist die Maßgabe, dass sie keinen Gewinn erwirtschaften darf.)
Kreditkarte:<br><span style="color:#8EB726;"> Du kannst den blauen Paypal-Button nutzen, um per Kreditkarte zu spenden. Ein Paypal-Account ist nicht notwendig. </span>

#### Hardware-Spenden: <span style="color:#8EB726;"> Siehe unten </span>
