---
title: Resumen
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---

## Resumen Mensual

![](graph.png?lightbox=1024)


Apoyado por <button class="button button4">214</button> Disrooters
_(Click sobre el número para una vista general)_

![](graph_support.png?lightbox=1024&resize=40,40&class=transparent)

## Donaciones a proyectos de Soft. Libre y Cód. Abierto
![](donated.png?lightbox=1024&resize=70%)
