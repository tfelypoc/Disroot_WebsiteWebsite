---
title: Overview
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---

## Situazione finanziaria

![](graph.png?lightbox=1024)


Supporto di <button class="button button4">214</button> Disrooters
_(Clicca sul numero per una panoramica)_

![](graph_support.png?lightbox=1024&resize=40,40&class=transparent)

## Donazioni alla comunità FOSS
![](donated.png?lightbox=1024&resize=70%)
