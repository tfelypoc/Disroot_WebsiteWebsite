---
title: 'Archiviazione sul cloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Spazio aggiuntivo per mail e/o cloud

![email](logo_email.png?resize=150) ![cloud](logo_cloud.png?resize=150)

---      

<br>
Con il tuo account **Disroot**, ottieni spazio di archiviazione GRATUITO: 1 GB per le tue e-mail, 2 GB per il cloud. Tuttavia, offriamo la possibilità di estenderlo.

Ecco i prezzi **all'anno, spese di pagamento incluse**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |

<br>
Le transazioni all'interno dell'UE sono soggette a un'IVA aggiuntiva (imposta sul valore aggiunto) del 21%.

Puoi decidere di allocare questo spazio di archiviazione aggiuntivo come desideri tra posta e cloud. Ad esempio, se ottieni 10 GB di spazio di archiviazione, potresti decidere di avere 8 GB per il cloud e 2 GB per la posta.


<a class="button button1" href="/forms/extra-storage-space">Richiedi spazio di archiviazione extra</a>
