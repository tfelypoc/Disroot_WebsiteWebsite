---
title: 'Paso 2'
bgcolor: '#1F5C60'
fontcolor: '#fff'
wider_column: right
---

# Paso 2:
## Configura tus DNS

Una vez que tengas tu propio dominio, necesitas hacer dos cosas para vincularlo a disroot.org.
*(El siguiente procedimiento se realiza a través de la interfaz provista por tu registrador de dominio -agregar registros DNS-)*:


---      

## 1: Prueba que el dominio te pertenece
Simplemente, crea un registro **TXT** con: **disroot.org-tudominio.tld** (remplazando `tudominio.tld` con **tu propio nombre de dominio**, claro). Esto indicará que realmente ese dominio te pertence.

## 2: Apunta tu dominio a disroot.org
**1ro.** Asigna en el registro MX a **disroot.org.** **`(¡no te olvides del punto final!)`**
**2do.** Configura **"v=spf1 mx a ptr include:disroot.org -all"** para que puedas enviar correos tanto desde tus propios servidores como desde los de **Disroot**. Esta es simplemente una configuración por defecto que debería ser aplicable en la mayoría de los casos. Si quieres aprender más acerca de los registros SPF y modificar los tuyos, busca más información en la web (p.ej.: en [esta página](https://www.dmarcanalyzer.com/es/spf-3/spf-record/)).
