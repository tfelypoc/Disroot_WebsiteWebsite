---
title: 'Etape 2'
bgcolor: '#1F5C60'
fontcolor: '#fff'
wider_column: right
---

# Etape 2:
## Configurez vos DNS

Une fois que vous avez votre propre domaine, vous devez faire deux choses afin de le lier à disroot.org.
*(Les étapes suivantes se font via l'interface fournie par votre fournisseur de domaine, quelque chose comme - ajout d'enregistrements DNS-)* :


---      

## 1 : Prouvez que vous êtes le propriétaire du domaine
Créez simplement un enregistrement **TXT** avec : **disroot.org-votredomain.tld** (en remplaçant `votredomain.tld` par **votre nom de domaine**, bien sûr). Cela indiquera que vous êtes bien le propriétaire de ce domaine.

## 2 : Faites pointer votre domaine vers disroot.org
**1er.** Définissez l'enregistrement MX sur **disroot.org.** **`(n'oubliez pas le point final !)`**.
**2ème.** Définissez **"v=spf1 mx a ptr include:disroot.org -all "** Cela vous permet d'envoyer des emails depuis vos propres serveurs ainsi que ceux de **Disroot**. Il s'agit simplement d'un paramètre par défaut qui devrait être applicable à la plupart des scénarios. Si vous souhaitez en savoir plus sur les enregistrements SPF et modifier les vôtres, consultez le Web pour plus d'informations (par exemple, [cette page](https://www.dmarcanalyzer.com/spf/how-to-create-an-spf-txt-record/)).
