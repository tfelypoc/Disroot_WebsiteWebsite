---
title: 'Step 3'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Step 3:
##  Donate to Disroot

![](../../01.home/_donation/premium.png?class=reward) **Disroot depends on your financial contribution**. That is what allows us to be fully independent and what pays the bills for hosting **Disroot**, as well as adds to the ultimate goal of putting food on our table.


---

<br>
**Custom domain linking is a lifetime feature**. There is no subscription nor price tag attached to it. **You decide how much you value our work and our service** and how much you are willing and able to donate for this feature.

**As an indicator we propose the equivalent of 12 coffees a year** (inviting your admins for a cup of your favorite coffee from your preferred coffee shop is something that should be a human right 😋 ). What coffee and what price, **is your choice**. Just keep in mind that well being of the platform means well being of your communication. So let's keep it healthy.

To donate to **Disroot**, choose one of the following options.
**Make sure to create a reference name for your donation**, something you can use in the request form below so we can check that you have donated. Once the request is processed, the reference information linking to your account is removed from our administration (however, for obvious reasons, it won't be from bank or Paypal as we have no control over it, so keep that in mind).

#### Online donation:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donate using Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Become a Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://flattr.com/profile/disroot" target=_blank><img alt="Flatter this!" src="donate/_donate/f_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

#### Bank transfer:
<span style="color:#8EB726; font-size:1.6em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>

Credit cards:<br><span style="color:#8EB726;"> You can use the blue Paypal button for credit card donations (Paypal account is not needed). </span>
<br><br>
