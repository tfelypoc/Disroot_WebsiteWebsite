---
title: 'Importante! A causa di una vulnerabilità di sicurezza, siamo passati a un altro software Webmail!'
date: '22-04-2022'
media_order: warship.jpg
taxonomy:
  category: news
  tag: [disroot, rainloop, snappymail. email,]
body_classes: 'single single-post'

---
Cari Disrooter,

a causa di una [vulnerabilità di sicurezza](https://thehackernews.com/2022/04/unpatched-bug-in-rainloop-webmail-could.html?m=1) nel software webmail che utilizziamo, abbiamo preso in modo rapido la decisione di migrare a un nuovo software basato su Rainloop. Dal momento che dovevamo agire in fretta, non abbiamo avuto il tempo per testare adeguatamente il nuovo software e preparare tutti i possibili casi limite. Se riscontri problemi, faccelo sapere in modo da poter fornire una soluzione.
<br><br>
Abbiamo inoltre disabilitato l'app Rainloop nextcloud (app di posta su cloud.disroot.org) poiché non esiste ancora una app sostitutiva per questo servizio. Abbiamo inoltre rimosso l'autenticazione a due fattori per la webmail. Perché la decisione di rimuovere 2FA?
<br><br>
L'autenticazione a due fattori nella posta funziona solo su webmail. Ciò significa che chiunque può semplicemente utilizzare il client IMAP o POP3 che non lo supporta e tentare di accedere alle e-mail. 2FA nella webmail dava quindi una falsa sensazione di sicurezza. Era qualcosa di cui non siamo mai stati grandi sostenitori, ma lo avevamo abilitato a causa delle continue richieste degli utenti. Per il futuro lavoreremo invece su una soluzione di autenticazione a due fattori a livello di Disroot che fornirà una sicurezza adeguata.
<br><br>
Ci scusiamo per qualsiasi inconveniente, ma speriamo ci sia comprensione. Il problema era potenzialmente veramente molto grave (vedi il link pubblicato sopra). 
<br><br>
Vorremmo, per concludere, inviare i nostri ringraziamenti alle persone dietro a SnappyMail per il software webmail che propongono.
