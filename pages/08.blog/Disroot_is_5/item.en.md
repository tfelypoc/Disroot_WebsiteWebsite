---
title: '5 years of Disroot?'
media_order: 2020-reset.png
published: true
date: '07-08-2020 13:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

Due to a bug in the calendar, where March has not ended yet, Disroot will not turn 5 this year. We are looking forward to 2021 and the 6th year's celebration.