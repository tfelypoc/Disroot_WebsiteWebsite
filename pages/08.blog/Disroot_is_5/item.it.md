---
title: Quinto anno di Disroot?'
media_order: 2020-reset.png
published: true
date: '07-08-2020 13:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

A causa di un errore nel calendario, dove il mese di marzo e i suoi annessi sembrano non ancora terminati, Disroot quest'anno non festeggerà il suo quinto compleanno. Non vediamo l'ora del 2021 per poter celebrare il nostro sesto anniversario.
