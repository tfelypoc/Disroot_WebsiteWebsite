---
title: 'Nextcloud - Leistungseinschränkungen'
date: '20-11-2019 15:00'
media_order: this-is-fine.0.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud]
body_classes: 'single single-post'

---

Wir Ihr vielleicht bemerkt habt, ist unsere Cloud-Instanz derzeit langsam und instabil. Wir arbeiten sowohl mit den Nextcloud-Entwicklern als auch mit einigen anderen Instanzen zusammen, die von dem gleichen Phänomen betroffen sind, das ein Fehler zu sein scheint, um eine grundlegende Ursache herauszufinden.

Wir hoffen, möglichst bald eine funktionierende Lösung zu haben, so dass alles wieder normal funktioniert. Allerdings haben die schlaflosen Nächte der letzten Tage bis jetzt noch kein Ergebnis gebracht.
