---
title: 'Disroot Total Blackout'
media_order: vrijeplaatsen.jpg
published: true
date: '09-11-2017 21:38'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - foundation
body_classes: 'single single-post'
---

First we'd like to start with the big news: **As of 3rd November Disroot is an official foundation!** This is an important milestone in Disroot's timeline and a validation of our commitment to the project.

This last month was extremely busy, perhaps our busiest so far, as we were preparing the system for these new features:

#### Aliases
Aliases are now available for our regular supporters. By regular supporters we think of those who contribute the equivalent of at least one cup of coffee a month.
It is not that we are promoting coffee, on the contrary - coffee is actually quite the symbol for [exploitation](http://thesourcefilm.com/)  and [inequality](http://www.foodispower.org/coffee/) so we thought that it is a good way to let people measure themselves how much they can give.
We found this [list](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) of coffee cup prices around the world, it might not be very accurate, but it gives a good indication of the different fares.
Please take time to consider your contribution. If you can 'buy' us one cup of Rio De Janeiro coffee a month that's OK, but If you can afford a Double Decaf Soy Frappuccino With An Extra Shot And Cream a month, then you can really help us keep the Disroot platform running and make sure it is available for free for other people with less means.

To request aliases you need to fill in this [form](https://disroot.org/forms/alias-request-form).

*If you already donated to Disroot in the past and would still like to get aliases please get in contact with us.*

For information on howto to setup your new email aliases check our [howto section](https://howto.disroot.org/email/identities)

#### Extra shorter email address
A little something to all Disrooters - our new short domain **disr.it** is now available to all. Each account has now a *username*@disr.it alias by default, that can be used as an extra email address. Follow the link above to learn how to set it up on your device or in the setting of the webmail.

#### Testing new conferencing system
As some of you know developers at matrix.org recently decided to utilize "meet jitsi" service to handle video conferences. As a preperation to later serving our own selfhosted solution for matrix users on our server, we have also deployed jitsi on Disroot. You can find it via [https://calls.disroot.org](https://calls.disroot.org). It's a very simple yet powerful video conference system that supports audio/video, chat and screen sharing without the need to create any account. We haven't officially added it to the list of the services but we are curious to read what you think about it and at the same time to test its resource footprint on our platform when widely used. So check it out, play with it and remember to let us know what you think about it.

#### Donations
Since Liberapay does not allow rewarding for donations - which means we are unfortunately unable to give out email aliases to people who support Disroot through Librepay - we have also started a Patreon page. You can find the information on that and other contribution options at [https://disroot.org/donate](https://disroot.org/donate). 

We are also waiting to get the details of our new bank account, we will add the information to the donation page once we have it.

#### Upcoming sprint
The coming sprint will be our last sprint this year. Apart from releasing more cloud space and domain linking features we want to focus on our tutorial section. Lots of tutorials need updating and even more are waiting to be written. We therefor need help from all of you to contribute to tutorials and/or their translations. One of the first things we'll do is create a tutorial on how to submit contributions. We are also planning a major hardware upgrade in the coming weeks. We're going to migrate to a dedicated database server which should improve the overall performance of the platform.

(On the picture: A street rave in solidarity with Amsterdam’s last standing free spaces ♥ ADM ♥ Baijesdorp ♥)
