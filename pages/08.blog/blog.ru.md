---
title: 'Блог'
theme: disrootblog
blog_url: blog
body_classes: header-image fullwidth

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items: @self.children
    order:
        by: date
        dir: desc
    limit: 5
    pagination: true

feed:
    title: 'Disroot Blog'
    description: 'Disroot Blog'
    limit: 10

pagination: true
---
<link rel="alternate" type="application/atom+xml" title="Disroot Blog" href="blog.atom" />
