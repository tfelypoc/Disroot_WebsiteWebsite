---
title: 'DisNews #4 - Nextcloud performance; Framadate and Ethercalc; Custom domains and XMPP; Lacre.io; fe.disroot.org'
date: '29-12-2021'
media_order: 2022.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud, etherpad, framadate, cryptpad, lacre,]
body_classes: 'single single-post'

---

Hallo, liebe Disrooter!


Wir möchten euch allen ein frohes neues Jahr wünschen! Lasst uns nicht darüber reden, wie verrückt die Welt geworden ist und wie düster die Zukunft aussieht. Konzentrieren wir uns stattdessen auf die guten Dinge im Leben. Stellt Euch vor, Ihr säßet draußen in einem heißen Bad (oder in einem kalten Pool, wenn Du Dich gerade in der südlichen Hemisphäre befindest. Du genießt Dein Lieblingsgetränk, während eine frische Brise Dein Gesicht berührt; Du öffnest die Augen und siehst Dir nahe stehende Menschen, die mit Dir die letzten Minuten des alten Jahres feiern. Die Zeit scheint in diesem Moment still zu stehen, und keine Deiner Sorgen oder Deiner schlechten Gedanken scheinen real zu sein. Du nimmst den letzten Schluck des Jahres 2021, und bevor Du die Augen öffnest bist du im Jahr 2022. Ein Jahr, auf das Du Dich freust, das Jahr, in dem du endlich Deine schlechte Angewohnheit ablegen kannst, die du schon immer loswerden wolltest, das Jahr, in dem Du endlich das coole Projekt verwirklichen kannst, das Du Dir schon immer vorgestellt hast, das Jahr, in dem staatlichen Beschränkungen der Vergangenheit angehören und jeder Dich auf der Straße anlächelt und mit "Guten Morgen" begrüßt...

**Das wünschen wir Euch allen! Lasst uns das Jahr 2022 wieder großartig machen!**

Na gut. Obwohl unser letzter Newsletter noch gar nicht so lange her ist gibt es ein paar Kleinigkeiten, die wir gerne noch einmal beleuchten  möchten.


# Nextcloud-Performance-Einbruch

Für einigen Wochen war Nextcloud langsamer als normal. Wir haben das Problem inzwischen erkannt und behoben, so dass jetzt alles wieder „normal langsam“ sein sollte. Derzeit arbeiten wir daran, die Leistung des Cloud-Dienstes zu verbessern, so dass es wieder angenehmer wird, damit zu arbeiten. Wir bereiten uns darauf vor, neue Server für unsere Infrastruktur anzuschaffen, was definitiv ein großer Fortschritt sein wird, aber bis dahin wollen wir sicherstellen, dass wir so viel wie möglich aus der aktuellen Hardware herausholen, um sicherzustellen, dass die Leistung der Cloud so gut ist, wie sie sein kann.


# Die Zukunft von Framadate/Ethercalc

Im letzten Beitrag haben wir über unsere Bedenken informiert, dass **Framadate** und **EtherCalc** seit einiger Zeit nicht mehr aktiv weiterentwickelt werden, und über die Idee, diese Dienste zugunsten von **CryptPad** zu entfernen, das ähnliche Lösungen mit dem Vorteil bietet, dass sie Ende-zu-Ende-verschlüsselt sind und sehr aktiv weiterentwickelt werden. Die überwältigende Mehrheit von Euch unterstützt diese Idee, und so werden wir bis Ende März 2022 sowohl **[Ethercalc](https://calc.disroot.org)** als auch **[Framadate](https://poll.disroot.org)** abschalten und CryptPad als Lösung für kollaborative Tabellenkalkulationen und Umfragen mit zusätzlicher Sicherheitssauce empfehlen. Und da wir gerade davon sprechen, möchten wir Euch einladen, alle in CryptPad gebündelten Dienste genauer unter die Lupe zu nehmen. CryptPad ist unserer Meinung nach eine großartige Möglichkeit, mit anderen zusammenzuarbeiten, egal ob es sich um ein Textdokument, eine Kalkulationstabelle, eine Projekttafel oder eine Präsentation handelt. Darüber hinaus handelt es sich um eine Ende-zu-Ende-verschlüsselte Software, die den Inhalt all Ihrer Dateien geheim hält, so dass nicht einmal wir, die Administratoren, sie lesen können.


# Benutzerdefinierte Domains und XMPP

Wir sind sehr überrascht und dankbar über Eure Reaktionen. Die Anfragen nach benutzerdefinierten Domänenverknüpfungen für E-Mails häufen sich, und wir freuen uns, dass Ihr Euch darauf einlassen wollt. Wir sind der Meinung, dass dies die beste Möglichkeit ist, Eure E-Mail-Adresse/Internetpräsenz beizubehalten, ohne sich Gedanken über den Provider machen zu müssen, den Ihr nutzt; denn Ihr könnt diese Domain überall hin auf andere Server verschieben, ohne Euch Gedanken darüber machen zu müssen, alle Ihr Eure Kontakte über den Wechsel zu informieren.

Obwohl wir auch XMPP-Chat-Domain-Verknüpfungen anbieten, haben wir noch keine davon bearbeitet (Prioritäten!), aber wir werden uns bald mit allen in Verbindung setzen, die diese angefordert haben, also bleibt dran!


# Ausblicke in die Zukunft

## Lacre.io

Obwohl wir noch nicht so viel an dem Mailverschlüsselungsprojekt arbeiten konnten, wie wir gerne hätten, und es erst vor kurzem begonnen haben, wollen wir es im kommenden Jahr fertigstellen. @pfm hat gezaubert und daran gearbeitet, die Software auf den neuesten Stand zu bringen und benutzbar zu machen, so dass wir fast so weit sind, Euch sehr, sehr bald in geschlossene Alpha-Tests zu schicken. Wir freuen uns schon sehr darauf.


## Fe.disroot.org - Eine neue Saat im Fediversum.

Für diejenigen, die es noch nicht wissen: Das Fediverse ist ein Netzwerk von föderierten Diensten, die von sozialen Netzwerken über File-Sharing bis hin zu Image-Boards und so weiter reichen. Es ist ein großartiges Netzwerk, in dem keine einzelne Instanz alle Ressourcen und Daten der Nutzer besitzt. Es ist ein dezentralisiertes Netzwerk, das die Freiheit und die Zusammenarbeit zwischen verschiedenen Instanzen fördert, um ein wirklich unabhängiges, selbstverwaltetes Netzwerk von ethischen Dienstleistern zu schaffen.

Eines der Hauptziele von Disroot ist die Bereitstellung von föderierten Diensten, und so ist die Einführung des ActivityPub-Protokolls (die treibende Kraft hinter dem Fediverse) etwas, das wir schon eine Weile tun wollten. Wir haben viele mögliche Softwarelösungen für eine Weile getestet. Obwohl wir **Hubzilla** für die fortschrittlichste und datenschutzfreundlichste Lösung auf dem Markt halten, ist es für uns sehr aufwändig, sie zu pflegen und benutzerfreundlich zu gestalten. Wir haben beschlossen, einen anderen Ansatz zu wählen. Anstatt eine einzige Software für alle Aufgaben zu verwenden, haben wir beschlossen, uns verschiedene föderierte Dienste anzuschauen und eine Reihe von ihnen zu nutzen, die eine bestimmte Aufgabe erfüllen. So haben wir mit einer Microblogging-Plattform (wie Mastodon oder Twitter) begonnen und uns für **Pleroma** entschieden. Das befindet sich noch in der Testphase, aber es ist benutzbar genug, um es auszuprobieren. Wenn Du bereits mit dem Fediverse vertraut bist können wir Dein Feedback gut gebrauchen, um all die kleinen Probleme und Fehler zu beseitigen, bevor wir es für den regulären Gebrauch freigeben. In der Zwischenzeit bereiten wir alle notwendigen Anleitungen und Website-Informationen vor, um Disrootern, die mit dem Fediverse-Konzept noch nicht vertraut sind, den Einstieg so einfach wie möglich zu machen. Wenn Du daran interessiert bist, die Dinge auszuprobieren, kannst Du Dich mit Deinen Disroot-Zugangsdaten bei unserer Pleroma-Instanz anmelden: **https://fe.disroot.org**. Wenn Du an Diskussionen über zukünftige Fediverse-Apps teilnehmen möchtest kannst Du unserem föderierten Chat unter xmpp:fedisroot@chat.disroot.org beitreten.


**Noch einmal: Wir wünschen Euch allen ein tolles neues Jahr und ein großartiges 2022!**

Besonderen Dank an alle Disrooter, die uns geholfen haben, indem sie Probleme gemeldet, gespendet, in unserem Chat (disroot@chat.disroot.org) abgehangen haben. Und denen, die anderen bei Disroot-bezogenen Problemen geholfen haben, all denen, die Patches, Übersetzungen und Tutorials eingereicht haben! Ihr seid alle großartig!

Disroot würde auch nicht ohne die unermüdliche Arbeit aller FLOSS-Entwickler existieren. Wir möchten Euch allen für eure harte Arbeit danken! Allen, die an der Entwicklung von Linux-Kernel, GNU-Utilities, Debian und all den anderen Distributionen (auch Arch :P) beteiligt sind, allen, die hinter Projekten stehen, die wir bei Disroot täglich benutzen: **Nginx, Debian, CryptPad, Framasoft, Mumble, Pleroma, Soapbox, Hubzilla, Etherpad, Ethercalc, Taiga, Nextcloud, Lufi, ConverseJS, Privatebin, Searx, Jitsi, Gitea, Prosody, Mattermost, Postfix, Dovecot, Amavis, Rainloop und viele viele mehr...** Ihr seid Helden! Frohes neues Jahr!
