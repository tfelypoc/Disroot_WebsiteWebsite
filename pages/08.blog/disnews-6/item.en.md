---
title: 'DisNews #6 - Catchall mail for custom domain; Improving SPAM handling; Roundcube webmail'
date: '20-09-2022'
media_order: stickers.jpg
taxonomy:
  category: news
  tag: [disroot, news, mail, roundcube, webmail]
body_classes: 'single single-post'

---

Hi there Disrooters,
Been a while since you heard from us. The blogpost we have been planning for July got very much delayed. This was due to the fact we had to take care of some personal things as well as resolve some of the most pressing server issues that made it impossible to release planned features. Thankfully this is behind us and we are back on track. Below we would like to present to you summary of last three months of work.

So let's get to it. Here's what we prepared for you.

# Catchall mail feature

Some of you who decided to link their custom domain to our mail server, have been requesting this feature. It allows you to receive all emails addressed to your domain even for addresses that don't exist. After much anticipation, we have finally managed to make that happen. And so, everyone with their domain linked to our service can now enable or disable the **catchall** feature by updating your account's settings at [https://user.disroot.org](https://user.disroot.org/pwm/private/updateprofile). This was also a reason we have not process new domain linking requests, so if you have applied for one, we will have some good news for you in coming days.

# SearxNG

Our beloved Searx instance has been pretty much unusable for quite some time. We have been trying to find a way to improve the situation but we could not find a solution (perhaps lack of time to dedicate to the issue is to blame or lack of expertise). While trying to fix the situation, some Disrooters have pointed us to a recent fork of Searx called SearxNG (due to internal conflict within the development team some of the people decided to leave and start their own project). We have decided to try it out and see whether this will improve the situation. To our surprise, apart from UI (user interface) improvements, our SearxNG started providing results. We are very happy with this change but we have still not made our decision, whether to stay with SearxNG or go back to Searx. As much as we enjoy the improved UI we want to investigate the situation better to make best decision possible. Searx has been with us since the beginning and we very appreciate all the work asciimoo (author of Searx) so that decision needs more thought to be put in.

# Better SPAM handling

In our last post, we have announced our decision to move SPAM automatically to Junk folders. Recently we have loosen some of the spam prevention such as RBL list checks, greylisting and number of other checks that were straight up rejecting suspicious emails. We wanted to give more decision power to Disrooters to handle their email. This naturally leads to increase of potential SPAM emails. To help combat the increasing amount of SPAM, we have ordered our anti-spam bot to autolearn what is SPAM and what is HAM. Choosing autolearn option allows us to improve spam autodetection. In order to response to false positives where legit email got marked as spam, we urge you to forward such email (or at least it's headers) to **ham.report@disroot.org** email address. This will allow us to tweak the settings so that legit email does not end up in Junk folder. Additionally we have created an email address where you can report email that should be marked as spam (**spam.report@disroot.org**). With your feedback, we will be able to strike the balance in the war on spam and improve the situation for everyone.

# Roundcube our new go to webmail

Do you remember when we announced migrating to Roundcube back in 2020? Well, that took a while, but after a bit of delay it's finally here. Our new webmail is ready to be used. You can reach it under new url at [https://webmail.disroot.org](https://webmail.disroot.org) Give it a try and help us with feedback. We are aware of some rough edges and we are sure there is much more to discover once more people start using it. For the time being we provide both SnappyMail as well as Roundcube, but eventually we will stick to just one solution, which we hope will be Roundcube, however we don't want to close all the doors yet. What is particulary interesting is that Roundcube is syncing automatically with your Nextcloud contacts. To learn how to migrate your contacts from snappymail (current webmail) to Roundcube or Nextcloud, check the [howto's](https://howto.disroot.org/en/tutorials/email/webmail/migration) baked by @fede

# XMPP Upload file limit increase

As the global inflation is on the raise, we have decided to increase the limit for files uploaded when using our xmpp chat service from 10MB to 512MB! While doing that we also decided it makes sense to make file retention and chat history equal and so we set it both to 1 month.

# Stickers, stickers, stickers!!!!

Do you want some disroot swag? Were you dreaming about putting that awesome disroot sticker on your laptop to show everyone you are using service that no one heard about? XD  For a while we wanted to provide some merch for all disrooters and while we are waiting for @muppeth and @antilopa to build their shirt printing facility and be able to self produce shirts, we decided to start with sticker packs. And so, we present you the first disroot sticker pack. The idea is simple. Donate at least 10 euro and leave address where we should send you the stickers to in reference of your donation. We plan to change designs everytime we order new batch of stickers to make things more interesting.

That is it for now. We are planning as always big things but we won't jinx it. Let's hope for more regular news from now on :P

Cheers, enjoy yourself and stay safe.
