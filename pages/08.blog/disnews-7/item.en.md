---
title: 'DisNews #7 - Testing Lacre on Disroot!'
date: '26-10-2022'
media_order: lacre.png
taxonomy:
  category: news
  tag: [disroot, news, mail, lacre, encryption]
body_classes: 'single single-post'

---
Hi there,

As you know, for the past months, we have been busy with Lacre. Open source, GnuPG based mailbox encryption software. **@pfm** has been working like crazy exceeding the initial plans and bringing the project to the next level. We have now reached milestone allowing us to finally battle test it on Disroot. Our dream is coming true folks. This is why in the weekend between **4th and 6th of November** we will enable Lacre on Disroot mail server for open beta testing for everyone interested. The objective of this test is to see how Lacre will behave at scale and realistic load scenario. Additionally, we would like to gather user experience feedback. This will allow us to iron out all possible issues before we add Lacre permanently.

Here is some important information about the event. **Please read it carefully before you decide to participate**.

 - On friday the 4th, we will be post detailed information about the event under [https://disroot.org/lacre_test](https://disroot.org/lacre_test) There you will find all the details about how to setup Lacre encryption as well as follow updates on the situation, information regarding contact addresses in case of emergency, information about critical issues, feedback, etc. Please visit the page on 6th when we will post all details.

- **This test is directed at those Disrooters familiar with GnuPG mail encryption!** - Once you enable Lacre on your Disroot account all incoming emails will be automatically encrypted for your email address with supplied public key for the duration of beta tests. **If you don't know what you're doing and lost us already, please do not join the tests or you may loose access to encrypted emails**. Keep in mind you can always use your @getgoogleoff.me and @disr.it aliases.

- Lacre encrypts only current incoming emails. This means it won't encrypt your entire mailbox so you don't have to be afraid about emails sent prior the moment you setup your key with Lacre.

- In case the mail server becomes unstable, causing outage or in any way impacts the operation of the server, we will terminate tests concluding, we need much more work before it is stable enough and move back to the drawing board. All of you who would like to further help with testing, can reach out to us to get access to our test server where we continue to work on things.

- Resetting your key in case key is lost does not currently work properly (hence you should know what you're doing). However since it's just testing you can always reach out to us for help. We can still reset encryption however we wont be able to decrypt emails.

We are looking forward to this event and can't wait for results. No matter the outcome, we are already happy and proud that the little idea materialized and is so close to completion. Big thanks to **Pfm** whitout whom we would not get that far, and to everyone helping along the way. Special thanks to **NLNET** who's financial aid helped a lot Lacre and ton of other projects (https://nlnet.nl/project/).

Keep your fingers crossed. Set a reminder, check the page and join us on **4th of November**!
