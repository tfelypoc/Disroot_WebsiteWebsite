---
title: 'Disroot joins the Matrix network'
date: 12/20/2016
taxonomy:
  category: news
  tag: [disroot, news, matrix]
body_classes: 'single single-post'

---

**For the last sprint of the year we decided to focus on improving instant messaging.** We’ve been big enthusiasts of XMPP as the best solution for chatting, since like… forever. The obvious perks of XMPP are **federation** and **decentralization**. There are plenty of XMPP service providers and setting up your own server to join the network is not that hard of a process (or is is at least, possible). However there are a lot of problems within the network; Incompatibility between clients, no real support for Apple products, problems with audio/video conferencing and small user base, just to name a few. We set out to work on our current chat solution in order to provide the best XMPP user experience possible. During our research and work we got involved in a lot of discussions within different FLOSS communities about the future of free and open IM. The more we discussed this matter, the more matrix.org project was coming up as a new alternative. After doing some research on our own we decided to try it out and see what the hype is all about.


![](logo1.png)

**Matrix.org** is, just like XMPP, a **decentralized, federated, open-source** Instant messaging software. Just like XMPP, it serves single chat as well as group chatting. However the strength of Matrix goes way beyond that. There is Matrix support for any device and operating system, either through its official app called “Riot” or various other clients to choose from. The **end-to-end encryption** works on all platforms using Riot app, for both single and group chats. VIdeo/Audio one-on-one calls and conferencing is supported on any device using Riot out of the box. Another very promising feature is its bridge possibilities. Bridging means you can connect different networks/protocols with Matrix network to have bidirectional communication. At this moment any IRC, Slack or Gitter, telegram and XMPP-MUC  room can be bridged together with Matrix, enabling people on all those separate networks to communicate with one another. There are many more bridges in the works as one of the main goals of the Matrix project is to bring all the “walled gardens“ together. Their approach to federation is also quite innovative. Rooms are distributed across the federated network. This means that group conversations are no longer tight to the single server they have been created on, so they can continue operating even if the originating server gets shut down.


## Matrix is still a very young project.

However it is already exceeding our expectations. it is easy to use, easy to maintain and the fact that it is easy to develop new functionalities for it, makes it a very promising project. This is why we too decided to join the new online sensation and deploy Matrix server for all Disroot users. Currently we are still testing, tweaking and gathering feedback before we launch it officially (1st of January meetup on Disroot’s Matrix channel anyone?). You can already test it yourself and say hello. You can join with your Disroot account.



## What’s with XMPP than?
As much as we were all for XMPP, the time has come to perhaps let go and move on to something newer with a fresh approach. Of course we are not ditching XMPP (and probably never will) so the server will remain functional with all the possible up to date features. Nothing will change in that regard, let’s not burn all the bridges just yet. In the coming time however, we want to put more emphasis on making Matrix the default communication service on Disroot. Let’s see where we get from there. Time will tell which solution is best.


Still not convinced?
For those of you who would like to read more about the philosophy behind Matrix check out [Matrix.org](http://matrix.org/#about). Those of you who prefer to watch things, [check this inspiring presentation from matrix core developer](https://www.youtube.com/watch?v=X9b9ZN5MVsE), **Matthew Hodgson**, at TADS – Lisbon 2016.
