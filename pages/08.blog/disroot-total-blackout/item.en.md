---
title: 'Disroot total Blackout '
media_order: kop_van_ende.jpg
published: true
date: '02-02-2018 16:50'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - issues
        - downtime
body_classes: 'single single-post'
---
Hi there.

Today Disroot server had a bit of a tumble.
Due to little mistake, our main server became a total zombie. There was no way for us to bring it back to life other then driving 70km to the datacenter to manually restarting it. Server was down at 14:45 and back up at 16:05


We're very sorry for this situation and we will take extra measures to prevent that from happening in the future.


### From other bad news.
Some of you already noticed, **Circles** app has been disabled on our server. This is due to a bug that caused huge performance issues. We hope that it will be resolved soon. Until then we need to keep it off in order to have our platform operational.

### And some good news.
In comming week we will be commencing huge disroot infrastructure upgrade operation! We will deploy SSD dedicated database server, upgrade RAM and storage capacity. This will greatly improve performance and give disroot a needed buffer to grow. Of course this will mean some short (hopefully) downtimes during the week, about which we will of course inform you all. 

If you want to be up to date with whats going on with disroot, you can follow our:
[Mastodon](https://social.weho.st/@disroot) or [Diaspora](https://pod.disroot.org/u/disroot) acccounts. Additionally you should definatelly check [https://state.disroot.org](https://state.disroot.org) and subscribe to incident and downtime reports via email or rss feed.
