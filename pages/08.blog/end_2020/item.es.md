---
title: 'Ufff... Por fin, final de 2020'
media_order: f2020.jpg'
published: true
date: '31-12-2020 11:15'
taxonomy:
    category:
        - news
    tag:
        - nextcloud
        - conversejs
        - xmpp
        - howto
        - novedades
body_classes: 'single single-post'

---
*Feliz Solsticio*

¡Y qué año fue este! No tiene sentido resaltar la cagada que fue para todxs y cómo todo parece estar al revés alrededor del mundo. Centrémonos en las cosas positivas y tratemos de disfrutar los últimos días del año.

¡Queremos desearles a todos una gran celebración y un gran 2021! Nos gustaría agradecer especialmente a todxs los que nos han apoyado este año de todas las maneras. ¡¡Les enviamos un gran abrazo a todxs y cada unx de lxs Disrooters!! ¡Que tengan un gran año!

Ahora bien, ya que hemos estado un poco flojos este 2020 poniéndolxs al tanto de lo que ha ido cambiando, veamos algunas de las pequeñas mejoras de las últimas semanas.

## Nextcloud 20
Hemos actualizado hace poco nuestra instancia de **Nextcloud** a su última versión que trae un montón de características nuevas buenísimas, siendo el **Tablero** la más notable. Como probablemente notaron, hay una nueva aplicación de tablero que muestra una lista de cambios en la nube. Ya sea que se trate de eventos de calendario, nuevos archivos, tarjetas de Deck o chats, pueden ver todo esto en un vistazo general. Además, hay plugins que permiten añadir servicios externos como Mastodon, Github, Gitlab y más. A medida que la lista de plugins crezca, añadiremos más opciones, por supuesto.

Un cambio adicional, especialmente para quienes utilizan **Nextcloud** como herramienta de colaboración, es el mensaje de estado. Ahora pueden mostrar fácilmente a lxs demás si están en línea, qué están haciendo y cualquier otra cosa. Viejo conocido de otras plataformas de chat, el estado permite a lxs demás ver con qué están ocupadxs y si están en línea para que lxs molesten o no.

Para aquellas personas que usan **Nextcloud Talk** hemos habilitado el puente a otras plataformas de chat. Todavía es una característica experimental pero es bueno probarla en el campo de batalla. Los puentes permiten conectar varias plataformas de chat en una, donde un robot transmite todo lo que sucede en una sala a todas las redes conectadas, permitiendo a las comunidades divididas participar juntas. Y así, una sala de chat de **Nextcloud** puede conectarse a una IRC, Matrix, Mattermost, XMPP y varias otras. Hagan la prueba y háganos saber sobre cualquier problema para que podamos transmitirlo al staff de **Nextcloud**.

Aparte de todas las cosas buenas y las mejoras, también habrán notado que el rendimiento de la nube ha mejorado mucho. Especialmente después de los problemas que tuvimos con ella durante algunas semanas. Estamos felices de que las cosas hayan vuelto a la normalidad y estamos disfrutando de la nube aún más como Equipo Central de Disroot, sobre lo cual pueden leer a continuación.

## Utilizando Nextcloud Deck para la planificación del Equipo Central de Disroot
Desde el principio mismo, hemos estado usando los servicios de **Disroot** como nuestras principales herramientas. Además de ser los administradores, también somos Disrooters nosotros mismos, utilizando y testeando nuestra plataforma tanto como sea posible. Pensamos que este enfoque ayuda a entender mejor los problemas y requerimientos y disfrutamos usando nuestra propia creación. Durante los últimos años hemos usado el tablero **Taiga** para planificar nuestro trabajo y gestionar los problemas, pero recientemente hemos cambiado a **Deck**. Una de las razones fue que decidimos abandonar la [metodología SCRUM](https://es.wikipedia.org/wiki/Scrum_(desarrollo_de_software)) ya que estaba consumiendo nuestra energía, especialmente porque **Disroot** no es nuestra principal ocupación (aunque no lo crean) y todavía necesitamos mantener nuestros trabajos diarios, familias y otros proyectos. La carrera constante de un sprint a otro durante los últimos 5 años, se volvió demasiado agotadora, y no pudimos mantener el ritmo que nosotros mismos creamos. Concluimos que ya no necesitamos la "pila de acumulados" (backlog), sprints y todo eso, y que era hora de hacer un mejor uso de otras herramientas que ofrecemos (ya saben, por aquello de que utilizar tu propia plataforma es siempre mejor), así que decidimos cambiar por un tiempo de **Taiga** a **Nextcloud Deck**. Para hacerlo transparente y permitir la participación de la comunidad, hemos creado y compartido tanto el Tablero principal del proyecto (donde quienes no son miembros del Equipo solo pueden leerlo) como el Tablero de Problemas (donde cualquiera puede reportar problemas, pedidos de mejoras, propuestas, etc.) con el **Círculo de la Comunidad Disroot**. Cualquier Disrooter puede unirse al **Círculo de la Comunidad** desde la aplicación **Círculos** en **Nextcloud** para participar o ver en qué estamos ocupados actualmente.

Además hemos creado una sala de chat en **Nextcloud Talk**, que está vinculada a las salas XMPP, IRC y Matrix. Esto significa que no importa qué plataforma usen para comunicarse, pueden conectarse fácilmente al lugar de encuentro de la **Comunidad Disroot**. \o/

¡Así que, únanse, sean activxs y diviértanse en nuestro Círculo de la comunidad!

## Tema Darkbeet de ConverseJS
El largamente prometido y pospuesto tema de **ConverseJS** (nuestro webchat XMPP de preferencia) ha aterrizado finalmente. Todavía hay muchas cosas que mejorar, pero *@muppeth* está muy feliz de que finalmente hayamos logrado avanzar algo. Es un buen comienzo y esperamos enviar el tema al [upstream](https://es.wikipedia.org/wiki/Upstream_(desarrollo_de_software)) una vez que esté pulido. Si a la gente de **ConverseJS** les gusta, será incluido en el código fuente.

![](converse_darkbeet.png)

Ya pueden usarlo en el [webchat](https://webchat.disroot.org) y si tienen interés en los últimos progresos y les gusta vivir al límite usen nuestro [devchat](https://devchat.disroot.org).

## Actualización de los manuales (Howtos)
Este año también comenzamos la necesaria actualización de los **Howtos** (manuales). *@Fede* está a full con ello, es una tarea que requiere dedicación constante y en el proceso vamos encontrando errores y posibilidades de mejoras. Hasta ahora, la mayor parte del uso básico de **Nextcloud** está cubierto y la sección de Usuarix está al día, pero todavía falta mucho por hacer.

El principal objetivo del proyecto Howto es brindar y hacer accesible la mayor cantidad de información posible, en la mayor cantidad de idiomas, sobre los servicios y software que usamos. Como podrán imaginar, ello requiere de una cantidad de trabajo y tiempo constantes.

Así que, si quieren colaborar en el desarrollo de los manuales, mejorar o traducir los existentes, enviarnos comentarios, sugerencias o incluso escribir sobre sus propias experiencias utilizando los servicios de **Disroot** y compartirlas con la Comunidad, solo tienen que comunicarse con nosotros a través de la [sala XMPP](xmpp:howto@chat.disroot.org?join) o por [correo](mailto:howto@disroot.org). Cualquier ayuda es muy bienvenida. Recuerden que también pueden ver el progreso en el **tablero Deck** accesible desde la Nube via el **círculo de Howto**.


**Así que una vez más a todxs: ¡A la mierda el 2020 y Feliz Año Nuevo!**
