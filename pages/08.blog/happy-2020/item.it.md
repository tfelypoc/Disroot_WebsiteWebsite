---
title: 'Buon 2020'
media_order: aliasPinguine.jpg
published: true
date: '01-01-2020 19:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - fundraiser
        - alias
body_classes: 'single single-post'
---

Saluti dal Team Disroot a tutti i Disrooter. Speriamo che vi siate divertiti un sacco ieri sera e non vediamo l'ora che arrivi il 2020. Quello appena trascorso è stato un anno pazzo e il 2020 non si prospetta molto diverso. Non vediamo l'ora di affrontarlo e speriamo che pure voi abbiate dei piani fantastici per il prossimo anno.

#DisrootAliasChallenge: obiettivo raggiunto!

Iniziamo l'anno con un annuncio straordinario: l'obiettivo della nostra campagna di raccolta fondi della scorsa settimana è stato raggiunto! Abbiamo raccolto 1500€! Ringraziamo tutti coloro che hanno deciso di donare a Disroot non solo quest'ultima settimana ma durante tutto l'anno! Vi siamo molto grati♥

Questo significa che molto presto @getgoogleoff.me verrà aggiunto come indirizzo email alias per tutti i Disrooter.

Se hai donato oltre 30 € da banca o criptovaluta, ti preghiamo di contattarci per darci i tuoi dati in modo da poterti inviare il tuo premio a modo tuo.

E nel caso te lo fossi perso, quarda il [video](https://peertube.co.uk/videos/watch/5a3cbc03-1831-4dee-b2cd-92d80dd51f2b) della raccolta fondi.

## Prossimi passi

Attualmente stiamo chiudendo l'anno e finendo le ultime cose presenti sulla nostra tabella di marcia. Stiamo anche lavorando per cristallizzare i piani per il nuovo anno e per scrivere il rapporto del 2019 che dovrebbe essere pubblicato alla fine di questo mese.

### Un saluto e grazie per essere stati con noi. Auguriamo a tutti in 2020 strabiliante!
