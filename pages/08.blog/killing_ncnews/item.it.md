---
title: 'Interruzione del servizio RSS'
media_order: paperpile.jpg
published: true
date: '05-04-2021 17:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - nextcloud
        - news
        - rss
body_classes: 'single single-post'
---
La nostra decisione di dismettere il servizio **Nextcloud News** (lettore di feed RSS) e di non proporre altri servizi simili alternativi probabilmente infastidirà alcuni, ma speriamo attraverso questo post di convincervi della bontà della nostra decisione.
<br>
I motivi di questa decisione riguardano esclusivamente le risorse hardware necessarie e il fatto che il servizio viene ampiamente utilizzato in modo improprio da molti utenti.

Da diverso tempo siamo confrontati con una situazione che ci porta ad avere circa 1 milione di articoli non letti a settimana, di cui circa 10k-20k che vengono letti entro tre settimane. Ciò significa che la maggior parte degli elementi che estraiamo e archiviamo sul server non vengono mai letti. Capiamo che si tratti di un meccanismo contorto... è facile aggiungere feed, ma poi si ha poco tempo per leggere le notizie aggregate. 

La situazione è diventata così grave che lo spazio nel database per le notizie da solo supera del doppio la quantità necessaria per memorizzare tutta la cache dei file, dei calendari, dei contatti che sono attualmente memorizzati sul server. Abbiamo contattato gli sviluppatori di **Nextcloud News** ma non abbiamo trovato una soluzione. 
Pertanto, ci siamo guardati in giro e abbiamo considerato di utilizzare **TTRSS** in sostituzione a **Nextcloud News**. Questo servizio offre impostazioni di amministrazione migliori, tuttavia mentre lavoravamo alla sua implementazione, abbiamo parlato con alcune persone che già forniscono questo servizio e abbiamo appreso che, a lungo termine, la situazione potrebbe non essere molto megliore di quella attuale in quanto il problema con le prestazioni e lo spazio del database è presente pure in **TTRSS**. 

Questo ci ha portato alla conclusione che i costi benefici di offrire un servizio simile non sono positivi e che forse non abbiamo poi così davvero bisogno di un servizio online per la gestione dei feed RSS. 

Tutti i [lettori RSS] (https://alternativeto.net/browse/search/?q=feed%20reader&license=opensource), disponibili su tutte le piattaforme e sistemi operativi, forniscono opzioni per gestire i feed RSS in locale. 
Con il file in formato OPLM, che puoi facilmente esportare/importare, puoi utilizzare lo stesso elenco di feed su più dispositivi. L'unico vantaggio di avere una soluzione RSS online è il fatto che puoi nascondere il tuo indirizzo IP dai siti web da cui vuoi estrarre gli elementi, ma pensiamo che questo non sia un una ragione sufficiente per creare un tale sovraccarico al nostro sistema.

Questi sono i motivi per cui abbiamo deciso di chiudere il servizio **Nextcloud News** e promuovere l'utilizzo locale dei feed RSS. 

Ecco come procederemo per concedere a tutti tutto il tempo necessario per salvare i propri feed: 

  - Sin da subito tutti gli articoli (feed) sono stati cancellati dal nostro database 
  - Di default News app è stata disabilitata per i nuovi utenti
  - ⚠️ **Dismetteremo completamente l'app News entro il 31 maggio 2021**, così avremo tutti tempo a sufficienza per esportare il proprio file OPLM con la lista delle proprie fonti. 

<br>
Puoi vedere come esportare la lista delle tue fonti [qui](https://howto.disroot.org/en/tutorials/user/gdpr/nextcloud/news)
