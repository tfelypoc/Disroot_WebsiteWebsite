---
title: 'Lacre beta test annullato'
date: '04-11-2022'
media_order: lacre_header.png
taxonomy:
  category: news
  tag: [disroot, news, mail, lacre, encryption]
body_classes: 'single single-post'

---
Siamo spiacenti di informarvi che abbiamo deciso di annullare il test di Lacre su Disroot programmato per questo fine settimana posticipandolo a data da convenire. Una settimana fa, abbiamo eseguito in modo non ufficiale di Lacre sul server Disroot. Poiché tutto il traffico e-mail passa attraverso Lacre indipendentemente dal fatto che l'utente abbia caricato o meno la propria chiave pubblica, abbiamo potuto osservare come si comporta Lacre. Siamo rimasti molto soddisfatti dei risultati. Lacre ha prestazioni eccellenti e siamo fiduciosi che avrà un impatto minimo sulle prestazioni del server. Tuttavia, grazie all'esecuzione di Lacre in condizioni di traffico ordinario, abbiamo riscontrato numerosi problemi che non avremmo potuto individuare altrimenti. Sebbene la maggior parte di questi problemi sia già stata risolta (pfm for president \o/ ), ci sono dei problemi che non siamo ancora riusciti a risolvere. Anche se i bug interessano casi rari, non vogliamo rischiare problemi nella consegna della posta che potrebbe interessare anche utenti che non partecipano ai beta test.

Con rammarico, abbiamo quindi deciso che è meglio concentrarsi sul risolvere i problemi già conosciuti, e annunciare una nuova data una volta che saremo pronti. Ci piacerebbe abilitare Lacre per tutti i Disrooter sin da subito, ma è importante fornire la migliore esperienza possibile. Siamo molto vicini a raggiungere questo livello, ma abbiamo aspettato per anni, quindi poche settimane in più non dovrebbero fare una grande differenza.
Ci scusiamo per l'inconveniente e non vediamo l'ora del lancio della nuova beta o persino dell'implementazione permanente di Lacre su Disroot. Per tutti coloro che vogliono dare un'occhiata e giocare con Lacre, ci si può  [unire alla nostra chatroom](https://lacre.io/contact) e chiedere un account di prova. Controlla il sito web del nostro progetto su [https://lacre.io](https://lacre.io).
