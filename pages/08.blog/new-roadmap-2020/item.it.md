---
title: 'Nuova tabella di marcia 2020'
media_order: road.jpg
published: true
date: '19-01-2020 18:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - roadmap
body_classes: 'single single-post'
---

Ciao Disrooter! State tutti bene, riposati e preparati per il nuovo anno? Noi lo siamo e siamo pure belli carichi! Prima di tutto però vogliamo pianificare l'anno e capire su quali fronti impegnarci nei prossimi mesi.

## I piani per il 2020

Come sempre abbiamo piani ambiziosi e abbiamo identificato alcuni temi su cui vorremmo concentrarci.

1.  **Miglioramenti posta elettronica**

	Vogliamo concentrarci su una serie di miglioramenti del nostro servizio di posta elettronica. Questi miglioramenti riguardano una nuova e migliore soluzione di webmail, una maggiore promozione dei client di posta elettronica (app) nativi. Vorremmo inoltre concentrarci sul pieno supporto di Autocrypt nella webmail.
 
	 Stiamo diventando sempre più "popolari" e purtroppo siamo sempre più attaccati da spam, phishing e altre brutte cose che rovinano l'esperienza della posta elettronica. Vogliamo quindi concentrarci anche su migliori soluzioni anti-spam/virus.
 
2.  **Mailbox crittografate**

	Una delle cose che non siamo riusciti a raggiungere l'anno scorso è la crittografia delle caselle di posta. Quest'anno speriamo di riuscire finalmente a raggiungere l'obietitvo. Inizialmente abbiamo pensato alla crittografia lato server (dove sono memorizzate le tue chiavi private sul server), tuttavia recentemente abbiamo iniziato ad esplorare la possibilità di utilizzare GnuPG (gpg/pgp) come sistema per crittografare end-to-end le e-mail. Se riusciamo nell'intento, solo il tempo ce lo dirà. Nel mentre, come soluzione di fallback lavoreremo sulla crittografia delle caselle di posta su lato server.

3.  **Miglior amministrazione**

	Uno dei temi dell'ultima tabella di marcia per il 4° trimestre del 2019 riguardava i modi per migliorare l'amministrazione, la gestione dei ticket di supporto, le richieste personalizzate e le altre attività di routine. Siamo consapevoli che alcune delle attività come la gestione dello spazio di archiviazione aggiuntivo, i domini personalizzati ecc. possono essere gestite in modo più efficace ed efficiente. Vogliamo quindi ottimizzare le procedure di questi tipi di lavori per permetterci di dedicare maggior tempo alle cose che più ci appassionano e piacciono come il miglioramento dei servizi che offriamo.

4. **Più amministratori a bordo**

	Negli ultimi anni Disroot è stato gestito da muppeth e antilopa che erano responsabili di tutto quello che gravitava attorno a Disroot (sito Web, attività quotidiane, amministrazione, supporto, amministrazione del sistema, manutenzione ecc.). Negli ultimi mesi il team Disroot è cresciuto: fede e meaz nel core team, massimiliano e maryjane come stretti collaboratori. Già nell'ultima tabella di marcia abbiamo iniziato a definire e ridistribuire alcune attività come ticket di supporto, progetti di traduzione e gestione degli howto. Vogliamo continuare su questa via coinvolgendo ulteriormente e formando i nuovi membri del team principale per consentire loro di accedere a più server di produzione, per aiutare con la risoluzione dei problemi, la manutenzione e altre attività e richieste che richiedono l'accesso ai server.

5. **Lancio di Hubzilla**

	Alcuni di voi sanno che da due anni a questa parte ci siamo innamorati di Hubzilla. Pensiamo che sia un ottimo software in anticipo sui tempi e con un grande potenziale. Per questo ci piacerebbe che diventasse il nostro social network principale. Proprio per questo Hubzilla ha bisogno di molto amore e molto lavoro. Purtroppo dopo due anni non siamo ancora riusciti a portare la nostra istanza di test al livello che volevamo. Ci poniamo quindi questo come obiettivo, così da poter fornire hubzilla a tutti i disrooter.

6. **Finanziamento**

	Dire che Disroot non sia diventato il nostro lavoro quotidiano sarebbe una bugia. Dedichiamo a Disroot più tempo di quanto facciamo nei nostri lavori canonici. Dal momento che dobbiamo mantenere le nostre famiglie e vorremmo poterci dedicare ai nostri hobby e forse pure ad un po 'di vita sociale (che in questo momento non esiste), dobbiamo cambiare questa situazione. Raggiungere la situazione in cui Disroot diventi la nostra principale occupazione sembra essere ancora improbabile, ma dobbiamo iniziare seriamente a lavorare pure su questo aspetto. L'anno scorso Disroot ha superato le nostre più rosee aspettative e ci ha mostrato che il sogno dell'indipendenza finanziaria è realistico. Ecco perché vogliamo dedicare un po' di tempo ad esplorare possibili vie per realizzare questo sogno.


## E il prossimo?

Abbiamo suddiviso i nostri piani in tabelle di marcia di tre mesi. Al momento, stiamo lavorando per gettare le basi per tutti i temi e continuiamo a lavorare sui compiti che non siamo riusciti a svolgere negli ultimi mesi. Stiamo inoltre perfezionando tutti i piani in modo dettagliato per migliorare la nostra esperienza lavorativa.
