---
title: 'DisNews #1 - Nuovo core member; finanziamento per le mailbox con crittografia; finestra di manutenzione'
media_order: Protest_in_Myanmar_against_Military_Coup_14-Feb-2021_05.jpg
published: true
date: '27-03-2021 14:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - mailgate
body_classes: 'single single-post'
---
Ci siamo lasciati alle spalle i primi tre mesi, ma nonostante le nostre speranze non siamo ancora riusciti a tornare alla normalità. Anzi, sembra che le cose stiano peggiornando e non si vede la luce in fondo al tunnel. Vorremmo quindi inviare un caloroso abbraccio a tutti coloro che si sono dovuti confrontare con questa situazione particolare. Rimaniamo solidali e uniti, cercando di sostenerci a vicenda.

Quest'anno abbiamo deciso di cambiare il modo di comunicare le novità. Invece di postare solo sui social e sul blog, comunicheremo anche attraverso una newsletter che cercheremo di inviare in modo regolare. Eccoci quindi qui a scrivere il nostro primo post di quest'anno.

## avg_joe nuovo core team support member

avg\_joe che alcuni di voi potrebbero conoscere dalla nostra [Community chat hangout room](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org), è stato via via sempre più coinvolto nel progetto Disroot. Gli abbiamo quindi chiesto se desiderava aiutarci e ha gentilmente accettato. Nelle ultime settimane ha già partecipato ai nostri incontri bisettimanali e da pochi giorni ha pure iniziato a rispondere ai ticket di supporto. Il suo innesto ci ha già dato una nuova prospettiva sulle cose e stiamo lavorando su alcuni miglioramenti che ci ha suggerito. Siamo felicissimi che avg_joe abbia deciso di impegnarsi nel progetto Disroot e speriamo che si divertirà a lavorare per la comunità quanto ci stiamo divertendo noi. Diamogli quindi un caloroso benvenuto, soprattutto quando risponderà ai ticket aperti.


## Piano 2021 per crittografare le caselle di posta e finanziamento NLNET

L'anno scorso abbiamo iniziato a lavorare sull'implementazione della crittografia per le caselle di posta. Abbiamo optato per la crittografia ent-to-end basata su GnuPG. Avevamo pianificato di implementare il servizio entro fine 2020, ma le cose sono andate diversamente.
In ottobre [NLNET] (https://nlnet.nl/thema/NGIZeroPET.html) ha pubblicato un invito a presentare proposte di progetti da finanziare. Abbiamo presentato il nostro progetto e la nostra domanda è stata accolta. Stiamo quindi ricevendo i finanziamenti e lavorando al progetto. Questo significa che per la prima volta nella storia del progetto Disroot siamo riusciti ad ottenere dei finanziamenti per sviluppare una funzionalità che speriamo possa essere apprezzata da tutti i Disrooters e non solo. Stiamo pianificando la campagna per la nuova funzionalità e preparando la documentazione sia per gli amministratori, sia per gli utenti finali. Vi aggiorneremo regolarmente sullo stato dei lavori. Il nostro obiettivo è quello di avere un closed testing funzionante entro la fine dell'anno e speriamo di poter fare dei beta test aperti su Disroot durante l'inverno. Quindi, a meno che non sperimentiamo un'invasione aliena, un'eruzione vulcanica o altre catastrofi, speriamo che la crittografia delle caselle di posta possa arrivare su Disroot.

Ancora una volta, un super grazie a NLNet per l'aiuto!

## Finestra di manutenzione bisettimanale

Una delle cose che abbiamo deciso di introdurre di recente è una finestra di manutenzione bisettimanale (il martedì dalle 20:30 (CET)). Durante queste finestre lanceremo le nuove versioni dei servizi che ospitiamo, implementeremo nuove e grandi funzionalità o eseguiremo altri lavori di manutenzione che potrebbero causare tempi di inattività.
Tutto ciò sarà annunciato tramite i nostri account sui social (mastodon / hubzilla) e pubblicato su https://state.disroot.org (a cui puoi iscriverti via e-mail o rss) e la nostra stanza xmpp su xmpp: state@chat.disroot.org?join

Riteniamo che questo approccio renda più chiaro, sia per voi che per noi stessi, su cosa lavoreremo e quale servizio verrà interrotto nel tempo. Inoltre, rendendolo un evento ricorrente e di routine, si eviteranno interruzioni casuali dei servizi e si consentiranno procedure chiare e facili da seguire. 

Per rendere il processo più trasparente, abbiamo creato un repository git dove è possibile seguire la cronologia (changelog) dei cambiamenti. Il prossimo passo è fornire quel log sul nostro sito Web per permettere in un colpo d'occhio di capire cosa è cambiato.

Ci sono un sacco di cose in fase di sviluppo e ancora di più in cantiere. Con l'avvicinarsi della primavera, stiamo diventando più attivi ed entusiasti e sicuramente vogliamo investire quell'energia in Disroot. 

Restate quindi sintonizzati per ulteriori notizie. 

