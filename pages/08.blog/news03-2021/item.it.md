---
title: "DisNews #3 - Progressi con Lacre; Domini personalizzati; Nuova dashboard; Muppeth ha lasciato il suo lavoro; terza quota per i volontari"
media_order: news_image.jpg
published: true
date: '20-11-2021 14:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - lacre
        - custom_domain
        - volunteer
        - dashboard
body_classes: 'single single-post'
---

Ciao Disrooters.

*È da un po' che non ci sentiamo. In effetti questo post è stato redatto già ad agosto ma, come spesso accade, la vita si è messa in mezzo e le cose sono state leggermente ritardate prima che ci rendessimo conto che forse avremmo dovuto inviare la newsletter. Quindi fai finta di leggerla come se fosse agosto.* 

Siamo orgogliosi di farti sapere che Disroot, il 4 agosto, ha festeggiato il suo sesto compleanno. Siamo molto contenti di quello che siamo riusciti a fare fino ad ora e non vediamo l'ora di festeggiare altri 6 anni come questi. Quindi la prossima volta che bevi qualcosa, festeggia alla nostra salute.

Abbiamo iniziato questa Disnews con un tono super celebrativo. Vediamo cosa c'è di nuovo, anche perché è passato un po' di tempo dal nostro ultimo post.

# Ambiente di test di Lacre

Sebbene sia ancora in una fase embrionale, **Lacre**, il nostro progetto di crittografia delle caselle di posta elettronica, ha fatto dei passi avanti. Siamo riusciti a impostare un ambiente di test adeguato. Abbiamo fatto alcuni test iniziali e siamo pronti a portare avanti il progetto. **pfm** ha già lavorato sul codice, ha notato alcuni problemi ed è pronto per aggiornare la base di codice a Python3. L'attuale linea di condotta è la seguente: in primo luogo, vogliamo introdurre i test del codice. Quindi aggiorneremo la base di codice sul backend a Python3 e il frontend a php7/8 senza modificare alcuna funzionalità. Una volta eseguite queste operazioni, vogliamo eseguire le modifiche a monte. Questo sarà per noi il punto di partenza  che porterà **Lacre** su una rotta leggermente diversa rispetto al progetto originario. Ovviamente trasferiremo le modifiche in upstream, tuttavia, da quel momento in poi alcune delle funzionalità nuove o rimosse potrebbero non essere accettate dall'upstream (ad esempio la rimozione della funzionalità di decrittazione). Questo significa che potrebbe essere molto probabile che **Lacre** iniziarà a vivere di vita propria. Questo, pure essendo ancora all'inizio, è un momento molto entusiasmante per la crittografia delle caselle di posta elettronica. 

# Domini personalizzati

Finalmente! Dopo molto, molto tempo, abbiamo riattivato il servizio di **domini personalizzati**! Abbiamo deciso di cambiare un po' le regole del gioco. Il collegamento al dominio personalizzato rimane una "ricompensa" per una donazione. Tuttavia, a differenza della situazione precedente, questa volta il collegamento alla donazione viene effettuato per donazione singola ed è una funzionalità permanente! Inoltre, stiamo collegando il dominio non solo alla posta elettronica ma anche alla chat XMPP (sebbene quest'ultima funzionalità sia ancora sperimentale). Ciò significa che potrai anche chattare con il tuo nome di dominio personalazzato. Su richiesta questo può anche significare la possibilità di creare delle chat room con il proprio dominio.
Anche se abbiamo deciso che non devi più donare regolarmente, ti invitiamo a considerare di donare almeno l'equivalente di 12 caffè all'anno dal tuo bar preferito. *Ricorda inoltre che una donazione ricorrente ci consente di mantenere a galla il progetto, pagare le bollette per server e hardware e magari eventualmente mettere del cibo sulle nostre tavole*. 

Puoi richiedere il collegamento del dominio con la nostra email e chat XMPP tramite questo [**link**](https://disroot.org/en/forms/domain-linking-form).


# Nuova dashboard e nuovo tema per Searx

Abbiamo deciso di fondere la [**dashboard**](https://apps.disroot.org) con la nostra [**istanza di Searx**](https://search.disroot.org). Ora, invece di una semplice dashboard con pochissime funzionalità, abbiamo una vera e propria landing page che puoi usare come scorciatoia per tutti gli altri servizi **Disroot** e come luogo per cercare sul web. Siamo molto soddisfatti del risultato finale. Puoi inoltre scegliere tra **beetroot** (tema chiaro) e **darkbeet** (tema scuro). In futuro introdurremo più funzionalità nella pagina principale come avvisi su problemi o manutenzione programmata. Non vediamo l'ora di apportare miglioramenti alla pagina principale e aspettiamo i vostri feedback. Ci auguriamo che il cambiamento piaccia!


# Muppeth ha deciso di smettere...

Finalmente possiamo usare questo argomento come un "clickbait". **Muppeth ha deciso di smettere...**

...con il suo lavoro. 

> Dopo quasi sei anni passati a vivere praticamente due vite parallele senza riposo e con pochissime ore di sonno, ho deciso che non potevo più sostenere tutto questo senza compromettere la mia sanità mentale. Lasciare **Disroot** non era un'opzione in quanto è il progetto più importante della mia vita, quindi ho dovuto prendere la decisione di scegliere un'opzione più incerta, rischiosa ma sicuramente eccitante. Ho deciso che concentrerò tutti i miei sforzi su **Disroot** e proverò a trasformarlo (in un modo o nell'altro) in un progetto che permetta di portare del cibo sulla tavola della mia famiglia. Credo che progetti come **Disroot**, in cui è richiesta la presenza attiva 24 ore su 24, 7 giorni su 7, proprio come qualsiasi piattaforma commerciale, dovrebbero da una parte garantire una remunerazione etica, trattando dall'altra i propri utenti nel rispetto dei loro dati e della loro privacy. Credo che piattaforme migliori, più sostenibili ed etiche non dovrebbero essere escluse dal guadagnarsi da vivere senza sacrificare le proprie idee, e penso che questo possa essere realizzato da **Disroot** e dalla sua vivace comunità. Chi altri se non noi. 

Siamo molto felici che **Muppeth** abbia preso questa decisione molto difficile. Sosteniamo i suoi sforzi e cercheremo di trovare un modo per rendere **Disroot** finanziariamente sostenibile. Non ci vuole davvero molto per trasformare quel sogno in realtà. Se ogni disrooter decidesse di donare l'equivalente di un solo caffè al mese a sostegno della piattaforma che utilizza quotidianamente, potremmo aggiornare l'hardware su cui eseguiamo disroot, donare ai progetti FLOSS (Free/Libre and Open Source Software) per far progredire ulteriormente il software che tutti noi usiamo quotidianamente e pagare tutti i salari di sussistenza del team. Non c'è bisogno di capitale di rischio, investimenti loschi, funzionalità orientate al profitto o data mining. 

Quindi non essere timido e [**dona**](https://disroot.org/en/donate).


# Terza quota per i volontari

Per dimostrare che quanto scritto sopra è a portata di mano, siamo orgogliosi di annunciare che **possiamo pagare una quota di volontariato a un altro membro del team principale**. Le quote di volontariato, in conformità alla normativa olandese, possono essere pagate da fondazioni senza scopo di lucro ai volontari, laddove la quota non superi i 1700€ all'anno. Abbiamo deciso che pagheremo una quota di volontariato di 140€ al mese se verranno soddisfatte determinate condizioni:

  - Dopo tutti i costi, la donazione ai progetti FLOSS e l'accantonamento di 400€, dobbiamo avere più di 140€ per un periodo di almeno tre mesi prima di iniziare a pagare una quota per volontario*(rete di sicurezza)*.
  - 140€ vengono pagati a un membro del team principale fino a quando non è stato raggiunto un margine di 140€ per tre mesi consecutivi *(cintura di sicurezza)*.

Vorremmo ringraziare tutti voi che decidete di contribuire finanziariamente a **Disroot**. Siamo molto grati di avere una tale comunità di Disrooters attorno al progetto❤️.


# Futuro di Framadate e di Ethercalc
Il software FLOSS è in continua evoluzione e noi, come parte di quel movimento, ne siamo influenzati. Il software in generale si evolve, continua a cambiare e migliorare, ma a volte smette di progredire o addirittura si estingue.
È il caso di **Ethercalc**, il software che alimenta il nostro servizio su [**https://calc.disroot.org**](https://calc.disroot.org) e pure di **Framadate** che alimenta [**http://poll.disroot.org**](https://poll.disroot.org). Vedendo lo stato di entrambi i progetti, abbiamo iniziato a chiederci se aveva senso continuare a fornirli visto che né **Framadate** né **Ethercalc** riceveranno nuove funzionalità e nel tempo potrebbero verificarsi problemi di sicurezza senza patch. 

Allo stesso tempo abbiamo iniziato ad adorare, **CryptPad**, una suite di collaborazione a conoscenza zero crittografata end-to-end completa che alimenta il nostro [**https://cryptpad.disroot.org**](https://cryptpad.disroot.org). [**https://cryptpad.disroot.org**](https://cryptpad.disroot.org) include funzionalità di sondaggio e foglio di calcolo. **CryptPad** è attivamente sviluppato e fornisce qualcosa che alla luce dei recenti cambiamenti nella legislazione non solo nell'UE ma in tutto il mondo, sta diventando molto più importante: la crittografia end-to-end. Si tratta del tipo di crittografia in cui i dati sui server vengono archiviati in un modo che nemmeno gli amministratori con accesso completo all'hardware del server potrebbero decrittografare. Sappiamo che entrambi questi servizi sono stati con noi sin dall'inizio di **Disroot** e sono stati utilizzati da molti Disrooters. Vorremmo conoscere il tuo feedback su questa situazione e vedere cosa ne pensi. Puoi utilizzare qualsiasi forma di contatto elencata sul nostro [**sito web**](https://disroot.org/contact). Abbiamo creato un semplice sondaggio ([**Framadate/EtherCalc vs CryptPad Poll**](https://cryptpad.disroot.org/form/#/2/form/view/t+aoDjPYZRjdT-wZQer3TdKJn8X3NKJhbDQHvc+yNKg/)
) in modo da poter ottenere qualche *stima* sulle opinioni generali sull'argomento. 

Di nuovo grazie per aver condiviso con noi questi 6 anni! È stato un piacere condividere questa avventura straordinaria. E il bello deve ancora venire visto che il viaggio è appena iniziato! 

E naturalmente grazie per il tuo supporto, feedback, amicizia, per le tue lacrime e per il tuo amore. Grazie per aver preso in considerazione l'idea di usare quella piccola isola nel mare di Internet! Ci vediamo in giro! 