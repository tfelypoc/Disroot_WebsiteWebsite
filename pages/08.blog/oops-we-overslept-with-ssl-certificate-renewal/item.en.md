---
title: 'Oops… We overslept with SSL certificate renewal.'
date: 06/26/2016
taxonomy:
  category: news
  tag: [disroot, news, ssl]
body_classes: 'single single-post'

---

**Ehhhh SSL.**

As we are working on writing Ansible roles to automate most of the work done on disroot platform. We thought it’s great opportunity to update all our HTTPS certificates with  letsencrypt. Last week we started implementing Letsencrypt  ansible role on disroot platform, not realising our certificates are about to expire. And today was the day. We woke up to Disroot being crippled by inaccessible websites due to expired certs. Just a few days before our ansible role was polished, tested and put in production. We quickly re-issued most of the certificates with STARTSSL to extinguish the fire.

In next week or so we will move entire infrastructure to letsencrypt, meaning that certificate renewal nightmare will be a thing of the past.

Hope the downtime was not that noticeable. After all it’s Sunday
