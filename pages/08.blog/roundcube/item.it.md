---
title: 'Nuova webmail!'
media_order: roundcube.png
published: true
date: '20-04-2020 00:30'
taxonomy:
    category:
        - news
    tag:
        - roundcube
        - webmail
        - news
body_classes: 'single single-post'

---
Cari Disrooters,
a partire dall'8 giugno, andremo a sostituire l'attuale webmail (**Rainloop**) con un nuovo software: **Roundcube**.

Negli ultimi 4 anni abbiamo utilizzato **Rainloop**, ma a causa di alcune funzionalità mancanti e della necessità di migliorare l'integrazione della piattaforma, abbiamo deciso di introdurre un ambiente di webmail basato su **Roundcube**.

**Roundcube** è un software ampliamente testato negli ultimi 10 anni e offre una soluzione per webmail moderna, veloce, semplice e affidabile. Ecco alcuni punti salienti delle sue funzionalità:

* Una interfaccia grafica integrata in Disroot (anche per tema scuro scuro)
* Miglior supporto per i filtri
* Archiviazione email e mailbox
* Temi personalizzati
* Interfaccia tradotta in oltre 80 lingue
* Funzione "drag & drop" per la gestione dei messaggi: sposta i messaggi con il mouse
* Pieno supporto per messaggi MIME e HTML
* Anteprime degli allegati: vedi le immagini direttamente nei tuoi messaggi
* Elenco dei messaggi "thread": visualizza le conversazioni in modalità threaded
* Cartelle: gestisci le tue cartelle (aggiungi, rimuovi o nascondi)
* Autentificazione a due fattori: aggiungi un secondo livello di sicurezza per l'accesso alla tua webamil

E molto altro...

Per il futuro prossimo stiamo pianificando di integrare la crittografia delle casselle di posta, così da affiancarla a **Nextcloud** (calendari, contatti e allegati) e altri servizi attualmente forniti da **Disroot**. Riteniamo che **Roundcube** ci consentirà di migliorare ulteriormente la tua esperienza di posta elettronica da web.


Speriamo che questa implementazione sia di tuo gradimento. Noi siamo impazienti di implementare **Roundcube**!

**!! Avvertimento per utenti con 2FA attivo!!**

Per quelli di voi che ora hanno l'autentificazione a due fattori attiva, sarà necessario reimpostare la 2FA nuovamente una volta che la nuova webmail sarà attiva e funzionante. Ci dispiace per l'inconveniente!
