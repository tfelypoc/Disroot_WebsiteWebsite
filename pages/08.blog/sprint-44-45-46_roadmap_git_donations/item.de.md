---
title: 'Fahrplan und eine neue Welle von FLOSS-Spenden, Git und eine neue Hauptseite'
date: '07-11-2019 16:00'
media_order: roadmap.jpg
taxonomy:
  category: news
  tag: [disroot, news, git, services, donations, floss, roadmap]
body_classes: 'single single-post'

---
*(Sprint 44-45-46)*
<br><br>
Dieser Blogbeitrag ist seit zwei Monaten überfällig! Irgendwie, obwohl er sogar schon entworfen war, wurde er immer wieder auf der Prioritätenliste nach unten geschoben bis in den Abgrund der vergessenen Aufgaben. Es ist Zeit, ihn auszugraben und ein bisschen was über die Dinge zu schreiben, mit denen wir zuletzt beschäftigt waren.

## Fahrplan Q4 2019

Wir beginnen mit Verbesserungen unserer internen Organisationsstruktur. Deshalb haben wir enschieden, einen Fahrplan für das letzte Quartal dieses Jahres zu entwerfen. Das wird uns, so hoffen wir, in die Lage versetzen, dass unsere Pläne besser funktionieren und dass wir einen klaren Blick und Ziele vor uns haben. Unsere Absicht ist es, diese Art der Planung in 2020 und darüber hinaus fortzuführen. Wir haben vor, zwei Arten von Fahrplänen zu nutzen:

  - **Quartalsweise** - Wobei wir kurzfristige Ziele setzen und uns auf spezifische Projekte konzentrieren.
  - **Halbjährlich** - Um die Sicht auf das große Ganze nicht zu verlieren, wollen wir zweimal jährlich Langzeitziele definieren, wodurch wir unsere Arbeit, Ziele und Visionen für das Projekt besser organisieren können.
<br><br>


Wir haben festgestellt, dass wir häufig dazu tendieren, unseren Fokus zu verlieren oder Teile des Projekts zu vernachlässigen aus Gründen limitierter Ressourcen oder fehlender, klarer Visionen und Pläne. Fahrpläne werden uns dabei helfen, unsere Aufmerksamkeit auf einem gemeinsamen Ziel zu halten. Wie gut das klappt, wird die Zeit zeigen.
<br><br>
**Auf was werden wir uns nun für den Rest des Jahres konzentrieren?**
<br><br>
Von September bis Dezember wollen wir die organisatorische und strukturelle Arbeit beenden, mit der wir bereits begonnen haben: Restrukturierung der internen Organisation, die Art, wie wir arbeiten, interagieren, Entscheidungen treffen, die Verantwortlichkeiten teilen. Das beinhaltet sowohl die Suche nach den besten Lösungen, ihrer Dokumentation, die Verbesserung unserer Arbeitsabläufe und -prozeduren als auch die Ausarbeitung einer wesentlich effizienteren Administration. Zusätzlich wollen wir uns auf kleine Verbesserungen der UX (User Experience / Nutzererfahrung) und UI (User Interface / Benutzeroberfläche) konzentrieren, insbesondere bei allem, was neue Nutzer betrifft. Nachdem wir **Disroot** nun schon einige Zeit betreiben, haben wir das Gefühl, uns von der Zeit entfernt zu haben, als wir die Plattform in Leben gerufen haben oder ihr anfänglich als Nutzer beigetreten sind. Wir haben einige der Themen definiert und sammeln immer noch Rückmeldungen zu möglichen Verbesserungen. Eine weitere, sehr wichtige Aufgabe ist das Aufräumen, Automatisieren und Verbessern aller serverseitigen Aufgaben. **Disroot** kostet mehr und mehr wertvolle Zeit der Admins und die Einführung von Standardisierung, Automatisierung, Scripting und Verbesserung der täglichen Arbeit wird uns helfen, einen Teil dieser Zeit zurück zu gewinnen, die wir dann wieder dazu nutzen können, die Anwendungen weiter zu verbessern und unsere Verbesserungen auch einzubringen.
<br><br>
Um den Fortschritt zu verfolgen, kannst Du Dir unser [Epic auf Taiga](https://board.disroot.org/project/disroot-disroot/epic/2167) ansehen.

## Verbesserungen im Supportticket-System

Meaz hat hart daran gearbeitet, Ordnung in die Ticket-Warteschlange zu bringen. Zuletzt war diese sehr chaotisch geworden, was sowohl unsere Antwortzeit beeinflusst als auch dafür gesorgt hat, dass einige Tickets im Stapel 'verloren gegangen' sind. Die Erfahrung des Aufräumens hat zur Entwicklung eines Ablaufs geführt, der den Umgang mit den Tickets, die Erstellung detaillierter Ticket-Warteschlangen die Einführung von Schichten beinhaltet. Die Einführung von Schichten bedeutet, dass jede Woche eine Person die Rolle des "Ticket Masters" übernimmt und damit den Ablauf steuert, indem sie Tickets delegiert, sicherstellt, dass sie in vernünftiger Zeit beantwortet werden, und, und das ist das Wichtigste, verhindert, dass Tickets 'vergessen' werden. Das wird uns definitiv helfen, wesentlich effizienter zu arbeiten.

## Spenden an FLOSS

Unsere letzten Spenden an FLOSS-Projekte haben wir im Mai getätigt. Wir haben seitdem jeweils eine bestimmte Menge Geld beiseite gelegt, basierend auf den Zielberechnungen, und nun ist es an der Zeit, ein weiteres Bündel Geld an die Projekte zu übergeben. Diesmal haben wir uns für die folgenden Spendenempfänger entschieden: **Hubzilla, ConverseJS, Etherpad, Framasoft, DavX5, Nextcloud bounties** *(noch nicht veröffentlicht)*, **Conversations and Debian**. *(die Spenden sollten in den nächsten Tagen ausgezahlt werden)*
<br><br>
Im Folgenden haben wir eine Übersicht erstellt, die all die Spenden aufzeigt, die wir in diesem Jahr an all die Projekte ausgezahlt haben. Einen aktuellen Überblick über unsere Finanzen, die Zahl der unterstützenden Disrooter und eine Übersicht über die Spenden, welche wir an FLOSS-Projekte getätigt haben, findest Du auf unserer [Spenden-Seite](https://disroot.org/de/donate)

![](donated.png)

Wir danken Euch allen für Eure großartige Unterstützung und senden Grüße an all die FLOSS-Entwickler, die unzählige Stunden arbeiten für bessere, offenere Software ♥♥♥

## Neue Titelseite

Meaz und Antilopa waren damit beschäftigt, unserer Titelseite ein neues, aufgefrischtes Aussehen zu verpassen. Uns gefällt es sehr und wir hoffen, Ihr mögt es auch. Das der erste Schritt einer Reihe von Aufgaben, bei denen wir uns auf die Vereinheitlichung des Aussehens sowohl der Website als auch der Anwendungen konzentrieren wollen.

## Opt-in Passwortrücksetzungs-Email

Eine der am häufigsten wiederkehrenden Support-Anfragen ist die der Passwort-Rücksetzung. Viele von Euch haben sich registriert und bis zu dem Zeitpunkt, als ihr Account freigegeben wurde, ihr Passwort vergessen, was zu einem gesperrten Account führt. So wenig wir auch Email-Adressen Dritter sammeln wollen, so scheint es doch der einzige Weg zu sein. Daher kannst Du von nun an während der Registrierung auswählen, dass die Email-Adresse zur Verifikation (die normalerweise nach Freigabe/Ablehnung aus unseren Datenbanken gelöscht wird) als Email-Adresse zur Passwort-Rücksetzung genutzt werden soll. Du kannst das auch später einstellen, indem Du Dich über [https://user.disroot.org](https://user.disroot.org) anmeldest und die Option "Profil aktualisieren" auswählst. Bedenke, dass dies absolut **optional** ist und wenn Du Deine externe Email-Adresse nicht mit uns teilen möchtest oder keinen Sinn darin siehst, brauchst Du das auch nicht tun (pass gut auf Deine Passwörter auf und Du wirst nichts davon benötigen).

## Unsere Git-Instanz

Seit einiger Zeit haben wir diskutiert, ob wir mit unseren derzeitgen Git-Repositories, die wir bei unseren Freunden der [Fosscommunity India](https://git.fosscommunity.in) hosten, woanders hin umziehen. Wir mögen die Einfachheit von Gitea im Speziellen, während wir die Extra-Funktionen von Gitlab niemals wirklich genutzt haben und auch nicht wirklich begeistert sind von der UI. Während wir nach einem neuen Zuhause suchten, kamen wir zu dem Schluss, dass das Selbsthosting unserer eigenen Git-Instanz möglicherweise für uns, unsere Übersetzungs-/Howto-Crew und tatsächlich für jeden in der Gemeinschaft, der nach einem neuen Zuhause für seinen Code sucht, das beste sei. Wir arbeiten noch an einigen Details bevor wir live gehen, aber wir hoffen, dass wir es diese Woche fertigstellen Website und Howtos von **Disroot** dorthin migrieren können.

Du kannst es Dir unter [https://git.disroot.org](https://git.disroot.org) ansehen und Dich, wenn Du möchtest, registrieren. Wir haben uns entschieden, eine separate Login-Methode zu nutzen (Du kannst Dich nicht mit Deinen **Disroot**-Daten anmelden), weil wir denken, dass, um die Kooperation von Nicht-Disrootern anzuregen, der Zwang, den Aufwand der Erstellung eines kompletten **Disroot**-Accounts zu durchlaufen, nur um einen Fehler mitzuteilen oder Änderungen einzureichen, nicht gerade motivieren auf die Leute wirken dürfte. Des Weiteren erwägen wir, github-/gitlab-Logins hinzuzufügen, um es für Außenstehende noch einfacher zu machen, sich zu beteiligen.

## Was noch kommt...

In den letzten Wochen kümmert sich **Fede** federführend um die Erstellung einer neuen Version unserer Datenschutzerklärung, wodurch hoffentlich die Erklärung verständlicher und detaillierter wird. **Meaz** nimmt weitere Optimierungen der Website vor, während **Antilopa** an vernünftigen projekteigenen GUI-Leitfäden. **Muppeth** war damit beschäftigt, das Server-Setup zu reparieren, verbessern und dokumentieren.
