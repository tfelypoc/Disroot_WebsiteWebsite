---
title: 'End-of-the-Year sticker design competition'
media_order: stickers.jpg
published: true
date: '07-12-2020 21:00'
taxonomy:
    category:
        - news
    tag:
        - news
        - contest
body_classes: 'single single-post'
---

Last year, our End-of-the-Year fundraising was a success, and something really fun to do. We thought we should end this crazy year with something fun and creative as well. For a while we have been wanting to provide more gifts to all Disrooters donating money to keep the platform afloat. While making a traditional logo sticker is nice, we thought it would be cool if Disrooters themself design some cool stickers. So we thought of organizing a little contest.

The goal is simple. **Create a sticker design you like and drop it to [this url](https://cloud.disroot.org/s/FP9HPEXbfKb6ZQQ) before the 25th of December**. Winning stickers will be printed and send as a sticker-pack to Disroot donators throughout 2021.

We will organize a poll so anyone could cast a vote for the stickers they like the most. The winners will receive a sticker pack as a New Year present first ;)

Here are the specifications:

* Be creative
* Your sticker design does not have to be Disroot specific, you can do FLOSS, federation, Fediverse, open hardware related sticker designs too (we are all one family).
* Go wild
* Send svg files (preferably, though you could drop png too)
* Fuck 2020 (would love to see that sticker too 😂)

Your sticker design should have Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

So, get your graphic tablet, mouse, a pen or whatever you need, and draw the most good looking Disroot sticker! We can't wait for your designs and we will join the competition in too :).

Do not hesitate to share it with the hashtag **#Distickeroot** on your favorite social networks!

**The Disroot Team**
