---

title: "This year couldn't become any crazier"
date: '16-03-2020 20:00'
media_order: Italians-sing.jpg
taxonomy:
category: news
tag: [disroot, news, privacy, hubzilla, email]
body_classes: 'single single-post'

---
Already a couple of sprints of 2020 are behind us. Lots of new things we've been working on and going trough a bit of a chaos. Therefore we decided to change the length of our planned short term work (sprint) from 2 to 4 weeks. This gives us a bit more breathing space and slows down the pase a little. Additionally we've went though some extra administrative improvements on how we divide and oversee each-other work, and give feedback. This hopefully will reduce chaos and improve the work comfort we had in last few weeks.

Ok, so let's jump right into what's new.

## Privacy Statement and Terms of Service version 1.2

For a while now we've been working on new version of our **Privacy Statement**. **Fede** took a leading role in getting all the ideas and missing information together and thanks to him we have a new version of the documents. There are no major changes to the essence of the document, although there are notable changes in the organization, structure and clarity of content. We focused on making it as clear and detailed as possible so that it is easy to read and understand. And, of course, also to comply with the **GDPR** guidelines. We added detailed information about the specifics of each service provided by **Disroot.org**. We hope you will appreciate the work and that things are more clear. Additionally, in order to better follow changes to the **Privacy Statement** we have created a git repository (version control system) [here](https://git.disroot.org/Disroot/Disroot-Privacy-Policy/commits/branch/master) where you can follow all the changes to the document.

Hopefully it will also help all users of the online services to be more responsible with their data - regardless of the continent they live in, the regulations in force, the providers they use and the needs they have - and to know how to defend it.

**Terms of Service**'s changes are also cosmetic besides new addition of minimum age requirement which we set to be 16 years old. Similarly to **Privacy Statement** changes to the document can be followed [here](https://git.disroot.org/Disroot/Disroot-TOS/commits/branch.master).

## Etherpad and XMPP data storage retention

Two other notable changes in the new Privacy Statement is the modification in the retention times for data stored on both, **Etherpad** and **chat(XMPP)**.

For Etherpad (as well as Ethercalc) pads that have been untouched for 6 months will be removed from the server.

XMPP Chat history storage period was also changed from 6 to 1 month. After consulting with other Admins, XMPP server developers and users, we concluded that the six months value was oversized. Since our devices store the history, server should store just a minimum and reasonable amount and we think one month is enough.

This change will be effective starting from 1st April. This gives you three weeks to prepare if you need to.

## Preparation work

We spent January and February on preparing the ground for all upcoming work for our main themes: **email improvements and Hubzilla launch**. All this will allow us to properly work on things like mailbox encryption, better spam protection, new webmail, as well as ton of improvements on our Hubzilla instance (DisHub) and our Android Hubzilla app called Nomad. As boring as it seems, this work was essential for the flashy improvements to come.

## Update to Nextcloud 18

Yup. Finally has landed. Last weekend we have rolled out the update a lot of you have been waiting for. Quick disclaimer. OnlyOffice solution still does not seem to be supported by the server side encryption we use, however we are still looking into it to confirm for sure.

Here's some highlights of the new features available:

 - **File app** - Folder header allows to add additional information in a form of readme file displayed on top of the folder. You can now see the list of users you share the file with.
 - **Photo app** - The Gallery app has been replaced by the Photo app. It looks refreshed and allows to share photos easier.
 - **Calendar app** - You can now create a Talk room directly from the Calendar app, which allows to collaboratively discuss an event.
 - **Change ownership** - Allows for easy transfer the ownership of a file or a folder to another user.
 - **Video verification of password protected files** - Enable video verification of your password enabled shares for better security


## The Phenomenal 2019 End of the Year Bonus Alias Challenge

In December 2019, the goal for our fund-raising campaign was reached! We've raised 1500€. We would like to thank once again to everyone who decided to donate to Disroot throughout the entire year! We are very grateful.

As for now, each Disrooter has a new alias: *yourusername@getgoogleoff.me*

If you don't know how to set it up, check our [tutorial](https://howto.disroot.org/en/tutorials/email/alias)

## Roundcube testing

As some of you know, we are planning to replace current Rainloop webmail with Roundcube. The reason for that is that we believe that Roundcube can better integrate with what we are planning to do in the near future (wkd, gpg) but also looks to be much more polished email solution. Additionally we want to do better Nextcloud and XMPP integration in the webmail and so we decided to Roundcube is the platform we could achieve that. For those who want to have a sneak peak and help us with feedback, suggestions, and shape the new webmail, you can already use it at http://roundcube.disroot.org Just bare in mind this is test instance which means things can break or we can wipe your settings (not your emails though).

## Coming up

This month we continue the preparation work. We are working on improvements to custom domain linking which, as some of you experienced, feels like a never ending long waiting chore.

To provide better overview for ourselves and perhaps Hubzilla community at large, we want to document our ideas for visual improvements to it. This will, on one hand, allow us to set a goal on how we want our Hubzilla instance to look and feel (we are not only talking about theme but overall user experience), but also, we hope, this will help Hubzilla developers to improve this amazing piece of software.

One other thing we plan to push this month is our Yearly Report which as you noticed is a bit overdue given it is already March. But, hey, it was a really busy end and start of the year.

**Everyone, stay safe, take care of yourself and your close ones. Stay home if you can, and let's get through this weird surreal time fast and without too much totalitarian, dystopian aftermath to deal with later.**
