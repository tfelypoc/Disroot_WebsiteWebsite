---
title: 'Time to switch some lights off'
date: '14-10-2020 10:00'
media_order: belarus.jpg
taxonomy:
  category: news
  tag: [disroot, news, matrix, diaspora, nextcloud]
body_classes: 'single single-post'

---

**Switching off Matrix and Diaspora.**

### Matrix

As you might remember, two years ago we have decided to no longer provide Matrix as a service ([Blog post](https://disroot.org/en/blog/matrix-closure)). Reason for that decision, apart from a growing un-addressed privacy concerns, was also the huge server resources needed to operate what is essentially a Text based chat application. We have decided to go back to the roots and rather focus on providing XMPP as our go-to chat solution. Since the announcement we have been slowly removing inactive accounts. At this moment we are left with only a handfull of users, but still the amount of resources that are used is unacceptably big: our current Matrix server happily consumes about 5GB of RAM (database ram usage not counted), and takes 170GB of database storage. We find this a tremendous waste of resources and so we have decided it is time to shut the lights off. And so, by 1st of December we will switch off our Matrix server entirely. We hope those who are still using it will understand our decision and we hope they will find a new home on another homeserver.

### Diaspora

Since the announcement of phasing out our Diaspora instance ([Blog post](https://disroot.org/en/blog/demise-diaspora)), we noticed huge drop in our pod active users. We have reached the state of just few users within first two months. Therefore, unlike Matrix phasing out, the decision to switch the lights off on Diaspora\* aged much faster. Just like with Matrix, on 1st of December our pod will no longer be part of the federated network.

Once again we wanted to thank everyone involved during the years of providing us with the software that was one of the big inspirations to start Disroot in the first place. Maybe one day we will meet again in the federated multiverse.

# New Nextcloud apps

Recently we have enabled few new apps on Nextcloud some of you were requesting for a while. Those are:

  - **Forms** - Without too much distracting options. To the point, simple surveys and questionnaires app. Forms allows for export of results to CSV file similar to what Google forms does. <https://apps.nextcloud.com/apps/forms>
  - **Appointments** - Book appointments into your calendar via secure online form. Attendees can confirm or cancel their appointments via an email link. <https://apps.nextcloud.com/apps/appointments>
  - **Cospend** - Nextcloud Cospend is a group/shared budget manager. You can use it when you share a house, when you go on vacation with friends, whenever you share money with others. <https://apps.nextcloud.com/apps/cospend>

We hope you will like the additional apps and find them useful in your daily life. We are already using it internally and enjoying it quite much. Some detailed tutorials will follow, however if you want to give us a helping hand, you can join the Howto community at howto@chat.disroot.org

# Nextcloud performance hit

As all of you probably noticed, the past weeks we experience some issues with some services. The cause of this is unfortunately Nextcloud. We seem to be hitting some issue which we yet need to find the root cause of. For now we have moved some services affected by the Nextcloud's performance hit to another box which means that at least those will work properly. In comming days we will try to focus on the issue and find the root cause. Sorry for this turbulence.
