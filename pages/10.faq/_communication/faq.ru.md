---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "КОММУНИКАЦИИ"
section_number: "100"
faq:
    -
        question: "Как использовать IRC Disroot через XMPP?"
        answer: "<p><b>Disroot</b> предоставляет так называемый шлюз IRC. Это означает, что вы можете присоединиться к любой комнате IRC, размещенной на любом сервере IRC. Это основная схема адреса:</p>
        <ol>
        <li>Войти в IRC-комнату: <b>#room%irc.domain.tld@irc.disroot.org</b></li>
        <li>Добавить IRC-контакт: <b>contactname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Где <b>#room</b> является комнатой IRC, в которую вы хотите войти, <b>irc.domain.tld</b> это адрес IRC-сервера, к которому вы хотите присоединиться. Не забудьте оставить <b>@irc.disroot.org</b> без изменений, потому что это адрес шлюза IRC.</p>"
        
---
