---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SOBRE CIFRADO"
section_number: "200"
faq:
    -
        question: "¿Puedo utilizar 2FA (Autenticación de dos factores)?"
        answer: "<p>Sí, puedes usarlo para la <b>Nube</b>. Pero, antes de habilitarlo, asegúrate que entiendes plenamente cómo funciona y cómo usarlo. Para más información, ve <a href='https://howto.disroot.org/es/tutorials/cloud/settings#two-factor-authentication' target='_blank'>aquí</a></p>"

    -
        question: "¿Puedo usar cifrado de extremo a extremo (E2EE) en la Nube?"
        answer: "<p>Actualmente, el cifrado de extremo a extremo está deshabilitado debido a un error de larga data en la aplicación de escritorio de <b>Nextcloud</b>.</p>"


---
