---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "CIFRATURA"
section_number: "200"
faq:
    -
        question: "Posso utilizzare il sistema di autentificazione a due fattori (2FA)?"
        answer: "<p>Sì, puoi utilizzarlo per il <b>Cloud</b>. Attenzione, prima di abilitarlo, assicurati di comprenderne appieno il funzionamento. Per maggiori informazioni leggi <a href='https://howto.disroot.org/it/tutorials/cloud/settings#two-factor-authentication' target='_blank'>qui</a></p>"
    -
        question: "Posso utilizzare la cifratura end-to-end nel cloud?"
        answer: "<p>Attualmente la cifratura end-to-end è disabilitata a causa di un bug di vecchia data con l'app desktop <b>Nextcloud</b>.</p>"


---
