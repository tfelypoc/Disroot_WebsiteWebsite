---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "GENERALE"
section_number: "300"
faq:
    -
        question: "Cosa significa Disroot?"
        answer: "<p><b>Dis'root'</b>: Sradicare, rimuovere le radici o dalle radici, separare da una base; estirpare.</p>"
    -
        question: "Durerà nel tempo Disroot?"
        answer: "<p>Gli amministratori e mantainers di <b>Disroot</b> usano quotidianamente i servizi di Disroot e dipendono da essi come principale strumento di comunicazione. Anche per questi motivi intendiamo quindi continuare a lungo con il progetto. <a href='https://forum.disroot.org/t/will-disroot-last/101' target='_blank'>Guarda il forum per una risposta più completa</a></p>"
    -
        question: "Quanti utenti utilizzano Disroot?"
        answer: "<p>Non teniamo traccia degli utenti attivi, quindi non possiamo rispondere a questa domanda. Inoltre, riteniamo che non sia affatto un modo per misurare lo stato di salute della piattaforma. I numeri degli utenti sono suscettibili a manipolazione per molte ragioni. Poiché gli scopi della piattaforma non hanno nulla a che fare con tutto ciò, troviamo non abbia senso fornire statistiche false o manipolate.</p>"

---
