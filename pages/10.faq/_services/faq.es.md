---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SOBRE LOS SERVICIOS"
section_number: "400"
faq:
    -
        question: "¿Qué servicios puedo usar con mi cuenta de Disroot?"
        answer: "<p>Tu cuenta te permite utilizar los siguientes servicios con el mismo nombre de usuarix y contraseña:</p>
        <ul class=disc>
          <li>Correo electrónico</li>
          <li>Nube</li>
          <li>Forum</li>
          <li>Foro</li>
          <li>Tablero de Proyectos</li>
        </ul>
        <p>El resto de los servicios (<b>Pads, PasteBin, Subida, Buscar, Encuestas</b>) no requieren ningún registro.</p>"
    -
        question: "¿Qué usos puedo darle a mi cuenta de correo electrónico?"
        answer: "<p>Puedes utilizarla de la manera que quieras, excepto con fines comerciales o enviar SPAM. Para tener una mejor idea de lo que puedes y no puedes hacer con el correo, por favor, lee los párrafos 10 y 11 de nuestras <a href='https://disroot.org/en/tos' target='_blank'>Condiciones de Uso</a></p>"
    -
        question: "¿Cuál es el tamaño de la bandeja de correo y de la nube?"
        answer: "<p>El límite del <b>buzón</b> es de <b>1GB</b> y el <b>tamaño de los adjuntos</b> es de <b>50MB</b>.</p>
        <p>El <b>almacenamiento en la nube</b> es de <b>2GB</b>.</p>
        <p>Es posible aumentar el almacenamiento del correo y la nube. Revisa las opciones <a href='https://disroot.org/en/forms/extra-storage-space/' target='_blank'>aqui</a></p>"
    -
        question: "¿Puedo ver algún estado de los servicios como el uptime (el tiempo de funcionamiento), los mantenimientos programados, etc. ?"
        answer: "<p>Sí. Y hay múltiples maneras de estar actualizadx con respecto a los problemas, el mantenimiento y la información sobre la salud general de la plataforma.</p>
        <ul class=disc>
        <li>Visita <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Suscríbete a la fuente RSS de <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Suscríbete para recibir notificaciones por correo de <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Sigue a <b>disroot_state@hub.disroot.org</b> via Hubzilla, Diaspora*, Mastodon, Pleroma, Pixelfed, etc. (el fediverso...)</li>
        <li>Sigue a <b>disroot@social.weho.st</b> (el fediverso, de nuevo...)</li>
        <li>Únete a <b>state@chat.disroot.org</b> (XMPP)</li>
        <li>Instala <b>DisApp</b>, la aplicación de <b>Disroot</b> para Android en la que recibirás notificaciones en tiempo real</li>
        </ul>"
---
