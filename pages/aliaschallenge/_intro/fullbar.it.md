---
title: 'Introduzione'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;" id="fund1">
Negli ultimi giorni di quest'anno, mentre il mondo intero si prepara a salutare la luce del nuovo decennio, tre domini si stanno battendo tra di loro. Chi la spunterà? **Unisciti alla battaglia e scegli il tuo dominio preferito**.
</span>

<span style="padding-right:30%;" id="fund2">
Dal 23 dicembre al 1 gennaio, partecipa al nostro evento di raccolta fondi di fine anno. **Dona a Disroot** utilizzando uno degli hashtag di dominio che trovi sotto come riferimento per la donazione. Il nome di dominio vittorioso verrà dato come **alias extra per tutti i Disrooter**.
</span>

## Il dominio più cool vince!
