---
title: 'Ranking'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: center
---

<h1 style="color: #ffffff; font-size: 56px; font-family: 'kingthings_christmas'; line-height: 1.2;">Ranking</h1>


<div class="dots">
    <hr class="left"  style="width: 9vw;" /><h2 class="right"> 180€ </h2>
    <figure>
    <img src="/user/pages/aliaschallenge/_ranking/hacker.png" width="60">
    <figcaption>glorifiedhacker.net</figcaption>
    </figure>
</div>

<div class="dots" >
    <hr class="left" style="width: 49vw;" /><h2 class="right"> 972€ </h2>
    <figure>
    <img src="/user/pages/aliaschallenge/_ranking/google.png" width="60">
    <figcaption>getgoogleoff.me</figcaption>
    </figure>


</div>

<div class="dots" >
    <hr class="left" style="width: 24vw;" /><h2 class="right"> 476€ </h2>
    <figure>
    <img src="/user/pages/aliaschallenge/_ranking/cat.png" width="60">
    <figcaption>morecats.net</figcaption>
    </figure>
</div>
