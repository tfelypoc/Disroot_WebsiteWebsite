---
title: 'Registro de cuenta'
cache_enable: false
process:
    twig: true
---

<h2> Solicitud de cuenta </h2>

<br> **Gracias por registrar una cuenta con Disroot. Revisaremos tu solicitud y nos contactaremos. El tiempo de demora, usualmente, no es mayor de 48 horas.**
<br>
<br>
**Saludos cordiales, <br>el equipo de Disroot.**
