---
title: "Formulaire de demande d'alias"
header_image: introverts.jpg
form:
    name: Alias-request-form
    fields:
        -
            name: username
            label: "Nom d'utilisateur"
            placeholder: "Entrez votre nom d'utilisateur Disroot"
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Deuxième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Troisième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Quatrième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Cinquième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Contribuant via'
            placeholder: sélectionner
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                stripe: 'Stripe (Carte de crédit)'
                bank: 'Virement bancaire'
                faircoin: Faircoin
                bitcoin: Bitcoin

            validate:
                required: true
        -
            name: amount
            label: Montant
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Répété
            type: radio
            default: Mensuellement
            options:
                monthly: Mensuellement
                yearly: Annuellement
            validate:
                required: true
        -
            name: referance
            label: Référence
            type: text
            placeholder: 'Nom du titulaire du paiement ou autre référence de transfert'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Envoyer
        -
            type: reset
            value: Réinitialiser
    process:
        -
            email:
                from: alias-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: "[Disroot] Votre demande d'alias"
                body: "<br><br>Bonjour {{ form.value.username|e }}, <br><br><strong>Merci de votre contribution à Disroot.org!</strong><br>Nous apprécions vraiment votre générosité.<br><br>Nous examinerons votre demande d'alias et vous répondrons dès que possible..</strong><br><br><hr><br><strong>Voici un résumé de la demande reçue:</strong><br><br>{% include ''forms/data.html.twig'' %}"
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Alias request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: "Votre demande d'alias a été envoyée!"
        -
            display: Merci
---

<h1 class="form-title"> Demande d'alias de courriel </h1>
<p class="form-text"><strong>Des alias sont à la disposition de nos contributeurs réguliers. </strong> Par contributeurs réguliers nous pensons à ceux qui donnent l'équivalent d'au moins une tasse de café par mois.
 <br><br>
Ce n'est pas que nous faisons la promotion du café, au contraire - le café est un symbole pour <a href="http://thesourcefilm.com/">l'exploitation</a> et <a href="http://www.foodispower.org/coffee/">l'inégalité</a> donc nous avons pensé que c'était une bonne façon de laisser les gens mesurer eux-mêmes combien ils peuvent donner.
Nous avons trouvé ceci <a href="https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee">liste</a> des prix des tasses de café dans le monde, ce n'est peut-être pas très précis, mais cela donne une bonne indication des différents tarifs..
<br><br>
<strong>Veuillez prendre le temps de réfléchir à votre contribution.</strong> Si vous pouvez nous "acheter" une tasse de café Rio De Janeiro par mois, c'est super, mais si vous avez les moyens d'acheter un double décaféiné Frappuccino au soja avec un extra Shot And Cream par mois, alors vous pouvez nous aider à maintenir la plateforme Disroot en fonctionnement et vous assurer qu'elle est disponible gratuitement pour d'autres personnes avec moins de moyens.</p>
