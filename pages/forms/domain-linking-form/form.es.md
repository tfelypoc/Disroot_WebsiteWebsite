---
title: 'Formulario para vinculación de dominio'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
           type: spacer
           title: 'Detalles de usuarix y dominio:'
           text: "Proporciona los detalles de tu cuenta y el dominio que quieres vincular a nuestros servidores."
           markdown: true

        -    
          name: username
          label: 'Nombre de usuarix'
          placeholder: 'Ingresa tu nombre de usuarix de Disroot'
          autofocus: 'on'
          autocomplete: 'on'
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: Domain
          label: 'Tu dominio'
          placeholder: example.com
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: catchall
          markdown: true
          type: checkbox
          label: '**Correo catchall** - es una funcionalidad para tu dominio personalizado. Te habilita a recibir todos los correos dirigidos a tu dominio incluso si no van dirigidos a tu dirección de correo (loquesea@tudominio.ltd).'
          validate:
            required: false
        -
          name: xmpp
          markdown: true
          type: checkbox
          label: '**Avanzado:** ¿Quieres utilizar el chat XMPP con tu dominio? Asegúrate que tu registro A esté apuntando a xmpp.disroot.org'
          validate:
            required: false
        -
          type: spacer
          title: 'Información de Alias'
          markdown: true
          text: "Utilizar alias te permite crear múltiples identidades para tu cuenta de Disroot que se pueden usar con tu dominio personalizado *(p.ej. minombre@ejemplo.com o apodo@ejemplo.com)*. Recuerda que estos están conectados a tu cuenta única y que puedes tener un máximo de cinco. Si quieres agreagar cuentas de usuarixs adicionales para usar con tu dominio, necesitas contactarnos via email o especificar más abajo las cuentas y alias adicionales que quieras agregar. Cualquier alias o cambio adicional necesita ser solicitado a Soporte hasta que una solución automática pueda ser implementada."

        -
          name: 'Alias 1'
          label: Alias
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: 'Alias 2'
          label: 'Segundo Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 3'
          label: 'Tercer Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 4'
          label: 'Cuarto Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 5'
          label: 'Quinto Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          type: spacer
          title: "Detalles de la Donación"
          markdown: true
          text: "Por favor, ingresa abajo los detalles de la donación que has realizado. También, asegúrate de hacerla antes de solicitar la funcionalidad de vinculación de dominio. **Esta información será eliminada al momento de procesar tu solicitud.**"

        -
          name: contribution
          label: 'Contribución via'
          placeholder: select
          type: select
          options:
            patreon: Patreon
            stripe: 'Stripe (tarjeta de crédito)'
            paypal: Paypal
            bank: 'Transferencia bancaria'
            faircoin: Faircoin
            bitcoin: Bitcoin
          validate:
            required: true

        -
          name: amount
          label: Amount
          placeholder: 'EUR/USD/BTC/etc.'
          type: text
          validate:
            pattern: '[A-Za-z0-9., ]*'
            required: true

        -
          name: comments
          type: textarea

        -
          name: honeypot
          type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Tu solicitud de vinculación de dominio'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Gracias por tu contribución a Disroot.org!</strong><br>Apreciamos honestamente tu generosidad.<br><br>Revisaremos tu solicitud y volveremos a contactarnos contigo tan pronto como podamos.</strong><br><br><hr><br><strong>Aquí un breve resúmen de la solicitud recibida:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Solicitud de vinculación de dominio] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: '¡Tu solicitud de vinculación ha sido enviada!'
        -
            display: thankyou
---

<h1 class="form-title"> Solicitud de vinculación de dominio </h1>
<p class="form-text"><strong> Completa este formulario si quieres utilizar tu propio para alias de correo y/o chat XMPP.</strong>
<br><br>
Asegúrate de haber seguido todos los pasos necesarios antes de realizar la solicitud. Cuida tu tiempo y el nuestro.
Revisamos todas las solicitudes pendientes una vez a la semana, así que se paciente. Nos contactaremos contigo tan pronto como tu solicitud sea procesada.  
</p>
<br><br><br><br>
