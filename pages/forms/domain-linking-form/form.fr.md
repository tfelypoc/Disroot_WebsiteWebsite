---
title: 'Formulaire pour lier un domaine'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
           type: spacer
           title: "Détail de l'utilisateur et du domaine:"
           text: "Fournissez des détails sur votre compte et le domaine que vous souhaitez lier à nos serveurs."
           markdown: true

        -    
          name: username
          label: "Nom d'utilisateur"
          placeholder: "Entrez votre nom d'utilisateur Disroot"
          autofocus: 'on'
          autocomplete: 'on'
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: Domain
          label: 'Votre domaine'
          placeholder: exemple.com
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: catchall
          markdown: true
          type: checkbox
          label: '**Email attrape-tout** - est une fonctionnalité pour votre domaine personnalisé. Elle vous permet de recevoir tous les e-mails adressés à votre domaine, même s'ils ne sont pas adressés à votre adresse e-mail (tout@votredomaine.ltd).'
          validate:
           required: false
        -
          name: xmpp
          markdown: true
          type: checkbox
          label: '**Avancé:** Voulez-vous utiliser xmpp chat avec votre domaine ? Assurez-vous de faire pointer un enregistrement A vers xmpp.disroot.org.'
          validate:
            required: false
        -
          type: spacer
          title: "Information sur l'alias"
          markdown: true
          text: "L'utilisation d'alias vous permet de créer plusieurs identités pour votre compte Disroot qui peuvent être utilisées avec votre domaine personnalisé *(ex. john@exemple.com ou surnom@exemple.com)*. N'oubliez pas que ces identités sont toujours liées à votre compte unique et que vous pouvez en avoir un maximum de 5 par compte. Si vous souhaitez ajouter des comptes d'utilisateurs supplémentaires pour utiliser votre domaine, vous devez nous contacter par e-mail ou ajouter les comptes supplémentaires et les alias que vous souhaitez leur ajouter dans la description ci-dessous. Tout alias supplémentaire ou changement doit être demandé en contactant le support jusqu'à ce qu'une solution automatique soit fournie."

        -
          name: 'Alias 1'
          label: Alias
          placeholder: votre-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: 'Alias 2'
          label: 'Deuxième alias (optionnel)'
          placeholder: votre-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 3'
          label: 'Troisième alias (optionnel)'
          placeholder: votre-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 4'
          label: 'Quatrième alias (optionnel)'
          placeholder: votre-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 5'
          label: 'Cinquième alias (optionnel)'
          placeholder: votre-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          type: spacer
          title: "Détails du don"
          markdown: true
          text: "Veuillez fournir ci-dessous les détails du don que vous avez effectué. Veillez à le faire avant de demander la liaison de votre domaine. **Ces informations seront supprimées dès que votre demande sera traitée.**"

        -
          name: contribution
          label: 'Contribution par'
          placeholder: select
          type: select
          options:
            patreon: Patreon
            stripe: 'Stripe (carte de crédit)'
            paypal: Paypal
            bank: 'Virement bancaire'
            faircoin: Faircoin
            bitcoin: Bitcoin
          validate:
            required: true
        -
          name: amount
          label: Montant
          placeholder: 'EUR/USD/BTC/etc.'
          type: text
          validate:
            pattern: '[A-Za-z0-9., ]*'
            required: true

        -
          name: comments
          type: textarea

        -

          name: honeypot
          type: honeypot
    buttons:
        -
            type: submit
            value: Soumettre
        -
            type: reset
            value: Effacer
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Your domain linking request'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Merci de votre contribution à Disroot.org!</strong><br>Nous apprécions vraiment votre générosité.<br><br>Nous examinerons votre demande et vous répondrons dès que possible.</strong><br><br><hr><br><strong>Voici un résumé de la demande reçue:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Lien vers un domaine personnel] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Votre demande de lien vers un domaine personnel a été envoyée!'
        -
            display: merci
---

<h1 class="form-title"> Lien vers un domaine personnel </h1>
<p class="form-text"><strong> Remplissez ce formulaire si vous souhaitez utiliser votre propre domaine pour les alias de courriel et/ou le chat xmpp.</strong>
<br><br>
Assurez-vous d'avoir suivi toutes les étapes nécessaires avant de demander votre domaine. Faites attention à votre temps et au nôtre.
Nous examinons toutes les demandes en attente une fois par semaine, alors soyez patient. Nous vous répondrons dès que nous aurons traité votre demande.
</p>
<br><br><br><br>
