---
title: 'Thanks for your request'
process:
    markdown: true
    twig: true
cache_enable: false
---

<br><br> **Depending on our workload it might take even two weeks to review your request, though we are doing our best to review requests weekly.**
<br>
<hr>
<br>
**Here is a summary of the received request:**
