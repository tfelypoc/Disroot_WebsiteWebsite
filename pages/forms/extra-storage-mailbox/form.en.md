---
title: 'Extra Mailbox Storage'
form:
    name: 'Extra Mailbox Space'
    fields:
        -
            name: username
            label: 'User Name'
            placeholder: 'Enter the Disroot user name'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Contact Email Address'
            placeholder: 'Enter an email address to contact you'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Additional Inbox Storage'
            label: 'Additional Storage in GB'
            type: number
            classes: form-number
            outerclasses: form-outer
            validate:
              min: 1
              max: 10
              steps: 1
              value: 1
              required: true
        -
            name: comments
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Payment details:**'
        -
            name: country
            label: 'Country (necessary for VAT purposes)'
            placeholder: 'Please specify the country your paying account is registered in'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Payment via (May be subjected to extra transaction fees)'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Bank transfer (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Extra Storage Request'
                body: 'Dear {{ form.value.username }}, <br><br> We have received a request for extra storage space. <br><br>You should receive your confirmation per E-mail with your billing reference. <br> Once we receive your payment we will assign you the extra storage. <br><br> Thank you for supporting Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Your request has been sent!'
        -
            display: thankyou
---


<h1 class="form-title"> Extra Mailbox Storage </h1>

<div class="form-text">
<p markdown="1">
It is possible to extend your E-mail storage with up to 10 GB for the cost of 0.15 euro per GB per month, paid yearly. If you are adding storage both to your mailbox and cloud account - use [this form](https://disroot.org/en/forms/extra-storage-space)</p><br>
</div>
