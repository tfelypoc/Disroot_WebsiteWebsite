---
title: 'Extra Mailbox Storage'
form:
    name: 'Extra Mailbox Space'
    fields:
        -
            name: Nombre de usuario
            label: 'Nombre de usuario'
            placeholder: 'Ingresa el nombre de usuario de Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: Correo
            label: 'Dirección de correo de Contacto'
            placeholder: 'Ingresa una dirección de correo para contactarte'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Espacio adicional de buzón'
            label: 'Espacio adicional en GB'
            type: number
            classes: form-number
            outerclasses: form-outer
            validate:
              min: 1
              max: 10
              steps: 1
              value: 1
              required: true
        -
            name: Comentarios
            type: textarea
        -
            name: 'Detalles del Pago'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Detalles del Pago:**'
        -
            name: País
            label: 'País (Necesario por razones de I.V.A)'
            placeholder: 'Por favor, especifica el país en el que está registrada tu cuenta de pago'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: Pago
            label: 'Pago a través de: (Podría estar sujeto a costos adicionales de transacción)'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Transferencia bancaria (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Enviar
        -
            type: reset
            value: Reiniciar
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Solicitud de Almacenamiento Extra'
                body: 'Estimad@ {{ form.value.username }}, <br><br> Hemos recibido una solicitud de espacio extra de almacenamiento. <br><br>Deberías recibir tu confirmación por correo con tu referencia de facturación. <br> Una vez que hayamos recibido tu pago, te adjudicaremos el espacio adicional. <br><br> ¡Gracias por apoyar a Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: '¡Tu solicitud ha sido enviada!'
        -
            display: thankyou
---

<h1 class="form-title"> Espacio de almacenamiento adicional </h1>

<div class="form-text">
<p markdown="1">
Es posible aumentar el espacio de almacenamiento de tu E-mail hasta 10 GB por 0.15 euro por GB mensual. Si estás adicionando espacio para tus cuentas de correo y la nube, utiliza [este formulario](https://disroot.org/es/forms/extra-storage-space)</p><br>
</div>
