---
title: 'Archiviazione extra per la Mailbox'
form:
    name: 'Archiviazione extra per la Mailbox'
    fields:
        -
            name: username
            label: 'Nome utente'
            placeholder: 'Inserire il nome utente Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Indirizzo Email del contatto'
            placeholder: 'Inserisci un indirizzo Email per contattarti'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Additional Inbox Storage'
            label: 'Memoria aggiuntiva in GB'
            type: number
            classes: form-number
            outerclasses: form-outer
            validate:
              min: 1
              max: 10
              steps: 1
              value: 1
              required: true
        -
            name: comments
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Dettagli del pagamento:**'
        -
            name: country
            label: 'Paese (necessario ai fini IVA)'
            placeholder: 'Si prega di specificare il paese in cui è registrato il tuo conto di pagamento'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Pagamento tramite (Può essere soggetto a spese di transazione extra)'
            placeholder: seleziona
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Bonifico bancario (Solo paesi area SEPA)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Invia
        -
            type: reset
            value: Azzera
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Richiesta di archiviazione extra'
                body: 'Gentile {{ form.value.username }}, <br><br> Abbiamo ricevuto una richiesta di spazio di archiviazione supplementare. <br><br>Dovresti ricevere la tua conferma via Email con il tuo riferimento di fatturazione. <br> Una volta ricevuto il tuo pagamento ti assegneremo il volume extra. <br><br> Grazie per sostenere Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'La tua richiesta è stata inviata!'
        -
            display: grazie
---

<h1 class="form-title"> Spazio di archiviazione extra </h1>

<div class="form-text">
<p markdown="1">
E' possibile estendere l'archiviazione della posta elettronica fino a 10 GB al costo di 0,15 euro per GB al mese. Se state aggiungendo spazio di archiviazione sia alla vostra casella di posta che al vostro account cloud - utilizzate [questo modulo](https://disroot.org/en/forms/extra-storage-space)</p><br>
</div>
