---
title: 'Extra Storage Space'
form:
    name: 'Extra Storage Space'
    fields:
        -
            name: username
            label: 'User Name'
            placeholder: 'Enter the Disroot user name'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Contact Email Address'
            placeholder: 'Enter an email address to contact you'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Amount of Space'
            label: 'Total Storage Space'
            type: select
            default: 5GB
            options:
                5GB: '5GB for 11€ annually'
                10GB: '10GB for 20€ annually'
                15GB: '15GB for 29€ annually'
                30GB: '30GB for 56€ annually'
                45GB: '45GB for 83€ annually'
                60GB: '60GB for 110€ annually'
            validate:
                required: true
        -
            name: 'Mail size'
            label: 'How many of these GB do you want for mail?'
            placeholder: 'Enter the desired size for mail storage'
            autofocus: 'on'
            autocomplete: 'on'
            type: number

        -
            name: 'Cloud size'
            label: 'How many of these GB do you want for cloud?'
            placeholder: 'Enter the desired size for cloud storage'
            autofocus: 'on'
            autocomplete: 'on'
            type: number

        -
            name: comments
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Payment details:**'
        -
            name: country
            label: 'Country (necessary for VAT purposes)'
            placeholder: 'Please specify the country your paying account is registered in'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Payment via'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Bank transfer (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Extra Storage Request'
                body: 'Dear {{ form.value.username }}, <br><br> We have received a request for extra storage space. <br><br>You should receive your confirmation per E-mail with your billing reference. <br> Once we receive your payment we will assign you the extra storage. <br><br> Thank you for supporting Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Your request has been sent!'
        -
            display: thankyou
---

<h1 class="form-title"> Extra Storage Space </h1>
<p class="form-text">With your <b>Disroot</b> account, you get FREE storage: 1GB for your emails, 2GB for the cloud. However, we offer the possibility to extend this.<br>
Prices are <b>per year</b> and <b>payment fees included</b>.<br>
However, <b>European users</b> have to add a <b>VAT</b> (Value Added Tax) which is 21% more.<br>
