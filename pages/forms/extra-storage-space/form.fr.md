---
title: 'Espace de stockage supplémentaire'
form:
    name: 'Espace de stockage supplémentaire'
    fields:
        -
            name: username
            label: "Nom d'utilisateur"
            placeholder: "Tapez votre nom d'utilisateur Disroot"
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Adresse email de contact'
            placeholder: 'Tapez une adresse email pour vous contacter'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Amount of Space'
            label: "Espace de stockage total"
            type: select
            default: 5Go
            options:
                5GB: '5Go pour 11€ annuellement'
                10GB: '10Go pour 20€ annuellement'
                15GB: '15Go pour 29€ annuellement'
                30GB: '30Go pour 56€ annuellement'
                45GB: '45Go pour 83€ annuellement'
                60GB: '60Go pour 110€ annuellement'
            validate:
                required: true
        -
            name: 'Mail size'
            label: 'Combien de ces Go voulez-vous pour le mail ?'
            placeholder: 'Entrez la taille souhaitée pour le stockage mail'
            autofocus: 'on'
            autocomplete: 'on'
            type: number

        -
            name: 'Cloud size'
            label: 'Combien de ces Go voulez-vous pour le cloud?'
            placeholder: 'Entrez la taille souhaitée pour le stockage mail cloud'
            autofocus: 'on'
            autocomplete: 'on'
            type: number
        -
            name: comments
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Informations de paiement:**'
        -
            name: country
            label: 'Pays (nécessaires à des fins de TVA)'
            placeholder: 'Veuillez spécifier le pays dans lequel votre compte de paiement est enregistré.'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Paiement par'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (Carte de crédit)'
                bank: 'Virement bancaire (pays SEPA uniquement)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Envoyer
        -
            type: reset
            value: Réinitialiser
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Demande de stockage supplémentaire'
                body: 'Cher/Chère {{ form.value.username }}, <br><br> Nous avons reçu une demande de stockage supplémentaire. <br><br>Vous devriez recevoir votre confirmation par E-mail avec votre référence de facturation. <br> Une fois que nous aurons reçu votre paiement, nous vous attribuerons le stockage supplémentaire. <br><br> Merci de soutenir Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Votre demande a été envoyée!'
        -
            display: thankyou
---

<h1 class="form-title"> Espace de stockage supplémentaire </h1>
<p class="form-text">Avec votre compte <b>Disroot</b>, vous bénéficiez d'un stockage GRATUIT : 1Go pour vos emails, 2Go pour le cloud. Cependant, nous offrons la possibilité d'étendre cela.<br>
Les prix sont <b>par an</b> et <b>inclus les frais de paiement</b>.<br>
Cependant, <b>les utilisateurs européens</b> doit ajouter une <b>TVA</b> (Taxe sur la valeur ajoutée) qui est de 21% en plus.<br>
