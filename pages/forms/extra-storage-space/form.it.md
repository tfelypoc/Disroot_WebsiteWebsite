---
title: 'Spazio di archiviazione extra'
form:
    name: 'Spazio di archiviazione extra'
    fields:
        -
            name: username
            label: 'Nome utente'
            placeholder: 'Inserire il nome utente Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Indirizzo Email del contatto'
            placeholder: 'Inserisci un indirizzo Email per contattarti'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Amount of Space'
            label: 'Spazio di archiviazione totale'
            type: select
            default: 5GB
            options:
                5GB: '5GB per 11€ annuali'
                10GB: '10GB per 20€ annuali'
                15GB: '15GB per 29€ annuali'
                30GB: '30GB per 56€ annuali'
                45GB: '45GB per 83€ annuali'
                60GB: '60GB per 110€ annuali'
            validate:
                required: true
				
        -
            name: 'Mail size'
            label: 'Quanti di questi GB vuoi per la posta?'
            placeholder: 'Immettere la dimensione desiderata per l'archiviazione della posta'
            autofocus: 'on'
            autocomplete: 'on'
            type: number

        -
            name: 'Cloud size'
            label: 'Quanti di questi GB vuoi per il cloud?'
            placeholder: 'Immettere la dimensione desiderata per l'archiviazione su cloud'
            autofocus: 'on'
            autocomplete: 'on'
            type: number

        -
            name: comments
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Dettagli pagamento:**'
        -
            name: country
            label: 'Paese (necessario ai fini dell'IVA)'
            placeholder: 'Specifica il paese in cui è registrato il tuo conto bancario'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Pagamento tramite'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Trasferimento bancario (Solo per paesi SEPA)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Extra Storage Request'
                body: 'Dear {{ form.value.username }}, <br><br> We have received a request for extra storage space. <br><br>You should receive your confirmation per E-mail with your billing reference. <br> Once we receive your payment we will assign you the extra storage. <br><br> Thank you for supporting Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'La tua richiesta è stata inviata!'
        -
            display: thankyou
---

<h1 class="form-title">Spazio di archiviazione aggiuntivo </h1>
<p class="form-text">Con il tuo account <b>Disroot</b>, hai i seguenti spazi di archiviazione gratuiti: 1 GB per le tue e-mail, 2 GB per il cloud. Tuttavia, offriamo la possibilità di estenderlo .<br>
I prezzi si intendono <b>all'anno</b> e <b>spese di pagamento incluse</b>.<br>
Tuttavia, gli <b>utenti europei</b> devono aggiungere una <b>IVA</b> (imposta sul valore aggiunto) che è il 21% in più.<br>
