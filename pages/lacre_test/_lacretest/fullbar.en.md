---
title: Lacre test on Disroot
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: privacy
text_align: left
---

<br>
<br>
<br>
<br>
We are sorry to inform that we have decided to postpone this weekend's Lacre test on Disroot. Last weekend, we have unofficially run Lacre on Disroot server. Since all email traffic passes through Lacre regardless whether user uploaded their public key or not, we could observe how Lacre would perform. We were very happy with the results, both under normal everyday "plain text" load as well as when stress-testing with generated encrypted traffic, Lacre performs superb and we are confident it will have minimal impact on server performance. However, thanks to running Lacre in ordinary traffic situation, we have found number of issues we would not find otherway. Although most of them have been addressed and fixed (pfm for president \o/ ), there is one issue, we have not managed to address yet. Even though the bug affect rare cases, we do not want to risk issues in mail delivery, especially when it may also potentially affects users not participating in the beta tests.

So with hard feelings, we have made the decision that it is better to focus on ironing issues out, especially the ones we know about, and announce new date once we are ready. We would love to enable Lacre to all of you (and us) right away, but it's important to provide the best experience possible. We are very close to achieving that, we've waited for years so few more weeks aren't that bad.

Sorry for that and let's look forward to new beta or even Lacre's permanent appearance on Disroot. For all those that do want to check things out and play with Lacre, you can always [join our chatroom](https://lacre.io/contact) and ask for a test account. Check our project's website on [https://lacre.io](https://lacre.io).
