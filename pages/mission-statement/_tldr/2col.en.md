---
title: Mission Statement
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### TL:DR (Too Long, DID Read)

The purpose of Disroot.org is to reclaim the virtual world from the monolithic reality that has taken it over. We are certain that this approach has gigantic potential for uprooting society from current status quo ruled by advertisement, consumerism and the power of the highest bidder. By decentralizing a co-operated network of services in the 21st century, we can overcome the shortcomings imposed by financial limitations, energy footprint and complex infrastructures - giving the network the power to grow independently according to its own rules. A network of many small tribes cooperating with each other will defeat one monolithic giant. Will you come and disroot along with us?
