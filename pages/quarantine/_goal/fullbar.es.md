---
title: 'Objetivo'
bgcolor: '#09846aff'
fontcolor: '#FFFFFF'
text_align: left
---

<br>


# Guía rápida:
1. Para empezar, instala **Teeworlds** utilizando el gestor de software si estás usando un sistema operativo de verdad, como GNU/Linux / BSD, o ve al [sitio de Teeworlds](https://teeworlds.com/?page=downloads) y descárgalo para tu plataforma.
2. Una vez que lo hayas hecho, inicia el juego.
3. Primero selecciona "Setup" para agregar tu nombre de jugador o jugadora y elegir y personalizar tu Tee como quieras.
4. Dale a "Play", busca **"Quarantine with Disroot"** y únete a nosotrxs.

![](splashtee3.png)
