---
title: 'Vous vous ennuyez déjà'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;">
## Vous vous ennuyez déjà ?

Comme beaucoup d'entre vous (nous inclus) sont enfermés chez eux en essayant d'éviter tout contact avec d'autres êtres humains, nous avons pensé qu'il serait agréable de passer ce temps à socialiser devant l'ordinateur avec une bande d'étrangers que vous connaissez si bien grâce aux chats, aux réseaux sociaux fédérés et que vous n'avez probablement jamais vus dans la vraie vie.

Passons une soirée à boire de l'alcool, à manger des cochonneries et à se tirer dessus sur Teeworlds. La première édition de la Quarantaine avec Disroot commence le samedi 21.03.2020 20:00 CET et se poursuivra jusqu'au dernier debout. Le serveur sera probablement ouvert pendant toute la semaine ou plus s'il y a des gens qui veulent jouer.

</span>

<span style="padding-right:30%;">
</span>
