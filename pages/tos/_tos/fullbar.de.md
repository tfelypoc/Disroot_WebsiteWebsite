---
title: 'Nutzungsbedingungen'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: tos
text_align: left
---

<br>
**v1.2 - März 2020**

<a name="about"></a>
<br>
## Über dieses Dokument

Dieses Dokument wurde im Original in Englisch verfasst. Die englische Version ist die einzige Version des Dokuments, für die **Stichting Disroot.org** haftbar gemacht werden kann.<br>
Jede Übersetzung der **Nutzungsbedingungen**, so wie die vorliegende, ist eine Bemühung der Community, die Informationen in anderen Sprachen zugänglich zu machen und sollte als solche verstanden werden, mit keinem anderen Wert als der reinen Information.
<br>
# Annahme der Bedingungen
Durch die Nutzung eines beliebigen, von **Disroot.org** bereitgestellten Angebots stimmst Du den [**Nutzungsbedingungen**](https://disroot.org/en/tos) zu. Daher solltest Du diese Bedingungen zunächst lesen, bevor Du unsere Plattform nutzt.

## Community Hinweis
**Disroot** ist eine Gemeinschaft und unsere Plattform ist Teil des "Freie und quelloffene Software"-Ökosystems (FOSS). Wir alle teilen Serverressourcen, arbeiten zusammen und nutzen gemeinsam die **Disroot.org**-Dienste. Es ist sehr wichtig, dass jede und jeder von uns das versteht und sich gleichermaßen dafür verantwortlich fühlt. Halte Dich daher bitte an die folgenden Richtlinien, dann werden wir alle miteinander klarkommen.

# Nutzungsbedingungen
<a name="users"></a>
<br>
## 1. Wer kann unser Angebot nutzen?
Jede Person, die mindestens 16 Jahre alt ist, kann unser Angebot nutzen.

<br>

<a name="space"></a>
<br>
## 2. Speicherplatzbeschränkungen auf der Festplatte
**Ressourcen sind knapp**. Sei Dir bewusst, dass das Aufheben von unnötigen Dateien und E-Mails möglicherweise Serverplatz einschränkt, was dazu führen kann, dass weniger Accounts für Andere bereitsstehen.
<br>**Nicht zumüllen!**

<br>

<a name="expiration"></a>
<br>
## 3. Account-Verfall
Aus Sicherheitsgründen löscht **Disroot.org** keine ungenutzten Accounts. Das kann sich in Zukunft eventuell ändern, wenn wir gezwungen sind, Speicherplatz für andere Nutzer zu schaffen. Solche Änderungen in unseren Bedingungen werden ausreichend im Voraus über alle Kommunikationskanäle bekanntgegeben.<br>Wir würden es jedoch vorziehen, wenn unsere Nutzer Verantwortung für ihren Account und dessen Einfluss auf die **Disroot**-Ressourcen übernehmen und ihren [**Account löschen**](https://user.disroot.org), wenn sie unsere Angebote nicht länger nutzen wollen, oder ungenutzte bzw. unnötige Dateien löschen.

<br>

<a name="password"></a>
<br>
## 4. Passwort
Wir überwachen niemanden, der die Aktivierung eines **Disroot**-Accounts beantragt. Wenn Du Deinen Account erstellst, kannst Du entweder Sicherheitsfragen und/oder das Hinzufügen einer zweiten E-Mailadresse auswählen. Beide Optionen werden es Dir ermöglichen, Dein Passwort zurückzusetzen, wenn Du es verlierst oder vergisst.<br> - Wenn Du keine zweite E-Mailadresse angibst oder die Antworten auf die Sicherheitsfragen vergisst, sind wir nicht in der Lage, Dir die notwendigen Daten zuzusenden, um wieder Zugriff auf Deinen Account zu erhalten. Denke auch daran, Dein Passwort regelmäßig zu ändern: Das kannst Du machen, indem Du Dich in den [User Self-Service](https://user.disroot.org) einloggst und auf 'Change password' bzw. 'Passwort ändern' klickst.<br>

**! Warnung !** Verlorene Passwörter führen zu einem nicht wiederherstellbaren Verlust Deiner Clouddaten. Um Dich davor zu schützen, kannst Du die Passwort-Wiederherstellung in Deinen persönlichen Cloud-Einstellungen aktivieren.

<br>

<a name="spam"></a>
<br>
## 5. SPAM und Viren
Um zu verhindern, dass unsere Server auf Sperrlisten landen:
- Wir untersagen die Nutzung von **Disroot**-E-Mailaccounts zum Versenden von SPAM. Jeder diesbezüglich festgestellte Account wird ohne weitere Mitteilung deaktiviert.
- Wenn Du eine größere Menge an SPAM oder verdächtigen E-Mails bekommst, sag uns bitte auf [**support@disroot.org**](mailto:support@disroot.org) Bescheid, damit wir uns das näher ansehen können.

<br>


<a name="copyright"></a>
<br>
## 6. Copyright-Material
Eines der Hauptziele von **Disroot.org** ist es, Freie und quelloffene Software (FOSS) und alle Arten von Copyleft-Inhalten zu fördern. Nimm auch Du dieses Ziel auf und unterstütze Künstler und Projekte, die Ihre Arbeit unter diesen Lizenzen veröffentlichen.
- Das Hochladen und (vor allem) das öffentliche Teilen von Copyright-Material auf **Disroot** kann sowohl andere Nutzer und **Disroot.org** in ernsthafte juristische Schwierigkeiten bringen als auch das ganze Projekt und die Existenz unserer Server (und aller damit verbundenen Angebote) gefährden, also **lass es einfach**.

<br>


<a name="not_allowed"></a>
<br>
## 7. Nicht erlaubte Aktivitäten
Beteilige Dich nicht an den folgenden Handlungen, während Du die durch **Disroot.org** bereitgestellten Angebote nutzt:

<ol class=disc>
<li> <b>Belästigung und Beschimpfung anderer</b> durch die Beteiligung an Drohungen, Stalking oder dem Senden von SPAM.</li>
<li> <b>Beiträge zur Diskriminierung, Belästigung oder Schaden eines Individuums oder einer Gruppe.</b> Das umfasst auch das Verbreiten von Hass und Fanatismus durch Rassismus, Ethnophobie, Antisemitismus, Sexismus, Homophobie und andere Arten diskriminierenden Verhaltens.</li>
<li> <b>Beteiligung am Missbrauch anderer</b> durch die Verbreitung von Material, dessen Herstellungsprozess Gewalt oder sexuelle Übergriffe gegen Personen oder Tiere beinhaltete</li>
<li> <b>Imitation oder Darstellung einer anderen Person</b> auf eine verwirrende oder täuschende Weise, es sei denn, der Account ist deutlich als Parodie gekennzeichnet.</li>
<li> <b>Doxing</b>: die Veröffentlichung von persönlichen Daten einer anderen Person ist nicht erlaubt, denn:<br> (I) wir können weder die Absichten noch die Konsequenzen für die bloßgestellte Person abschätzen und (II) es bedeutet ein zu großes juristisches Risiko für <b>Disroot.org</b>. </li>
<li> <b>Missbrauch der Dienste</b> durch die Verbreitung von Viren oder Malware, Beteiligung an Denial-of-Service-Attacken oder den Versuch, sich unauthorisierten Zugriff auf irgendein Computersystem, dieses eingeschlossen, zu verschaffen.</li>
<li> <b>Verbotene Aktivitäten</b> nach nationalem oder internationalem Recht.</li>
</ol>

<br>

<a name="commercial"></a>
<br>
## 8. Nutzen des Disroot-Angebots für kommerzielle Aktivitäten
**Disroot** ist eine Non-Profit-Organisation, die Dienste für Individuen auf einer "zahle soviel Du willst"-Basis anbietet. Aufgrund dieser Struktur verstehen wir die Nutzung der **Disroot**-Dienste für kommerzielle Zwecke als Missbrauch des Angebots und werden sie als solche behandeln.
Diese kommerziellen Aktivitäten oder Zwecke beinhalten (nicht ausschließlich):
<ol class=disc>
<li>Handeln jedes <b>Disroot</b>-Angebots mit einer dritten Partei.<br>Das ist untersagt.</li>
<li>Nutzen der E-Mail für Zwecke wie "no-reply"-Accounts für ein Geschäft.<br>Wenn wir eine solche Aktivität feststellen, wird der Account ohne Warnung blockiert.</li>
<li>Versenden von Massen-E-Mails, zum Beispiel aber nicht ausschließlich für Vertrieb oder Werbung zu geschäftlichen Zwecken.<br>
Der Account wird als Spam behandelt und bei Feststellung blockiert ohne vorherige Mitteilung.</li>
<li>Nutzung des <b>Disroot</b>-Angebots zum finanziellen Gewinn, zum Beispiel aber nicht ausschließlich durch Handel oder zur Verkaufsverwaltung, wird nicht toleriert.<br>Accounts, die zum Zwecke der Gewinnerwirtschaftung erstellt wurden, werden bei Feststellung geschlossen.</li>
</ol>
<br>
Die Nutzung des <b>Disroot</b>-Angebots für andere kommerzielle Aktivitäten wird fallbezogen untersucht und die Entscheidung über eine Schließung des entsprechenden Accounts hängt ab von der Kommunikation mit dem Accountbetreiber und der Art der fraglichen Aktivitäten.
<br>
Wenn Du Dir nicht sicher bist oder Fragen hast über die Interpretation des Begriffs "Kommerzielle Aktivitäten", kontaktiere uns bitte.

<br>

<a name="ownership"></a>
<br>
## 9. Eigentum von und Verantwortlichkeit für Inhalte
**Disroot.org** ist nicht verantwortlich für jegliche Inhalte, die auf einem unserer Dienste veröffentlicht werden. Du bist verantwortlich für Deine Nutzung der **Disroot**-Dienste, für jeden Inhalt, den Du bereitstellst und alle Konsequenzen daraus. Allerdings behalten wir uns, um unsere Existenz zu schützen, das Recht vor, jeden Inhalt zu löschen, wenn er gegen geltendes Recht verstößt.

<br>

<a name="legal"></a>
<br>
## 10. Juristische Verantwortlichkeit
Wie bei allem anderen, was Du tust, sei Dir bewusst, dass **Disroot.org** weder verantwortlich ist für das, was Du schreibst, noch für die Absicherung Deiner Privatsphäre. Wir laden Dich daher ein, das beste aus den existierenden Werkzeugen zu machen, um Deine Rechte zu schützen.<br><br>
Um mehr darüber zu erfahren, wie Deine Daten gespeichert und genutzt werden, sieh Dir unsere [**Datenschutzerklärung**](https://disroot.org/de/privacy_policy) an.

<br>

<a name="laws"></a>
<br>
## 11. Gesetze
**Disroot.org** wird in den **Niederlanden** gehostet und unterliegt daher **niederländischen** Gesetzen und Rechtssprechung.

<br>

<a name="termination"></a>
<br>
## 12. Account-Löschung
**Disroot.org** kann Deinen Zugang zu jeder Zeit unter den folgenden Bedingungen beenden:
<ol class=disc>
<li>Der Account wurde beim Versenden von SPAM (übermäßige Mengen unaufgeforderter E-Mails) identifiziert.</li>
<li>Der Account war an einer oder mehrerer der oben genannten verbotenen Aktivitäten beteiligt.</li>
</ol>
Die genannten Bedingungen können sich jederzeit ändern.

<br>


<a name="warranty"></a>
<br>
## 13. Keine Garantie
Du verstehst und bist einverstanden, dass **Disroot.org** Dir in erster Linie internetbasierte Dienste zur Verfügung stellt. Die Dienste werden angeboten wie sie sind, abhängig von der Verfügbarkeit und ohne Verbindlichkeit oder Haftung. So wenig wir auch nur einen einzigen unserer Nutzer enttäuschen wollen, können wir doch keine Garantie für die Zuverlässigkeit, Erreichbarkeit oder Qualität unserer Dienste geben.<br> Du bist einverstanden, dass die Nutzung unserer Dienste auf Dein alleiniges und ausschlie&szlig;liches Risiko geschieht.

<br>


<a name="changes"></a>
<br>
## 14. Änderungen dieser Nutzungsbedingungen
Wir behalten uns das Recht vor, diese Bedingungen jederzeit zu ändern. Wir schätzen Transparenz sehr und tun daher unser Bestes um sicherzustellen, dass unsere Nutzer so früh wie möglich per E-Mail, über Soziale Netzwerke und/oder auf unserem [Blog](hhtps://disroot.org/de/blog) über wichtige Änderungen informiert werden. Über kleinere Änderungen werden wir vermutlich nur in uneserem Blog informieren.
<br>
Du kannst die vergangenen Änderungen dieses Dokuments auf unserem [**Git-Repository**](https://git.disroot.org/Disroot/Disroot-ToS/commits/branch/master).


[Zurück zum Anfang](#top)
