---
title: "Conditions d'utilisation"
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _tos
body_classes: modular
header_image: havefun.jpg

translation_banner:
    set: true
    last_modified: Mars 2022
    language: French
---
