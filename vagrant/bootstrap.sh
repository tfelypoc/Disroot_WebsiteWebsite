#!/usr/bin/env bash

# Custom configuration
WEB_NAME="disroot.lan"
WEB_ROOT="/var/www/"
WWW_USER="www-data"
YAML_SYSTEM="/var/www/disroot.lan/user/config/system.yaml"
YAML_SITE="/var/www/disroot.lan/user/config/site.yaml"
GRAV_VERSION="1.7.24"

# Provisioning actions
apt install dialog

# Avoid Postfix installation interactive screens by preconfiguring this information
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type select No configuration"
sudo debconf-set-selections <<< "postfix postfix/mailname string ${WEB_NAME}"

# Add sury repository to sources.list for php8.0
echo "set grub-pc/install_devices /dev/sda" | debconf-communicate # Fix grub error
sudo apt-get -y update
sudo apt-get -y dist-upgrade
sudo apt-get -y install ca-certificates apt-transport-https
if ! grep -q "^deb .*sury" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
   sudo sh -c 'echo "deb https://packages.sury.org/php/ buster main" > /etc/apt/sources.list.d/php.list'
fi

# Sury Key for php8.0
wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
sudo apt-get -y update

echo "Installing nginx..."
sudo apt-get install -y nginx-full

echo "Installing composer..."
sudo apt-get install -y composer

echo "Installing php8.0..."
sudo apt-get install -y php8.0 php8.0-zip php8.0-cli php8.0-curl php8.0-gd php8.0-mbstring php8.0-xml php8.0-fpm

# Create the Nginx config files and restart webserver
echo "Installing Nginx config files..."
sudo rsync -cr /vagrant/provision/etc/nginx/sites-available /etc/nginx/
if [ ! -f /etc/nginx/sites-enabled/"${WEB_NAME}".conf ]; then
    sudo ln -s /etc/nginx/sites-available/"${WEB_NAME}".conf /etc/nginx/sites-enabled/"${WEB_NAME}".conf
fi
if [ -f /etc/nginx/sites-enabled/default ]; then
    sudo rm /etc/nginx/sites-enabled/default
fi
sudo service nginx restart

# Install GRAV in webroot
echo "Installing GRAV..."
sudo chown -R ${WWW_USER}:${WWW_USER} "${WEB_ROOT}"

if [ ! -f "${WEB_ROOT}""${WEB_NAME}" ]; then
   sudo -u "${WWW_USER}" mkdir "${WEB_ROOT}""${WEB_NAME}"
fi

## Specific version
FILE="${WEB_ROOT}"grav-v"${GRAV_VERSION}".zip
if [ ! -f "$FILE" ]; then
  echo "Downloading grav-v"${GRAV_VERSION}".zip"
  sudo -u "${WWW_USER}" wget https://github.com/getgrav/grav/releases/download/"${GRAV_VERSION}"/grav-v"${GRAV_VERSION}".zip -P "${WEB_ROOT}"
  sudo -u "${WWW_USER}" unzip -o "${WEB_ROOT}"grav-v"${GRAV_VERSION}".zip -d "${WEB_ROOT}"
  sudo -u "${WWW_USER}" cp -r "${WEB_ROOT}"grav/* "${WEB_ROOT}""${WEB_NAME}"
else
  echo "grav-v"${GRAV_VERSION}".zip already exists"
  sudo -u "${WWW_USER}" unzip -o "${WEB_ROOT}"grav-v"${GRAV_VERSION}".zip -d "${WEB_ROOT}"
  sudo -u "${WWW_USER}" cp -r "${WEB_ROOT}"grav/* "${WEB_ROOT}""${WEB_NAME}"
fi

# Directly from Master
# if [ ! -f /usr/local/bin/composer ]; then
#     sudo wget https://getcomposer.org/download/1.9.1/composer.phar -O /usr/local/bin/composer && sudo chmod 755 /usr/local/bin/composer
# fi
# echo "Git"
# git clone -b master https://github.com/getgrav/grav.git "${WEB_ROOT}""${WEB_NAME}"
# sudo chown -R ${WWW_USER}:${WWW_USER} "${WEB_ROOT}"
# echo "enter git"
# cd "${WEB_ROOT}""${WEB_NAME}"
# echo "composer"
# sudo -u "${WWW_USER}" composer install --no-dev -o
# sudo chown -R ${WWW_USER}:${WWW_USER} "${WEB_ROOT}"
# sudo chmod 775 -R bin/
# sudo -u "${WWW_USER}" bin/grav install

#sudo -u "${WWW_USER}" bin/gpm install disroot
#sudo -u "${WWW_USER}" bin/gpm install language-selector -> Disabled as we're using grav-plugin-language-selector Disroot repo

echo "Enter git"
cd "${WEB_ROOT}""${WEB_NAME}"

yes | sudo -u "${WWW_USER}" php8.0 bin/gpm install themer
# https://github.com/sommerregen/grav-plugin-themer

yes | sudo -u "${WWW_USER}" php8.0 bin/gpm install tagcloud
# https://github.com/Perlkonig/grav-plugin-tagcloud

yes | sudo -u "${WWW_USER}" php8.0 bin/gpm install form
# https://github.com/getgrav/grav-plugin-form

# Add Grav config (the second extra false should be set to true, but it creates an error in Grav ("Trying to access array offset on value of type null")
# Create the Nginx config files and restart webserver
echo "Installing Grav config file config file..."
sudo rsync -cr "/vagrant/provision/${YAML_SYSTEM}" "${YAML_SYSTEM}"
sudo rsync -cr "/vagrant/provision/${YAML_SITE}" "${YAML_SITE}"

# Add website domain to local /etc/hosts file
sudo sed -i "s/127.0.0.1\tlocalhost/127.0.0.1\tlocalhost $WEB_NAME/" /etc/hosts

# Add website pages in GRAV
sudo mount -o bind /var/www/pages "${WEB_ROOT}""${WEB_NAME}"/user/pages
chown "${WWW_USER}":"${WWW_USER}" -R "${WEB_ROOT}""${WEB_NAME}"/user/pages

# Add Disroot theme in GRAV
echo "Installing Disroot theme in GRAV..."
if [ ! -d "${WEB_ROOT}""${WEB_NAME}"/user/themes/disroot/grav-theme-disroot ]; then
    sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"grav-theme-disroot "${WEB_ROOT}""${WEB_NAME}"/user/themes/disroot
fi

# Add Disroot Blog theme in GRAV
echo "Installing Disroot Blog theme in GRAV..."
if [ ! -d "${WEB_ROOT}""${WEB_NAME}"/user/themes/disrootblog/grav-theme-disrootblog ]; then
    sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"grav-theme-disrootblog "${WEB_ROOT}""${WEB_NAME}"/user/themes/disrootblog
fi

# Add language-selector in GRAV
echo "Installing language-selector in GRAV..."
if [ ! -d "${WEB_ROOT}""${WEB_NAME}"/user/plugins/language-selector/grav-plugin-language-selector ]; then
    sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"grav-plugin-language-selector "${WEB_ROOT}""${WEB_NAME}"/user/plugins/language-selector
fi

# Add Privacy pages in GRAV
echo "Installing Privacy pages in GRAV..."
if [ ! -d "${WEB_ROOT}""${WEB_NAME}"/user/pages/privacy_policy/_pp ]; then
    sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"privacy_policy "${WEB_ROOT}""${WEB_NAME}"/user/pages/privacy_policy/_pp
fi

# Add Changelog pages in GRAV
echo "Installing Changelog pages in GRAV..."
if [ ! -d "${WEB_ROOT}""${WEB_NAME}"/user/pages/changelog/_main ]; then
    sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"changelog "${WEB_ROOT}""${WEB_NAME}"/user/pages/changelog/_main
fi

# Changelog pages name in GRAV
if [ -f "${WEB_ROOT}"changelog/CHANGELOG ]; then
    sudo -u "${WWW_USER}" cp "${WEB_ROOT}"changelog/CHANGELOG "${WEB_ROOT}"changelog/fullbar.en.md
fi

# Remove the default grav files and folder from new installation
echo "Remove the default grav files and folder from new installation..."
if [ -d "${WEB_ROOT}""${WEB_NAME}"/user/pages/02.typography ]; then
rm -r "${WEB_ROOT}""${WEB_NAME}"/user/pages/02.typography
fi
if [ -f "${WEB_ROOT}""${WEB_NAME}"/user/pages/01.home/default.md ]; then
rm  "${WEB_ROOT}""${WEB_NAME}"/user/pages/01.home/default.md
fi

exit 0
